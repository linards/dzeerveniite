<?
error_reporting(E_ALL);
ini_set("display_errors", "1");  
require_once("./config.php");

//echo $cwd."<br>";

if(!is_writable($cwd)) trigger_error("Nav rakstīšanas atļaujas darba direktorijā!", E_USER_ERROR);

$tables = array(
    "db_cat" => "cat;files;data;comments",
    "top_image" => "top_images",
    "rnd_pic" => "",
    "db_lessons" => "lessons",
    "db_lessons_ch" => "lessons_ch",
    "db_event" => "events",
    "db_poll" => "poll;poll_votes",
    "db_user" => "user;permission",
    "db_graphics_conf" => "graphics_conf",
    "db_graphics" => "graphics",
    "db_caption" => "caption"
);

$tables_primary_lang = array("user");
$tables_secondary_lang = array();

$dirs = array(
    "db_cat" => "assets;tmp;assets/images;assets/files",
    "top_image" => "assets/top-images;assets/top-images/pixelated;assets/top-images/thumbs",
    "rnd_pic" => "assets/rnd-pic;assets/rnd-pic/thumbs",
    "db_graphics_conf" => "assets/graphics;assets/graphics/thumbs;assets/graphics/thumbs/".$GLOBALS["thumbsize"],
    "db_graphics_conf" => "assets/graphics;assets/graphics/thumbs;assets/graphics/thumbs/".$GLOBALS["thumbsize"]
);

foreach($modules as $module => $title){
    if(isset($tables[$module])){
        $list = explode(";", $tables[$module]);
        foreach($list as $key => $table){
            if(file_exists($cwd."inc/sql/tbl_".$table.".sql")){
                $query = file_get_contents($cwd."inc/sql/tbl_".$table.".sql");
                echo "\tVeidoju tabulu `".$table."`:<br>\n";
                echo "<pre>".$query."</pre><br><br>\n";
                foreach($supported_langs as $lang){
                    if(in_array($table, $tables_primary_lang) && $lang == $db_lang_primary) $sql->query(str_replace("_lva", "_".$lang, $query));
                    elseif(in_array($table, $tables_secondary_lang) && $lang != $db_lang_primary) $sql->query(str_replace("_lva", "_".$lang, $query));
                    elseif(!in_array($table, $tables_secondary_lang) && !in_array($table, $tables_primary_lang)) $sql->query(str_replace("_lva", "_".$lang, $query));
                }
            }
            if(file_exists($cwd."inc/sql/trg_".$table.".sql")){
                $query = file_get_contents($cwd."inc/sql/trg_".$table."_drop.sql");
                foreach($supported_langs as $lang){
                    if(in_array($table, $tables_primary_lang) && $lang == $db_lang_primary) $sql->query(str_replace("_lva", "_".$lang, $query));
                    elseif(in_array($table, $tables_secondary_lang) && $lang != $db_lang_primary) $sql->query(str_replace("_lva", "_".$lang, $query));
                    elseif(!in_array($table, $tables_secondary_lang) && !in_array($table, $tables_primary_lang)) $sql->query(str_replace("_lva", "_".$lang, $query));
                }
                $query = file_get_contents($cwd."inc/sql/trg_".$table.".sql");
                echo "\tVeidoju trigeri tabulai `".$table."`:<br>\n";
                echo "<pre>".$query."</pre><br><br>\n";
                foreach($supported_langs as $lang){
                    if(in_array($table, $tables_primary_lang) && $lang == $db_lang_primary) $sql->query(str_replace("_lva", "_".$lang, $query));
                    elseif(in_array($table, $tables_secondary_lang) && $lang != $db_lang_primary) $sql->query(str_replace("_lva", "_".$lang, $query));
                    elseif(!in_array($table, $tables_secondary_lang) && !in_array($table, $tables_primary_lang)) $sql->query(str_replace("_lva", "_".$lang, $query));
                }
                //var_dump($sql);
            }
            if(file_exists($cwd."inc/sql/ins_".$table.".php")){
                /*$query = file_get_contents($cwd."inc/sql/ins_".$table.".sql");
                $keys = array_keys($modules);
                $query = str_replace("#perms#", implode(";", $keys), $query);
                echo "\tIevietoju datus `".$table."`:<br>\n";
                echo "<pre>".$query."</pre><br><br>\n";
                $sql->query($query);*/
                include($cwd."inc/sql/ins_".$table.".php");
            }
        }
    }
    if(isset($dirs[$module])){
        $list = explode(";", $dirs[$module]);
        foreach($list as $key => $dir){
            //echo $cwd.$dir;
            if(!is_dir($cwd.$dir)){
                echo "Veidoju direktoriju: `".$dir."` <br>";
                mkdir($cwd.$dir, 0775, true) or die("Neizdevās izveidot direktoriju `".$dir."`!");
                chmod($cwd.$dir, 0775);
            }
        }
    }
}
?>
