<?

include_once("../../config.php");
$voted = 0;
//var_dump($_REQUEST);
if(!isset($_REQUEST["poll"])) die("Kļūda: nav norādīta aptauja!");
if(!isset($_REQUEST["opt"])) die("Kļūda: nav norādīts variants!");
if(!isset($_COOKIE)) die("Kļūda: balsot iespējams tikai tad, ja pārlūks pieņem sīkdatnes (cookies)!");
$stmt = $sql->prepare("select cook from poll_".$db_lang." where id = ?");
$stmt->bind_param("i", $_REQUEST["poll"]);
$stmt->bind_result($cook);
$stmt->execute();
$stmt->fetch();
$stmt->close();
if(!$cook) die("Kļūda: aptauja nav atrasta!");
if(!isset($_COOKIE[$cook])) die("Kļūda: nav atrasta aptaujas sīkdatne!");
if($_COOKIE[$cook]) die("Kļūda: no šī datora jau ir veikts balsojums!");
$stmt = $sql->prepare("update poll_votes_".$db_lang." set votes = votes + 1 where id = ?");
$stmt->bind_param("i", $_REQUEST["opt"]);
$stmt->execute();
$stmt->close();

setcookie($cook, 1, time()+120*24*3600, "/", "rchv.lv");
$_COOKIE[$cook] = 1;
$poll = new pub_poll();
echo $poll->poll();
?>
