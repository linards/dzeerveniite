<?

class pub_map extends pub{
    public $map;
    protected $sql;
    protected $out;
    public function __construct($dummy = 0){
        $this->sql = $GLOBALS["sql"];
        $this->title = "Lapas karte";
        $this->childs();
        $this->map = "\t\t<h2>Lapas karte</h2>\n".implode("\n", $this->out);
    }
    
    protected function childs($parent = 0, $prev = array()){ //var_dump($prev);
        $out = array();
        $stmt = $this->sql->prepare("select id, title, cipher from cat_".$GLOBALS["db_lang"]." where parent = $parent order by sortorder");
        $stmt->bind_result($id, $title, $cipher); $stmt->execute(); $stmt->store_result();
        $stmtD = $this->sql->prepare("select count(*) as cnt from data_".$GLOBALS["db_lang"]." where parent = ?");
        $stmtD->bind_param("i", $id); $stmtD->bind_result($cnt);
        while($stmt->fetch()){
            $next = $prev;
            $next["titles"][] = $title;
            $next["ciphers"][] = $cipher;
            $stmtD->execute(); $stmtD->store_result(); $stmtD->fetch();
            if($cnt) $this->out[] = "\t\t\t\t<a class=\"map\" href=\"/rchv/".implode("/", $next["ciphers"])."\">".implode(" &gt; ", $next["titles"])."</a>";
            $this->childs($id, $next);
        }
        $stmtD->close();
        $stmt->close();
        //return $out;
    }
    
    public function contents(){
        return $this->map;
    }

}


?>
