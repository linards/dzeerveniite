<?

class db_lessons extends db_entry{
    protected $lessons1;
    protected $lessons2;
    protected $lessons3;
    protected $lessons4;
    protected $lessons5;
    protected $obj_name = "Stundu saraksts";
    protected $mode = 1;
    protected $mode_selector = array(1 => "Pirmā maiņa", 2 => "Otrā maiņa");
    protected $cipher;
    
    public function __construct(){
        if(isset($_REQUEST["id"])){
            $this->id = $_REQUEST["id"];
        }
        $this->fields["mode"] = "mode";
        $this->pub_names["mode"] = "Maiņa";
        $this->pub_names["title"] = "Klase";
        $this->fields["lessons1"] = "lessons";
        $this->pub_names["lessons1"] = "pirmdiena";
        $this->fields["lessons2"] = "lessons";
        $this->pub_names["lessons2"] = "otrdiena";
        $this->fields["lessons3"] = "lessons";
        $this->pub_names["lessons3"] = "trešdiena";
        $this->fields["lessons4"] = "lessons";
        $this->pub_names["lessons4"] = "ceturtdiena";
        $this->fields["lessons5"] = "lessons";
        $this->pub_names["lessons5"] = "piektdiena";
        $this->fields["cipher"] = "cipher";
        
        parent::__construct("lessons",$this->id);
        //if($this->id) $this->fields["title"] = "none";
    }
    
    public function cl_sel(){
        $out = "\t<h2>Stundu saraksts</h2>\n\t<form name=\"classSel\" method=\"get\" action=\"$_SERVER[SCRIPT_NAME]\">\n\t\t<input type=\"hidden\" name=\"op\" value=\"$_REQUEST[op]\" />\n\t\t\t<select onChange=\"document.forms['classSel'].submit();\" name=\"id\">\n".$this->select("classes")."\t\t\t</select>\n\t</form>\n";
        return $out;
    }
    
    protected function before_delete(){
        return true;
    }
}

?>
