<?

class pub_poll extends pub{
    protected $start;
    protected $end;
    protected $cook;
    protected $voted = 0;
    protected $options = array();
    protected $sum = 0;
    
    public function __construct(){
        $this->fields[] = "start";
        $this->fields[] = "end";
        $this->fields[] = "cook";
        parent::__construct();
        if(!$this->id){
            $res = $this->sql->query("select id from ".$this->table."_".$GLOBALS["db_lang"]." where start <= curdate() and end >= curdate() order by start limit 1"); //echo "select id from ".$this->table."_".$GLOBALS["db_lang"]." where start >= curdate() and end <= curdate() order by start limit 1";
            //var_dump($res);
            if($res->num_rows){
                $row = $res->fetch_assoc();
                $this->id = $row["id"];
            }
            else{
                $res = $this->sql->query("select id from ".$this->table."_".$GLOBALS["db_lang"]." where end <= curdate() order by start desc limit 1"); //echo "select id from ".$this->table."_".$GLOBALS["db_lang"]." where end <= curdate() order by start desc limit 1";
                //var_dump($res);
                if($res->num_rows){
                    $row = $res->fetch_assoc();
                    $this->id = $row["id"];
                    $this->voted = 2;
                }
            }
            if($this->id) $this->load();
            if(isset($_COOKIE[$this->cook]) && !$this->voted) $this->voted = $_COOKIE[$this->cook];
            //var_dump($_COOKIE[$this->cook]);
        }
        if($this->id){
            $stmt = $this->sql->prepare("select id, title, votes from poll_votes_".$GLOBALS["db_lang"]." where parent = ".$this->id." order by sortorder");
            $stmt->bind_result($id, $title, $votes);
            $stmt->execute();
            while($stmt->fetch()){
                $this->options[$id] = array($title, $votes);
                $this->sum += $votes;
            }
        }
    }
    
    public function poll(){
        if(!$this->id) return null;
        //var_dump($this->voted);
        if($this->voted < 2) setcookie($this->cook, $this->voted, time()+120*24*3600, "/", "rchv.lv");
        $out = "Aptauja\n\t\t\t<div class=\"poll-title\">".$this->title."</div>\n\t\t\t<form method=\"post\" name=\"poll\" action=\"/rchv/\" onSubmit=\"cast_vote(document.forms['poll'].elements['poll-".$this->id."'], $this->id); return false;\">";
        foreach($this->options as $id => $val){
            if(!$this->voted){
                $out .= "\t\t\t\t<input type=\"radio\" name=\"poll-".$this->id."\" value=\"$id\" id=\"poll-".$this->id."-$id\"><label for=\"poll-".$this->id."-$id\">".$val[0]."</label><br />";
            }
            else{
                $proc = 0; $length = 0;
                if($this->sum){
                    $proc = round(100 * $val[1] / $this->sum);
                    $length = round(150 * $val[1]/ $this->sum);
                }
                $out .= "\t\t\t\t".$val[0]."<br /><img src=\"/img/white.png\" alt=\"$proc %\" height=\"10\" width=\"$length\"> $proc%<br />\n";
            }
        }
        $out .= "\t\t\t</form>\n";
        if(!$this->voted) $out .= "\t\t\t\t<input class=\"poll-button\" type=\"button\" onClick=\"cast_vote(document.forms['poll'].elements['poll-".$this->id."'], $this->id);\" value=\"Balsot\">\n\t\t\t</form><br />\n";
        else $out .= "\t\t\tKopā nobalsojuši: $this->sum<br />\n";
        $out .= "\t\t\tAptauja izveidota: ".format_date($this->start)."<br /><a href=\"/rchv/!p\">Aptauju arhīvs</a>\n";
        return $out;
    }
    
    public function contents(){
        $out = "<h2>Aptauju arhīvs</h2>";
        $stmt = $this->sql->prepare("select id, title, start from ".$this->table."_".$GLOBALS["db_lang"]." where start <= curdate() order by start desc limit ".$this->s.", ".$GLOBALS["ipp"]);
        $stmt->bind_result($id, $title, $start); $stmt->execute(); $stmt->store_result();
        while($stmt->fetch()){
            $out .= "\t\t<div class=\"data-block-poll\"><h3>$title</h3>\n";
            $stmt2 = $this->sql->prepare("select title, votes from poll_votes_".$GLOBALS["db_lang"]." where parent = $id order by sortorder");
            //var_dump($stmt2);
            $stmt2->bind_result($otitle, $votes);
            $stmt2->execute(); $options = array(); $sum = 0;
            while($stmt2->fetch()){
                $options[] = array($otitle, $votes);
                $sum += $votes;
            }
            $stmt2->close();
            //var_dump($options);
            foreach($options as $val){
                $proc = 0; $length = 0;
                if($sum){
                    $proc = round(100 * $val[1] / $sum);
                    $length = round(500 * $val[1]/ $sum);
                }
                $out .= "\t\t\t".$val[0]."<br /><img src=\"/img/black.png\" height=\"10\" width=\"$length\"> $proc%<br />\n";
            }
            $out .= "\t\t\tKopā nobalsojuši: $sum<br />\n";
            $out .= "\t\t\tAptauja izveidota: ".format_date($start)."\n";
            $out .= "\t\t</div>\n";
        }
        $res = $this->sql->query("select count(*) as cnt from ".$this->table."_".$GLOBALS["db_lang"]." where start <= curdate()");
        $row = $res->fetch_assoc(); //var_dump($row);
        $out .= other_items($row["cnt"]);
    return $out;
    }
}

?>
