<?

class db_poll_votes extends db_entry{
    protected $obj_name = "Atbildes variants";
    protected $sortorder;

    public function __construct(){
        if(isset($_REQUEST["id"])){
            $this->id = $_REQUEST["id"];
        }
        $this->fields["sortorder"] = "select";
        $this->pub_names["sortorder"] = "Novietots aiz";
        parent::__construct("poll_votes",$this->id);
    }
    
    protected function before_delete(){
        return true;
    }

}

?>
