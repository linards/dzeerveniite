<?
class db_entry{
    
    protected $obj_title = null;
    protected $table = null;
    protected $table_lang;
    protected $lang;
    protected $id = 0;
    protected $title = null;
    protected $parent = 0;
    protected $cipher;
    protected $op;
    
    protected $sql;

    protected $out;
    protected $road_parts = array();
    
    protected $fields = array("id" => "hidden", "parent" => "hidden", "title" => "text");
    protected $fields_ndb = array("op" => "hidden");
    protected $pub_names = array("title" => "Nosaukums");
    protected $mandatory = array();
    protected $fvalue;
    
    protected $integers = array("id", "parent", "integer");
    
    protected $query;
    protected $qparts = array();
    protected $qvals = array();
    protected $types;
    protected $stmt;
    
    protected $row = array();
    protected $i;
    
    protected $open_line = "\t\t<div class=\"db_field\">\n";
    protected $open_field = "\t\t\t<div class=\"db_field_title\">";
    protected $open_value = "</div>\n\t\t\t<div class=\"db_field_value\">";
    protected $close_line = "\t\t</div>\n";
    protected $close_value = "</div>\n";
    
    public function __construct($table, $id = "", $save = 1){
        //$_SESSION["message"] = array("alert-success", "Kaut kas no ".$this->obj_name);
        $this->sql = $GLOBALS["sql"];
        //var_dump($this->sql);
        $this->table = $table;
        $this->op = get_class($this);
        if(in_array($this->op, $GLOBALS["modules_primary"])) $this->lang = $GLOBALS["db_lang_primary"];
        else if(in_array("db_".$this->table, $GLOBALS["modules_primary"])) $this->lang = $GLOBALS["db_lang_primary"];
        else $this->lang = $GLOBALS["db_lang"];
        $this->table_lang = $this->table."_".$this->lang;
        //var_dump($this->op);
        if(get_class($this) <> "db_lessons") settype($id,"integer");
        if($id && isset($_REQUEST["data"]) && get_class($this) <> "db_data"){
            $id = $this->get_id_from_child($_REQUEST["id"]);
            }
        if($id){ $this->id = $id; }
        if($id){ $this->load(); }
        if(!$this->parent) $this->parent = 0;
        if($_POST && isset($_REQUEST["op"]) && $save && !isset($_REQUEST["nosave"])){
            //var_dump($GLOBALS["pub_user"]);
            /*if(isset($_REQUEST["data"])){
                if($_REQUEST["data"] == get_class($this)){
                    foreach($this->fields as $key => $value){
                        if(isset($_POST[$key])) $this->$key = $_POST[$key];
                    }
                    $this->save();
                }                
            }
            elseif(isset($_REQUEST["op"])){*/
                if($_REQUEST["op"] == get_class($this)){
                    foreach($this->fields as $key => $value){
                        if(isset($_POST[$key])) $this->$key = $_POST[$key];
                    }
                    $this->save();
                }                
            /*}
            else{
                foreach($this->fields as $key => $value){
                    if(isset($_POST[$key])) $this->$key = $_POST[$key];
                }
                $this->save();
            }*/
        }
        if(isset($_REQUEST["do"]) && $save){
            if($_REQUEST["do"] == "delete"){
                if(isset($_REQUEST["op"])){
                    if($_REQUEST["op"] == get_class($this)){
                        try{
                            $redirect = $this->delete();
                            //header("Location: $redirect");
                            //exit;
                        } catch (Exception $e) {
                            echo '<div class="error"><b>Kļūda!</b> ',  $e->getMessage(), "</div>\n";
                        }
                    }
                }
            }
            elseif($_REQUEST["do"] == "move_so"){
                echo $this->move_so();
                exit;
            }
        }
        //if($_POST) $this->save();
        $this->populate_road_parts($this->id);
    }
    
    protected function field($type, $title, $name = "", $id = "", $value = "", $new = 0, $delete = ""){
        $out = "";
        $opener = $this->open_line.$this->open_field.$title.$this->open_value;
        $closer = $this->close_value.$this->close_line;
        switch($type){
            case "hidden":
                $out .= "\t\t<input type=\"hidden\" id=\"$id\" name=\"$name\" value=\"".$value."\" />\n";
                break;
            case "text": //echo "$name => $value";
                $out .= $opener."<input class=\"$type\" type=\"text\" maxlength=\"255\" name=\"$name\" value=\"".htmlspecialchars($value)."\" />".$closer;
                break;
            case "integer": //echo "$name => $value";
                $out .= $opener."<input class=\"$type\" type=\"text\" maxlength=\"255\" name=\"$name\" value=\"".htmlspecialchars($value)."\" />".$closer;
                break;
            case "add-text": //echo "$name => $value";
                $num = "num_".$name;
                //echo $num; var_dump($this->$num);
                //for($i = 0; $i < count($value); $i++){
                $i = 0;
                foreach($this->options as $id => $option){
                    $cnt = $i + 1;
                    if($option){
                        $disabled = "disabled";
                        $edit = "<a href=\"$_SERVER[SCRIPT_NAME]?op=db_poll_votes&id=$id\">Labot</a>";
                    }
                    else{
                        $disabled = ""; $edit = "";
                    }
                    $out .= $this->open_line.$this->open_field.$cnt.". ".$title.$this->open_value."<input class=\"$type\" $disabled type=\"text\" maxlength=\"255\" name=\"".$name."[$i]\" value=\"".htmlspecialchars($option)."\" /> $edit".$closer;
                    $i++;
                }
                $out .= "\t\t\t<script type=\"text/javascript\">var $num = $i;</script>\n";
                $out .= "\t\t<div class=\"db_field\" id=\"data-$name\"></div>\n\t\t<div class=\"db_field\"><a href=\"#\" onClick=\"add_text('data-$name','$num'); return false;\">Pievienot jaunu atbildes variantu</a></div>\n";
                break;
            case "select": //echo "$name => $value";
                $out .= $opener."<select class=\"$type\" name=\"$name\">\n";
                $out .= $this->select($name, $new);
                $out .= "\t\t\t</select>".$closer;
                break;
            case "mode": //echo "$name => $value";
                $out .= $opener."<select class=\"$type\" name=\"$name\">\n";
                $out .= $this->select($name, $new);
                $out .= "\t\t\t</select>".$closer;
                break;
            case "textarea":
                $out .= $this->open_line.$title."<br /><textarea class=\"ckeditor wm\" name=\"$name\">".$value."</textarea>".$this->close_line;
                break;
            case "password":
                if(!$this->id){
                    $out .= $opener."<input class=\"$value\" type=\"password\" name=\"$name\" value=\"\" />".$closer;
                }
                elseif(isset($_REQUEST["pw"])){
                    //$out .= $this->open_line.$this->open_field.$title." (vecā)".$this->open_value."<input class=\"$value\" type=\"password\" name=\"$name-old\" value=\"\" />".$closer;
                    $out .= $this->open_line.$this->open_field.$title." (jaunā)".$this->open_value."<input class=\"$type\" type=\"password\" name=\"$name-new\" value=\"\" />".$closer;
                    $out .= $this->open_line.$this->open_field.$title." (jaunā atk.)".$this->open_value."<input class=\"$type\" type=\"password\" name=\"$name\" value=\"\" />".$closer;
                }
                else{
                    $out .= $this->open_line."<a href=\"$_SERVER[SCRIPT_NAME]?op=$_REQUEST[op]&id=$this->id&pw=1\">Mainīt lietotāja paroli</a>".$this->close_line;
                }
                break;
            case "lessons":
                $ls = "\t\t\t\t<textarea class=\"lesson-times-$this->mode\" id=\"lesson-times-$name\" disabled>";
                //var_dump($this->mode);
                foreach($GLOBALS["times"][$this->mode] as $tkey => $time){
                    if($time) $ls .= "$tkey. ".str_replace(array("<sup>","</sup>"), array(":",""),$time)."\n";
                }
                $ls .= "\t\t\t\t</textarea>";
                $pos = strpos($value, "\n"); //echo $pos;
                if($pos == 1){
                    $value = " ".$value;
                }
                $out .= $this->open_line.$title."<br />$ls<textarea id=\"$name\" class=\"lessons-$this->mode\" name=\"$name\">".htmlspecialchars($value)."</textarea>".$this->close_line;
                break;
            case "date":
                $out .= $opener."<input class=\"$type\" id=\"$name\" type=\"text\" name=\"$name\" value=\"".htmlspecialchars($value)."\" /><br /><iframe scrolling=\"no\" class=\"form-calendar\" src=\"inc/php/form-calendar.php?date=".$value."&field=$name\"></iframe>".$closer;
                break;
            case "classes":
                //echo "LA LA LA";
                $out .= $this->open_line.$title."<br />";
                $this->radios();
                $out .= $this->close_line;
                break;
            case "multi": //echo "muuuultiii!!!!";
                if(in_array(get_class($this), $GLOBALS["pub_user"]->allowed)){
                    $out .= $this->open_line.$title."<br />";
                    $out .= $this->multi($name);
                    $out .= $this->close_line;
                }
                break;
            case "options":
                $out .= $opener;
                $out .= $this->options($name, $value);
                $out .= $closer;
                break;
            case "save":
                $out .= $this->open_line.$this->open_field."<input class=\"btn btn-lg btn-success\" type=\"submit\" value=\"$title\" />".$this->open_value.$delete.$closer;
        }
    return $out;
    }
    
    public function edit($new = 0){
        // veidosim formu, kurā labo informāciju
        if(method_exists($this,"cl_sel")) $cl_sel = $this->cl_sel();
        else $cl_sel = "";
        if(!$new){
            if($this->id) $this->out = $cl_sel."\t<div class=\"header2\">".$this->obj_name.": labot</div>\n";
            else $this->out = $cl_sel."\t<div class=\"header2\">".$this->obj_name.": pievienot jaunu</div>\n";
        }
        $this->out .= "\t<form method=\"post\" id=\"data-form-$new\" name=\"db_entry\" action=\"$_SERVER[SCRIPT_NAME]\" enctype=\"multipart/form-data\">\n";
        if(method_exists($this,"before_edit")) $this->before_edit();
        if(isset($this->abort)) return null;
        foreach(($this->fields+$this->fields_ndb) as $key => $value){
            //echo "$key => $value";
            if(!$new || $key == "op") $this->fvalue = $this->$key;
            elseif($key == "parent") $this->fvalue = $this->id;
            else $this->fvalue = "";
            $this->out .= $this->field($value, @$this->pub_names[$key], $key, "$key-$new", $this->fvalue, $new);
        }
        if($this->id && !$new && in_array(get_class($this), $GLOBALS["pub_user"]->allowed)){
            //if(isset($_REQUEST["id"]) && isset($_REQUEST["op"])) $op = "&op=$_REQUEST[op]";
            //else $op = "";
            $delete = "<a class=\"btn btn-lg btn-danger\" href=\"$_SERVER[SCRIPT_NAME]?op=".$this->op."&id=".$this->id."&do=delete\" onclick=\"return confirm('Vai tiešām dzēst? Neatgriezeniska darbība!');\">Dzēst ierakstu</a>";
        }
        else $delete = "&nbsp;";
        $this->out .= $this->field("save", "Saglabāt", 0, 0, 0, 0, $delete)."</form>";
        //echo get_class($this);
        if($this->id && !$new){
            //echo "LA LA LA";
            if(isset($_REQUEST["id"]) && isset($_REQUEST["op"])) $op = "&op=$_REQUEST[op]";
            else $op = "";
            if(get_class($this) == "db_cat" && !isset($_REQUEST["add"])){
                if($this->writable("cat")){
                    $this->out .= "\t\t<div class=\"header2\">Pievienot jaunu sadaļu:</div>\n";
                    $this->edit(1);
                }
                if($this->writable("data")) $this->add_data();
            }
        }
        if(method_exists($this, "after_edit") && !$new) $this->after_edit();
        /*if(isset($_REQUEST["_"])){
            if(isset($GLOBALS["updates"][$_REQUEST["_"]])){
                $params = $GLOBALS["updates"][$_REQUEST["_"]];
                $this->out .= "\t\t\t<script>\n";
                //$this->out .= "\t\t\t\t$('$params[1]:first').load($_SERVER[SCRIPT_NAME]?update=$params[0]);\n";
                //$this->out .= "\t\t\t\t$('$params[1]:first').hide();\n";
                $this->out .= "\t\t\t\talert('yo');\n";
                $this->out .= "\t\t\t</script>\n";
            }
        }*/
        return $this->out;
    }
    
    protected function options($name, $value = null){
        $name_org = $name;
        $name = preg_replace('/\[[1-9][0-9]{0,15}\]/', '', $name);
        //echo $name;
        $array = $name."_vals";
        $out = "\t\t\t\t<select name=\"$name_org\">\n";
        foreach($this->$array as $key => $val){
            $sel = "";
            if($value === null) $value = $this->mode;
            if($value == $key) $sel = " selected";
            $out .= "\t\t\t\t\t<option value=\"$key\"$sel>$val</option>\n";
        }
        $out .= "\t\t\t\t</select>\n";
        //echo $out;
        return $out;
    }
    
    protected function load(){
        // ielādēsim rindiņu no DB, bet pagaidām nav nekā
        $query = "select * from ".$this->table_lang." where id = '$this->id'"; //echo $this->query;
        $res = $this->sql->query($query);
        $row = $res->fetch_assoc();
        //var_dump($row);
        foreach($this->fields as $key => $value){ if(isset($row[$key])) $this->$key = $row[$key]; }
    }
    
    protected function multi($arr){
        $i = 0; $out = "";
        foreach($GLOBALS[$arr] as $key => $val){
            $show = true;
            if(isset($GLOBALS[$arr."_primary"])){
                if(in_array($key, $GLOBALS[$arr."_primary"]) && $GLOBALS["db_lang"] <> $GLOBALS["db_lang_primary"]) $show = false;
            }
            if(in_array($key, $this->allowed)) $chk = " checked";
            else $chk = "";
            if($val && $show) $out .= "\t\t\t\t<label for=\"$arr-$key\"><input type=\"checkbox\" name=\"".$arr."[$i]\" id=\"$arr-$key\" value=\"$key\"$chk> $val</label><br />\n";
            $i++;
        }
        return $out;
    }
    
    protected function radios(){ //echo "radios";
        $classes = array();
        $res = $this->sql->query("select * from lessons_".$GLOBALS["db_lang"]); //var_dump($res);
        while($row = $res->fetch_assoc()){
            $temp = explode(".",$row["title"]);
            $classes[$row["id"]] = str_pad($temp[0], 2, 0, STR_PAD_LEFT).".".$temp[1];
        }
        asort($classes);
        $this->out .= "\t\t\t\t<script type=\"text/javascript\">cl_sel = $this->class;</script>\n";
        $this->out .= "\t\t\t\t<div class=\"form-classes\">\n";
        foreach($classes as $key => $val){
            $temp = explode(".",$val);
            if(!isset($prev)) $prev = $temp[0];
            if($prev <> $temp[0]) $this->out .= "\t\t\t\t</div>\n\t\t\t\t<div class=\"form-classes\">\n";
            $class = $temp[0];
            settype($class,"integer");
            $chkd = "";
            if($this->class == $key) $chkd = " checked=\"checked\"";
            $this->out .= "\t\t\t\t\t<input type=\"radio\" name=\"class\" onClick=\"lload_lessons(this.value);\" id=\"class-$key\" value=\"$key\"$chkd> <label for=\"class-$key\">$class.$temp[1]</label><br />\n";
            $prev = $temp[0];
        }
        $this->out .= "\t\t\t\t</div>";
    }
    
    protected function select($type, $new = 0){
        // sagatavo <option> ierakstus <select> tagam
        $out = "";
        if($type == "sortorder"){
            $out .= "\t\t\t\t<option value=\"1\">Sākumā</option>\n";
            if (!($stmt = $this->sql->prepare("select id, title, sortorder + 1 from ".$this->table_lang." where id <> ? and parent = ? order by $type"))){
               echo "Prepare failed: (" . $this->errno . ") " . $this->error;
            }
            if(!$new) $stmt->bind_param("ii", $this->id, $this->parent);
            else $stmt->bind_param("ii", $this->id, $this->id);
            $stmt->execute();
            $stmt->bind_result($id, $title, $sortorder);
            while ($stmt->fetch()) {
                //echo "$id, $title, $sortorder<br>";
                if($sortorder > $this->sortorder && $this->sortorder) $sortorder--;
                if(!$new && $sortorder == $this->sortorder) $out .= "\t\t\t\t<option value=\"$sortorder\" selected>$title</option>\n";
                else $out .= "\t\t\t\t<option value=\"$sortorder\">$title</option>\n";
            }
            $stmt->close();
        }
        elseif($type == "classes"){
            $out = "\t\t\t\t<option value=\"\">Izvēlēties klasi</option>\n";
            $res = $this->sql->query("select id, title from ".$this->table_lang." order by title");
            while($row = $res->fetch_assoc()){
                $sel = "";
                if($row["id"] == $this->id) $sel = " selected";
                $out .= "\t\t\t\t<option value=\"$row[id]\"$sel>$row[title]</option>\n";
            }
            //$out .= "\t\t\t\t<option value=\"new\">Pievienot jaunu klasi</option>\n";
        }
        elseif($type == "mode"){
            //var_dump($this->mode_selector);
            foreach($this->mode_selector as $key => $val){
                $sel = "";
                if($this->mode == $key) $sel = " selected";
                $out .= "\t\t\t\t<option value=\"$key\"$sel>$val</option>\n";
                //echo "$key => $val";
            }
        }
        return $out;
    }
    
    protected function save(){
        // saglabā objektu datubāzē
        //var_dump($_POST);
        //die();
        //if(get_class($this) == "db_cat") die("Kļūda: atkal gribam likt sadaļu...");
        if(!$this->parent) $this->parent = 0;
        if(method_exists($this,"before_save") && !isset($_REQUEST["nb"])) $this->before_save();

        foreach($this->mandatory as $mandatory){ //echo $mandatory;
            if(!$this->$mandatory) die("Kļūda: nav aizpildits obligātais lauks '".$this->pub_names[$mandatory]."'!");
        } //die();

        $this->query = ""; $this->types = ""; $this->qparts = array(); $this->qvals = array("");
        foreach($this->fields as $key => $value){
            //echo "$key => $value => ".$this->$key."<br />";
            if(($this->$key && $key <> "id" && $value <> "add-text") || $value == "cipher"){
                $bcrypt = new Bcrypt(12);
                //echo "$key => $value".$this->$key."<br>";
                $this->qparts[] = $key;
                if(in_array($key,$this->integers)) $this->qvals[0] .= "i";
                else $this->qvals[0] .= "s";
                if($value == "cipher"){
                    $test = no_lv_symbols($this->title); $i = 0;
                    while($test){
                        if(isset($this->parent)) $par = " and parent = $this->parent";
                        if(isset($this->date)) $par = " and `date` = '$this->date'";
                        $res = $this->sql->query("select * from ".$this->table_lang." where cipher = '$test'$par and id <> '$this->id'");
                        if(!$res->num_rows) break;
                        $test = substr(no_lv_symbols($this->title),0,250)."_".$i;
                        $i++;
                        }
                    $this->qvals[] = substr($test,0,255);
                    }
                elseif($value == "password" && isset($_REQUEST["$key-new"])){ //die("Esmu pie paroles!");
                    if(!$this->id) $this->qvals[] = $bcrypt->hash($this->$key);
                    else{
                        $np = $key."-new";
                        $op = $key."-old";
                        $resp = $this->sql->query("select password from ".$this->table_lang." where id = $this->id");
                        $rowp = $resp->fetch_assoc();
                        if($this->$key <> $_REQUEST[$np]) die("Kļūda: jaunās paroles nesakrīt!");
                        $this->qvals[] = $bcrypt->hash($this->$key);
                    }
                }
                elseif($value == "password" && !$this->id) $this->qvals[] = $bcrypt->hash($this->$key);
                elseif($this->$key) $this->qvals[] = $this->$key;
                else $this->qvals = "null";
            }
        }
    
        if($this->id){
            // atjaunina eksistējošu ierakstu
            // noskaidrojam veco sortorder vērtību
            if(isset($this->sortorder)){
            $this->query = "select sortorder from ".$this->table_lang." where id = ".$this->id;
                $res = $this->sql->query($this->query);
                $this->row = $res->fetch_assoc();
            
                // atjauninam citu ierakstu sortorder vērtības
                if($this->row["sortorder"] > $this->sortorder) $this->query = "update ".$this->table_lang." set sortorder = sortorder + 1 where sortorder < ".$this->row["sortorder"]." and sortorder >= '".$this->sortorder."' and parent = ".$this->parent;
                elseif($this->row["sortorder"] < $this->sortorder) $this->query = "update ".$this->table_lang." set sortorder = sortorder - 1 where sortorder > ".$this->row["sortorder"]." and sortorder <= '".$this->sortorder."' and parent = ".$this->parent;
                $this->sql->query($this->query);
                //echo $this->query;
            }
            
            $this->query = "update ".$this->table_lang." set `".implode("` = ?,`",$this->qparts)."` = ? where id = ".$this->id;
        }
        else{
            // veido jaunu ierakstu
            if(isset($this->sortorder)){
                $this->query = "update ".$this->table_lang." set sortorder = sortorder + 1 where sortorder >= '".$this->sortorder."' and parent = ".$this->parent; //die($this->query);
                $this->sql->query($this->query);
            }
            $this->query = "insert into ".$this->table_lang." (`".implode("`,`",$this->qparts)."`) values (".str_pad("?",count($this->qparts)*2-1,",?").")";
        }
        //if(get_class($this) == "db_data"){ var_dump($this->qvals); die($this->query); }
        //var_dump($this->query); die();
        $stmt = $this->sql->prepare($this->query);
        //if(get_class($this) == "db_poll"){ var_dump($stmt); echo $this->error; die(); }
        //$stmt->bind_param($this->qvals);
        call_user_func_array(array($stmt, 'bind_param'), makeValuesReferenced($this->qvals));
        //var_dump($stmt); die();
        $stmt->execute();
        if(!$this->id){
            $this->id = $this->sql->insert_id;
            $_SESSION["message"] = array("alert-success", "Pievienots/-a ".strtolower($this->obj_name)." '".$this->title."'!");
            }
        else $_SESSION["message"] = array("alert-success", "Labots/-a ".strtolower($this->obj_name)." '".$this->title."'!");
        $stmt->close();
        if(method_exists($this, "after_save")) $this->after_save();
        //if(isset($_REQUEST["op"])) header("Location: $_SERVER[SCRIPT_NAME]?op=$_REQUEST[op]&id=$this->id");
        //elseif(get_class($this) == "db_data") header("Location: $_SERVER[SCRIPT_NAME]?id=$this->id&data=db_data");
        //else header("Location: $_SERVER[SCRIPT_NAME]?id=$this->id");
        //die("all done, redirecting ...");
        //var_dump($_SESSION);
        //die();
        header("Location: $_SERVER[SCRIPT_NAME]?op=".get_class($this)."&id=$this->id".$GLOBALS["aq"]);
        exit;
    }
    
    protected function before_delete(){
        return true;
    }
    
    protected function delete(){
        //die("deleting ... $this->id of ".get_class($this));
        $test = $this->before_delete();
        if($test === true){
            if($stmt = $this->sql->prepare("delete from ".$this->table_lang." where id = ?")){
                $stmt->bind_param('i', $this->id); $stmt->execute(); $stmt->close();
                //die("Finished deleting cat");
                if(property_exists($this,"sortorder")){
                    $stmt = $this->sql->prepare("update ".$this->table_lang." set sortorder = sortorder - 1 where sortorder > ? and parent = ?");
                    $stmt->bind_param('ii', $this->sortorder, $this->parent); $stmt->execute(); $stmt->close();
                }
                if(isset($this->parent)){
                    if($this->parent){
                        $parent = "&id=".$this->parent;
                    }
                }
                //die("Finished all ready to redirect");
                if($_REQUEST["op"] == "db_data") $_REQUEST["op"] = "db_cat";
                elseif($_REQUEST["op"] == "db_poll_votes") $_REQUEST["op"] = "db_poll";
                //die("Finished all ready to redirect");
                //if($_REQUEST["op"] == "top_image") header("Location: /inc/php/unlink.php?op=$_REQUEST[op]&img=$this->img".$GLOBALS["aq"]);
                //header("Location: $_SERVER[SCRIPT_NAME]?op=$_REQUEST[op]$parent".$GLOBALS["aq"]);
                //die("Ieraksts dzēsts.<br><a href=\"$_SERVER[SCRIPT_NAME]?op=$_REQUEST[op]$parent\">Turpināt darbu</a>");
                $_SESSION["message"] = array("alert-block", "Dzēsts/-a ".strtolower($this->obj_name)." '".$this->title."'!");
                header("Location: $_SERVER[SCRIPT_NAME]?op=$_REQUEST[op]$parent".$GLOBALS["aq"]);
                exit;
            } else {
                throw new Exception('Dzēšana neizdevās: nepareizi noformēts datubāzes pieprasījums!');
            }
        }
        else $_SESSION["message"] = array("alert-error", $test);
    }
    
    public function show_id(){
        return $this->id;
    }

    protected function populate_road_parts(){
        //$road_parts = array();
        /*if($_REQUEST["id"] && isset($_REQUEST["data"])){
            $this->road_parts[] = $this->get_child_title($_REQUEST["id"]);
        }*/
        switch(get_class($this)){
            case "db_cat_menu":
                break;
            case "db_cat":
                if($this->id) $this->road_parts_cat($this->id);
                break;
            case "db_data":
                if($this->id) $this->road_parts[] = $this->title;
                else $this->road_parts[] = $this->obj_name;
                $this->road_parts_cat($this->parent);
                break;
            case "db_poll_votes":
                $this->road_parts[] = "<a class=\"road\" href=\"$_SERVER[SCRIPT_NAME]?op=db_poll\">Aptauja</a>";
                $res = $this->sql->query("select id, title from poll_".$GLOBALS["db_lang"]." where id = $this->parent");
                $row = $res->fetch_assoc();
                $this->road_parts[] = "<a class=\"road\" href=\"$_SERVER[SCRIPT_NAME]?op=db_poll&id=$row[id]\">$row[title]</a>";
                $this->road_parts[] = $this->title;
                break;
            default:
                $this->road_parts[] = "<a class=\"road\" href=\"$_SERVER[SCRIPT_NAME]?op=".get_class($this)."\">".$this->obj_name."</a>";
                if($this->title) $this->road_parts[] = $this->title;
                if(isset($this->img)){
                    if($this->img) $this->road_parts[] = $this->img;
                }
                break;
        }
        array_unshift($this->road_parts,"<a class=\"road\" href=\"$_SERVER[SCRIPT_NAME]?op=db_cat&id=0\">".$GLOBALS["project_title"][$GLOBALS["db_lang"]]."</a>");
    }
    
    protected function road_parts_cat($id){
        do{
           $res = $this->sql->query("select * from cat_".$GLOBALS["db_lang"]." where id = $id");
           $this->row = $res->fetch_assoc();
           //var_dump($this->row); die();
           array_unshift($this->road_parts,"<a class=\"road\" href=\"$_SERVER[SCRIPT_NAME]?op=db_cat&id=".$this->row["id"]."\">".$this->row["title"]."</a>");
            $id = $this->row["parent"];
        } while($id);
    }
    
    public function road(){
        if(count($this->road_parts) > 0){
            //return "<div class=\"road\">".implode(" &gt; ", $this->road_parts)."</div>";
            return "<ul class=\"breadcrumb\">\n\t\t\t<li>".implode("</li>\n\t\t\t<li>", $this->road_parts)."</li>\n\t\t</ul>";
        }
        else return null;
    }
    
    protected function move_so(){
        $move = explode("-", $_REQUEST["id"]);
        //var_dump($_REQUEST);
        $stmt = $this->sql->prepare("select id, parent, sortorder from ".$this->table_lang." where id = ?");
        $stmt->bind_param('i', $move[1]); $stmt->execute(); $stmt->bind_result($id, $parent, $so); $stmt->fetch();
        $stmt->close(); echo "$id - $parent - $so";
        $query = "update ".$this->table_lang." set sortorder = ".($so > $_REQUEST["so"] ? "sortorder + 1 where sortorder >= ".$_REQUEST["so"]." and sortorder < $so" : "sortorder - 1 where sortorder <= ".$_REQUEST["so"]." and sortorder > $so")." and parent = $parent";
        //echo "\n$query";
        $this->sql->query($query);
        $query = "update ".$this->table_lang." set sortorder = ".$_REQUEST["so"]." where id = $id";
        //echo "\n$query";
        $this->sql->query($query);
        return true;
    }
    
    protected function get_img_sig($file, $type = "txt"){
        $res = $this->sql->query("select $type from files_".$GLOBALS["db_lang"]." where file = '".filename_to_db($file)."'");
        $row = $res->fetch_assoc();
        if($row) return $row[$type];
        else if($type = "txt"){
            $pi = pathinfo($file);
            //var_dump($pi);
            return $pi["filename"];
        }
        else return null;
    }

    protected function call_ul($mode, $i, $width = 0, $height = 0){
        if($mode == "gal"){
            if(!$width) $width = $GLOBALS["imsize"];
            if(!$height){
                $title = "Pievienot jaunus attēlus (attēli tiek sakārtoti pēc faila nosaukuma; faila nosaukums tiks izmantots kā noklusētais paraksts; lieli attēli tiks automātiski samazināti līdz ".$width." px garākajā malā)";
                $height = $width;
            }
            else $title = "Pievienot jaunus attēlus (attēli tiek sakārtoti pēc faila nosaukuma; faila nosaukums tiks izmantots kā noklusētais paraksts; lieli attēli tiks automātiski samazināti līdz ".$width."&times;".$height." px, lielāki attēli, kam nebūs pievienota saite tiks palielināti uzklikšķinot.)";
            $filters = '{title : "Attēli", extensions : "jpg,JPG,jpeg,JPEG,gif,GIF,png,PNG"}';
            $resize = "{width : ".$GLOBALS["imsize"].", height : ".$GLOBALS["imsize"].", quality : ".$GLOBALS["thumbquality"]."}";
        }
        elseif($mode == "fil"){
            $title = "Pievienot jaunus failus (faili tiks sakārtoti pēc nosaukumiem):";
            $filters = '{title : "Visi faili", extensions : "*"}';
            $resize = "{}";
        }
        $out = "<script>$(function(){
                        var uploader = new plupload.Uploader({
                    		runtimes : 'html5',
                      		browse_button : 'pickfiles$i',
                       		max_file_size : '10mb',
                       		multiple_queues : true,
                       		url : '/inc/php/upload-handler.php',
                       		resize : $resize,
                       		filters : [
    		                    $filters
                       		],
                       	});
                        
                        //alert(uploader.context);
                        	
                       	uploader.bind('Init', function(up, params) {
                       		$('#uploader$i').html('$title' + '<input type=\"hidden\" name=\"files_id[$i]\" value=\"'+uploader.id+'\"><input type=\"hidden\" id=\"files-'+uploader.id+'\" name=\"file_sets['+uploader.id+']\">');
                       	});

                       	uploader.bind('FilesAdded', function(up, files) {
	                        $.each(files, function(i, file) {
                       			$('#uploader-'+up.id).append(
                       				'<div id=\"' + file.id + '\" class=\"file-added\">' +
                       				file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
                       			'</div>');
                       		});
                       	});
	
                       	uploader.bind('UploadFile', function(up, file) {
                       		$('<input type=\"hidden\" name=\"file-' + file.id + '\" value=\"' + file.name + '\">')
                       			.appendTo('#data-form');
                       	});

                       	uploader.bind('UploadProgress', function(up, file) {
                       		$('#' + file.id + ' b').html(file.percent + '%');
                       	});
                       	
                       	uploader.bind('FileUploaded', function(up, file, response) {
                       	    var obj = jQuery.parseJSON(response.response);
                       	    var val = $('#files-'+up.id).val();
                       	    $('#'+file.id).addClass('file-uploaded').removeClass('file-added');
                       	    $('#files-'+up.id).val(val+';'+file.id);
                   	        $('#uploader-'+up.id).append('<input type=\"hidden\" name=\"'+file.id+'\" value=\"'+obj.name+';'+file.name+'\">');
                        });

                       	$('#uploadfiles$i').click(function(e) {
                       		uploader.start();
                       		e.preventDefault();
                       	});

                        uploader.init();
                        $('#uploader$i').attr('id','uploader-'+uploader.id)
                        $('#files-'+window.field_num).attr('id','files-'+uploader.id);
                });
        </script>";
        return $out;
    }
    
}
?>
