<?

class pub_graphics extends pub{
    protected $conf;
    protected $conf_table;
    protected $db_lang;
    protected $conf_id;
    protected $mode;
    protected $width;
    protected $height;
    protected $number;
    protected $ids = array();
    protected $filenames = array();
    protected $alts = array();
    protected $urls = array();
    protected $blanks = array();
    
    public function __construct($conf = ""){
        $this->db_lang = $GLOBALS["db_lang"];
        $conf_arr = explode(";", $conf);
        $this->conf = $conf_arr[0];
        if(isset($conf_arr[1])){
            if($conf_arr[1] == "primary") $this->db_lang = $GLOBALS["db_lang_primary"];
        }
        parent::__construct();
        $this->fields = array("filename", "alt", "url", "blank");
        $this->conf_table = $this->table."_conf";
        if($conf) $this->load($conf); //echo "gr";
    }
    
    protected function load(){
        $stmt = $this->sql->prepare("select id, mode, width, height, number from ".$this->conf_table."_".$GLOBALS["db_lang_primary"]." where title = ?");
        $stmt->bind_param('s', $this->conf); $stmt->execute(); $stmt->store_result();
        if(!$stmt->num_rows) trigger_error("Kļūda: nav atrasta grafisko elementu konfihurācija '".$this->conf."'!",E_USER_WARNING);
        $stmt->bind_result($this->conf_id, $this->mode, $this-> width, $this->height, $this->number);
        $stmt->fetch(); $stmt->close();
        
        $res = $this->sql->query("select * from ".$this->table."_".$this->db_lang." where parent = '".$this->conf_id."' order by sortorder");
        while($row = $res->fetch_assoc()){
            $this->ids[] = $row["id"];
            foreach($this->fields as $field){
                if(isset($row[$field])){
                    $arr = $field."s";
                    $this->$arr += array($row["id"] => $row[$field]);
                }
            }
        }
        if($this->mode == 1) shuffle($this->ids);
        //var_dump($this->blanks);
    }
    
    public function show(){
        if($this->number > 0) $num = $this->number;
        else $num = count($this->ids);
        //echo $num;
        reset($this->ids); $cnt = 1;
        $out = ""; $path = "assets/graphics/";
        foreach($this->ids as $id){
            $hrefO = ""; $hrefC = ""; $fld = "";
            $info = getimagesize($GLOBALS["cwd"].$path.$this->filenames[$id]);
            if($info[0] > $this->width || $info[1] > $this->height){
                $fld = "thumbs/";
                //$hrefO = "<a class=\"highslide\" href=\"/".$path.$this->filenames[$id]."\" onClick=\"return hs.expand(this)\">";
                //$hrefC = "</a>\n\t\t\t\t<div class='highslide-caption'>".$this->alts[$id]."</div>";
            }
            if($this->urls[$id] && $this->urls[$id] != "http://"){
                $blank = "";
                if($this->blanks[$id]) $blank = "target=\"_blank\"";
                $hrefO = "<a href=\"".str_replace("/home-page", $GLOBALS["request_prefix"], $this->urls[$id])."\" $blank>"; $hrefC = "</a>";
            }
            $out .= "\t\t\t\t".$hrefO.img("/".$path.$fld.$this->filenames[$id], $this->conf, $this->alts[$id]).$hrefC."\n";
            if($cnt == $num) break;
            $cnt++;
        }
        return $out;
    }

}

?>
