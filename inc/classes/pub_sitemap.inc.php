<?

class pub_sitemap extends pub{
    protected $sitemap = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    
    public function __construct(){
        parent::__construct(); //echo $title;
    }
    
    protected function cat_data($parent, $ciphers){
        $query = "select id, date_format(created, '%Y-%m-%d'), title, value, cipher from data_".$GLOBALS["db_lang"]." where parent = ? order by created desc";
        $stmt = $this->sql->prepare($query); $stmt->bind_param("i", $parent); $stmt->execute(); $stmt->bind_result($id, $created, $title, $value, $cipher); $stmt->store_result();
        while($stmt->fetch()){
            $this->sitemap .= "\t<url>\n";
            $this->sitemap .= "\t\t<loc>".$GLOBALS["protocol"].$_SERVER["SERVER_NAME"].$GLOBALS["request_prefix"]."/".($GLOBALS["db_lang"] == $GLOBALS["db_lang_primary"] ? "" : $GLOBALS["db_lang"]."/").implode("/", $ciphers)."/".$cipher."</loc>\n";
            $this->sitemap .= "\t\t<lastmod>".$created."</lastmod>\n";

            $parts = get_data_parts($value);
            foreach($parts as $key => $part){
                if($part[0] == "gal"){
                    $path = $GLOBALS["cwd"]."assets/";
                    $path_pub = "/assets/";
    
                    $fld = $part[1];
                    $files = array();
            
                    if(file_exists($path.$fld)){
                        $dir = new DirectoryIterator($path.$fld);
                        foreach ($dir as $fileinfo) {
                            if ($fileinfo->isFile()) {
                                $files[] = $fileinfo->getFilename();
                            }
                        }
                    }
                    natcasesort($files); $tmp = "";

                    $stmt_im = $GLOBALS["sql"]->prepare("select txt, location from files_".$GLOBALS["db_lang"]." where file = ?");
                    $stmt_im->bind_param("s",$file_path);
                    $stmt_im->bind_result($txt, $location);
            
                    $i = 1;
                    foreach($files as $file){
                        $file_path = $part[1].rawurlencode($file);
                        $this->sitemap .= "\t\t<image:image>\n";
                        $this->sitemap .= "\t\t\t<image:loc>".$GLOBALS["protocol"].$_SERVER["SERVER_NAME"].$path_pub.$file_path."</image:loc>\n";
                        $file_path = filename_to_db($part[1].$file);
                        $stmt_im->execute(); $stmt_im->store_result(); //echo $file_path;
                        if($stmt_im->num_rows){
                            $stmt_im->fetch();
                            if(strlen($txt) > 2) $this->sitemap .= "\t\t\t<image:caption>".$txt."</image:caption>\n";
                            if(strlen($location) > 2) $this->sitemap .= "\t\t\t<image:geo_location>".$location."</image:geo_location>\n";
                        }
                        $this->sitemap .= "\t\t</image:image>\n";
                    }
                    $stmt_im->close();
                }
            }

            $this->sitemap .= "\t</url>\n";
        }
    }
    
    protected function subcats($parent = 0, $ciphers = array()){
        $query = "select id, title, cipher from cat_".$GLOBALS["db_lang"]." where parent = ? order by sortorder";
        $stmt = $this->sql->prepare($query); $stmt->bind_param("i", $parent); $stmt->execute(); $stmt->bind_result($id, $title, $cipher); $stmt->store_result();
        while($stmt->fetch()){
            $this->cat_data($id, $ciphers + array($cipher));
            $this->subcats($id, $ciphers + array($cipher));
        }
    }
    
    public function generate(){
        $this->sitemap .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"'."\n".
        ' xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">'."\n";
        foreach($GLOBALS["supported_langs"] as $GLOBALS["db_lang"]){
            $this->subcats();
        }
        return $this->sitemap.'</urlset>';
    }

}

?>
