<?

class pub_lessons extends pub{
    protected $lessons1;
    protected $lessons2;
    protected $lessons3;
    protected $lessons4;
    protected $lessons5;
    public $mode;

    public function __construct($id){
        $this->id = $id;
        $this->fields[] = "lessons1";
        $this->fields[] = "lessons2";
        $this->fields[] = "lessons3";
        $this->fields[] = "lessons4";
        $this->fields[] = "lessons5";
        $this->fields[] = "mode";
        $this->obj_name = "Stundu saraksts";
        parent::__construct();
    }
    
    public function contents(){
        if(!$this->id){
            $stmt = $this->sql->prepare("select lpad(title,4,'0') as ptitle, title, cipher from ".$this->table."_".$GLOBALS["db_lang"]." order by ptitle");
            $stmt->bind_result($ptitle, $title, $cipher);
            $stmt->execute();
            $i = 0; $out = array(""); $prev = "";
            if(!strpos($_SERVER["REQUEST_URI"], "!l")) $part = "/!l";
            else $part = "";
            while($stmt->fetch()){
                //echo $title." <= ".$cipher."<br />";
                if($prev && $prev != substr($ptitle, 0, 2)){ $i++; $out[$i] = ""; }
                $out[$i] .= "\t\t\t<a class=\"cl-sel\" href=\"$_SERVER[REQUEST_URI]$part/$cipher\">$title</a>\n";
                $prev = substr($ptitle, 0, 2);
            }
            //var_dump($out);
            $stmt->close();
            $out_formatted = "\t\t<h2>$this->obj_name</h2>\n"."\n\t\t<div class=\"cl-group\">\n".implode("\t\t</div>\n\t\t<div class=\"cl-group\">", $out)."\t\t</div>";
        }
        if($this->id){
            $out = array();
            $times = "\t\t\t<div class=\"data-block\">\n\t\t\t\t<div class=\"lesson-times-$this->mode\">";
            foreach($GLOBALS["times"][$this->mode] as $num => $time){
                if($time) $out[] = $num.". ".str_replace(array("<sup>","</sup>"), array(":",""),$time);
            }
            $times .= implode("<br />", $out)."</div>\n";
            $out = array();
            //var_dump($times);
            for($i = 1; $i < 6; $i++){
                $lday = "lessons".$i;
                //echo $this->$lday;
                $out[] = "\t\t\t<h3>".$GLOBALS["days"][$i]."</h3>\n".$times."\t\t\t\t<div class=\"lessons-$this->mode\">".nl2br($this->$lday)."</div>\n\t\t\t</div>";
            }
            //var_dump($out);
            $out_formatted = "\t\t<h2>$this->obj_name, $this->title klase</h2>\n".implode("\n", $out);
        }
        return $out_formatted;
    }
    
}

?>
