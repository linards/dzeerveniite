<?

class pub_user{
    protected $modules;
    protected $msg;
    protected $sql;
    protected $id;
    protected $path = array();
    public $allowed = array();
    public $limited = array();
    public $title;
    public $username;
    public $cipher;
    public function __construct($cipher = ""){
        $this->sql = $GLOBALS["sql"];
        if(isset($_REQUEST["user"]) && isset($_REQUEST["pw"])){
            //var_dump($_REQUEST);
            $stmt = $this->sql->prepare("select id, password from user_".$GLOBALS["db_lang_primary"]." where username = (?)");
            $stmt->bind_param('s', $_REQUEST["user"]);
            $stmt->execute();
            $stmt->bind_result($id, $pw);
            if($stmt->fetch()){
                $stmt->close();
                //var_dump($stmt);
                //var_dump($pw);
                $bcrypt = new Bcrypt(12);
                if($bcrypt->verify($_REQUEST["pw"],$pw)){
                    $cipher = sha1( uniqid() );
                    $stmt = $this->sql->prepare("update user_".$GLOBALS["db_lang_primary"]." set cipher = ? where id = ?");
                    $stmt->bind_param('si', $cipher, $id);
                    $stmt->execute();
                    //var_dump($stmt); die();
                    $stmt->close();
                    //die("update user_".$GLOBALS["db_lang_primary"]." set cipher = '$cipher' where id = $id");
                    //$_COOKIE["user"] = $cipher;
                    setcookie("user", $cipher, time()+3600);
                    $query = array();
                    foreach($_REQUEST as $field => $val){
                        if($field <> "user" && $field <> "pw" && $field <> "submit") $query[] = "$field=$val";
                        }
                    header("Location: $_SERVER[SCRIPT_NAME]?".implode("&",$query));
                }
            }
            else $this->msg = "<b>Kļūda: pieslēgšanās neizdevās - nepareizs lietotājvārds/parole";
        }
        elseif($cipher){
            $stmt = $this->sql->prepare("select id, title, username, modules from user_".$GLOBALS["db_lang_primary"]." where cipher = (?)");
            $stmt->bind_param('s', $cipher);
            $stmt->execute();
            $stmt->bind_result($this->id, $this->title, $this->username, $this->modules);
            //var_dump($stmt);
            if($stmt->fetch()){
                //var_dump($stmt);
                $stmt->close();
                $this->allowed = explode(";",$this->modules);
                if(in_array("db_cat", $this->allowed)) $this->allowed[] = "db_data";
                if(in_array("db_poll", $this->allowed)) $this->allowed[] = "db_poll_votes";
                if(isset($_REQUEST["op"]) && isset($_REQUEST["id"])){ if($_REQUEST["op"] == "db_user" && $_REQUEST["id"] == $this->id) $this->limited[] = "db_user"; }
                //var_dump($this->title);
            }
            setcookie("user", $cipher, time()+3600);
        }
    }
    
    public function auth(){
        $fields = "\t\t<!--hidden-fields-->\n\t\t<input type=\"hidden\" name=\"nosave\" value=\"1\">\n";
        foreach($_REQUEST as $field => $val){ if($field != "_") $fields .= "\t\t<input type=\"hidden\" name=\"$field\" value=\"$val\">\n"; }
        return str_replace("<!--hidden-fields-->", $fields, file_get_contents($GLOBALS["cwd"]."inc/html/auth-forma.htm"));
    }
    
    public function links(){
        $out = array(); //echo $_REQUEST["op"];
        foreach($this->allowed as $key => $val){
            $expanded = ""; if($_REQUEST["op"] == $val) $expanded = " expanded";
            if($val <> "db_data" && $val <> "db_poll_votes" && $val && isset($GLOBALS["modules"][$val]) && (!in_array($val, $GLOBALS["modules_primary"]) || $GLOBALS["db_lang"] == $GLOBALS["db_lang_primary"])){
                $tmp = "\t\t<li class=\"$val module$expanded\"><a class=\"jq\" href=\"$_SERVER[SCRIPT_NAME]?op=$val\">".$GLOBALS["modules"][$val]."</a>\n";
                $menu = $val."_menu";
                $parent = null; $so = null;
                if($val == "db_cat"){ $parent = 0; $so = 1; }
                if(method_exists($this, $menu)) $tmp .= $this->$menu($val);
                else $tmp .= $this->db_entry_menu($val, $parent, $so);
                $tmp .= "\t\t</li>\n";
                $out[] = $tmp;
            }
        }
        if($out) return implode("\n\t\t\t\t",$out);
    }
    
    protected function db_graphics_menu($module){
        $stmt = $this->sql->prepare("select id, title, `desc` from graphics_conf_".$GLOBALS["db_lang_primary"]." order by title");
        $stmt->execute(); $stmt->bind_result($id, $title, $desc);
        $tmp = "";
        while($stmt->fetch()){
            $tmp .= "\t\t\t\t<li><a class=\"\" href=\"$_SERVER[SCRIPT_NAME]?op=$module&id=$id\">$title</a></li>\n";
        }
        if(strlen($tmp) > 1) return "\t\t\t<ul>$tmp\t\t\t</ul>\n";
    }
    
    protected function db_entry_menu($module, $parent = null, $so = 0){
        if($module == "db_permission") $table = "user_".$GLOBALS["db_lang_primary"];
        else $table = substr($module, 3)."_".$GLOBALS["db_lang"];
        $out = ""; //echo $module;
        $query = "select id, title from ".$table;
        if(!is_null($parent)) $query .= " where parent = ?";
        if($so) $query .= " order by sortorder";
        else  $query .= " order by title"; //echo $query;
        $stmt = $this->sql->prepare($query);
        //var_dump($stmt);
        if(!is_null($parent)) $stmt->bind_param('i', $parent);
        $stmt->execute(); $stmt->bind_result($id, $title); $stmt->store_result();
        while($stmt->fetch()){
            $out .= "\t\t\t\t<li><a class=\"\" href=\"$_SERVER[SCRIPT_NAME]?op=$module&id=$id\">$title</a></li>\n";
        }
        $stmt->close();
        if(strlen($out) > 1) return "\t\t\t<ul>$out\t\t\t</ul>\n";
    }
    
    public function css(){
        $out = array(); //echo $_REQUEST["op"];
        foreach($GLOBALS["modules"] as $key => $val){
            if($val <> "db_data" && $val <> "db_poll_votes" && $val && isset($GLOBALS["icons"][$key])) $out[] = ".$key:hover, .$key.expanded{ background: left 0.25em no-repeat url('/inc/img/icons/24x24/".$GLOBALS["icons"][$key]."'); }";
        }
        if($out) return implode("\n\t\t\t",$out);
    }
    
    public function cats($res = null, $indent = 2, $parent = 0){
        $out = "";
        if(!$res){
            if((isset($_REQUEST["update"]) || $_REQUEST["op"] == "db_cat" || $_REQUEST["op"] == "db_data") && (isset($_REQUEST["id"]) || isset($_REQUEST["parent"]))){
                if(!isset($_REQUEST["op"])) $_REQUEST["op"] = "none";
                if($_REQUEST["op"] == "db_data"){
                    if(isset($_REQUEST["parent"])) $id = $_REQUEST["parent"];
                    elseif(isset($_REQUEST["id"])){
                        $res = $this->sql->query("select parent as id from data_".$GLOBALS["db_lang"]." where id = ".$_REQUEST["id"]);
                        $row = $res->fetch_assoc();
                        $id = $row["id"];
                    }
                }
                else $id = $_REQUEST["id"];
                $this->path[] = $id;
                while($id){
                    $this->path[] = $id;
                    //echo "select parent as id from cat_".$GLOBALS["db_lang"]." where id = $id";
                    $res = $this->sql->query("select parent as id from cat_".$GLOBALS["db_lang"]." where id = $id");
                    $row = $res->fetch_assoc();
                    $id = $row["id"];
                }
            }
            $this->query = "select id, parent, title, sortorder, cipher, public from cat_".$GLOBALS["db_lang"]." where parent = 0 order by sortorder";
            //echo $this->query."<br />";
            $res = $this->sql->query($this->query);
        }
        else{
            for($i = 0; $i < $indent; $i++) $out .= "\t";
            if(in_array($parent, $this->path)) $out .= "<ul style=\"display: block;\">\n";
            else $out .= "<ul>\n";
        }
        while ($row = $res->fetch_assoc()) {
            for($i = 0; $i < $indent; $i++) $out .= "\t";
            $newres = $this->sql->query("select id, parent, title, sortorder, cipher, public from cat_".$GLOBALS["db_lang"]." where parent = $row[id] order by sortorder");
            if($newres->num_rows && in_array($row["id"], $this->path)) $exp = "<a class=\"js expand\" title=\"Sakļaut\" href=\"$_SERVER[SCRIPT_NAME]?op=db_cat&id=$row[id]\"><img src=\"/inc/img/icons/24x24/Minus.png\" alt=\"Sakļaut\"></a>";
            elseif($newres->num_rows) $exp = "<a class=\"js expand\" title=\"Izvērst\" href=\"$_SERVER[SCRIPT_NAME]?op=db_cat&id=$row[id]\"><img src=\"/inc/img/icons/24x24/Plus.png\" alt=\"Izvērst\"></a>";
            elseif(in_array($row["id"], $this->path)) $exp = "";
            else $exp = "";
            $out .= "\t<li class=\"menu\">$exp<a href=\"$_SERVER[SCRIPT_NAME]?op=db_cat&id=$row[id]\">$row[title] <img src=\"/inc/img/icons/24x24/".($row["public"] == 2 ? "Weather Sun.png" : "Sleep.png")."\"></a>\n";
            if($newres->num_rows) $out .= $this->cats($newres, $indent + 2, $row["id"]);
        }
        if($res){
            for($i = 0; $i < $indent; $i++) $out .= "\t";
            $out .= "</ul>\n";
        }
        $out .= "</li>\n";
        return $out;
    }
    
    public function logout(){
        setcookie("user", "-1", time()-3600);
        header("Location: $_SERVER[SCRIPT_NAME]");
    }
    
    public function info(){
        return "\t\t<div class=\"user-info\">Lietotājs: $this->title ($this->username)<br /><a href=\"$_SERVER[SCRIPT_NAME]?op=db_user&id=$this->id\">Labot profilu</a> | <a href=\"$_SERVER[SCRIPT_NAME]?op=db_user&id=$this->id&pw=1\">Mainīt paroli</a> | <a class=\"logout\" href=\"$_SERVER[SCRIPT_NAME]?logout=1\">Beigt darbu</a></div>";
    }
    
    public function show_id(){
    	return $this->id;
    }
}

?>
