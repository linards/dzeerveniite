<?
class pub{
    protected $id;
    protected $title;
    protected $table;
    protected $table_lang;
    protected $fields = array("id", "title", "cipher");
    protected $road_parts = array();
    protected $obj_name;
    protected $s = 0;
    
    protected $sql;
    
    public $cipher;
    public $cat_parent = 0;
        
    public function __construct(){
        $this->sql = $GLOBALS["sql"];
        $this->table = str_replace("pub_","",get_class($this));
        //if(in_array(get_class($this), $GLOBALS["modules_primary"])) $this->table_lang = $this->table."_".$GLOBALS["db_lang_primary"];
        if(in_array(str_replace("pub","db", get_class($this)), $GLOBALS["modules_primary"])) $this->table_lang = $this->table."_".$GLOBALS["db_lang_primary"];
        else $this->table_lang = $this->table."_".$GLOBALS["db_lang"];
        if($this->id) $this->load();
        if(isset($_REQUEST["s"])) $this->s = $_REQUEST["s"];
        //var_dump($this->table);
        //var_dump($this->id);
    }
    
    protected function load(){
        $query = "select * from ".$this->table."_".$GLOBALS["db_lang"]." where id = '$this->id'"; //echo $query;
        $res = $this->sql->query($query);
        //var_dump($this->error);
        $row = $res->fetch_assoc(); //var_dump($row);
        foreach($this->fields as $field){ if(isset($row[$field])) $this->$field = $row[$field]; }
        //var_dump($this);
    }

    public function populate_road_parts(){
        //$road_parts = array();
        /*if($_REQUEST["id"] && isset($_REQUEST["data"])){
            $this->road_parts[] = $this->get_child_title($_REQUEST["id"]);
        }*/
        //if($this->request) $_SERVER["REQUEST_URI"] = $this->request;
        switch(get_class($this)){
            case "pub_cat":
                if($this->id) $this->road_parts_cat($this->id, $this->request);
                break;
            case "pub_data":
                $this->road_parts[] = "<a class=\"road\" href=\"".$_SERVER["REQUEST_URI"]."\">".$this->title."</a>";
                $this->road_parts_cat($this->parent, str_replace("/!d/".$this->cipher, "", $_SERVER["REQUEST_URI"]));
                break;
            case "pub_poll":
                $this->road_parts[] = "<a class=\"road\" href=\"".$_SERVER["REQUEST_URI"]."\">Aptauju arhīvs</a>";
                break;
            case "pub_lessons":
                $uri = $_SERVER["REQUEST_URI"];
                if($this->id){
                    $this->road_parts[] = "<a class=\"road\" href=\"".$_SERVER["REQUEST_URI"]."\">".$this->title."</a>";
                    $uri = str_replace("/".$this->cipher, "", $uri);
                    //$this->road_parts_cat($GLOBALS["parent_cat"], str_replace("/!l", "", $_SERVER["REQUEST_URI"]));
                }
                 array_unshift($this->road_parts,"<a class=\"road\" href=\"".$uri."\">".$this->obj_name."</a>");
                $this->road_parts_cat($GLOBALS["parent_cat"], str_replace("/!l", "", $uri));
                break;
            case "pub_lessons_ch":
                $uri = $_SERVER["REQUEST_URI"];
                if($this->class){ ///var_dump($this->class_obj);
                    $this->class_obj = new pub_lessons($this->class);
                    $this->road_parts[] = "<a class=\"road\" href=\"".$_SERVER["REQUEST_URI"]."\">".$this->class_obj->title."</a>";
                    $uri = str_replace("/".$this->cipher, "", $uri);
                    //$this->road_parts_cat($GLOBALS["parent_cat"], str_replace("/!l", "", $_SERVER["REQUEST_URI"]));
                }
                array_unshift($this->road_parts,"<a class=\"road\" href=\"".$uri."\">".$this->obj_name."</a>");
                $this->road_parts_cat($GLOBALS["parent_cat"], str_replace("/!l", "", $uri));
                break;
            default:
                $this->road_parts[] = "<a class=\"road\" href=\"".$_SERVER["REQUEST_URI"]."\">".$this->title."</a>";
                break;
        }
        array_unshift($this->road_parts,"<a class=\"road\" href=\"".$GLOBALS["request_prefix"]."/\">".$GLOBALS["project_title"][$GLOBALS["db_lang"]]."</a>");
        //var_dump($this->road_parts);
    }
    
    protected function road_parts_cat($id, $cipher){
        //echo $cipher;
        do{
           $res = $this->sql->query("select id, parent, title, cipher from cat_".$GLOBALS["db_lang"]." where id = $id");
           //echo "select parent, title, cipher from cat_".$GLOBALS["db_lang"]." where id = $id";
           $row = $res->fetch_assoc();
           //var_dump($row); die();
           if($row) array_unshift($this->road_parts,"<a class=\"road cat-road\" href=\"".str_replace("//","/",$cipher)."\">".$row["title"]."</a>");
           $cipher = str_replace("/".$row["cipher"], "", $cipher);
           $id = $row["parent"];
        } while($id);
    }
    
    public function road(){
        //var_dump($this->road_parts);
        if(count($this->road_parts) > 0){
            $this->road_parts[count($this->road_parts)-1] = strip_tags(end($this->road_parts));
            return "<ul class=\"breadcrumb\">\n\t\t\t<li>".implode("</li>\n\t\t\t<li>", $this->road_parts)."</li>\n\t\t</ul>";
        }
        else return null;
    }

}
?>
