<?

class db_caption extends db_entry{
    protected $obj_name = "Uzraksti";
    protected $caption;
    protected $desc;
    
    public function __construct(){
        if(isset($_REQUEST["id"])){
            $this->id = $_REQUEST["id"];
        }
        if(isset($_REQUEST["parent"])){
            $this->id = $_REQUEST["parent"];
        }
        $this->fields["parent"] = "none";
        $this->pub_names["title"] = "Identifikators";
        $this->fields["desc"] = "text";
        $this->pub_names["desc"] = "Informatīvs apraksts";
        $this->fields["caption"] = "textarea";
        $this->pub_names["caption"] = "Uzraksts";
        parent::__construct("caption",$this->id);
        //var_dump($this);
    }
    
    protected function before_delete(){
        return true;
    }

    protected function after_edit(){
        if(in_array(get_class($this), $GLOBALS["pub_user"]->allowed)){
            $this->out .= "\t\t<h3>Pievienotie objekti</h3>\n";
            $stmt = $this->sql->prepare("select id, title, `desc` from ".$this->table_lang." order by title");
            $stmt->execute();
            $stmt->bind_result($id, $title, $desc);
            while($stmt->fetch()){
                $this->out .= "\t\t<a class=\"data-list\" href=\"$_SERVER[SCRIPT_NAME]?op=$_REQUEST[op]&id=$id\">$title ($desc)</a>\n";
            }
        }
    }

}

?>
