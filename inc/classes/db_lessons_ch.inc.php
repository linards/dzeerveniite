<?

class db_lessons_ch extends db_entry{
    protected $obj_name = "Stundu saraksta izmaiņas";
    protected $value;
    protected $date;
    protected $class;
    protected $mode = 1;

    public function __construct(){
        if(isset($_REQUEST["id"])){
            $this->id = $_REQUEST["id"];
        }
        $this->fields["date"] = "date";
        $this->pub_names["date"] = "Datums";
        $this->fields["class"] = "classes";
        $this->pub_names["class"] = "Klase";
        $this->fields["value"] = "lessons";
        $this->pub_names["value"] = "Mainītais saraksts";
        $this->fields["title"] = "none";
        //$this->fields["id"] = "none";
        $this->date = next_business_day();
        parent::__construct("lessons_ch",$this->id);
        //if($this->id) $this->fields["title"] = "none";
        if($this->id) $this->set_mode();
    }
    
    protected function set_mode(){
        $res = $this->sql->query("select mode from lessons_".$GLOBALS["db_lang"]." where id = $this->class");
        $row = $res->fetch_assoc();
        $this->mode = $row["mode"];
    }
    
    protected function before_delete(){
        return true;
    }
    
}

?>
