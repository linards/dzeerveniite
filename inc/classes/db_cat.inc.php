<?
// sadaļas klase
class db_cat extends db_entry{

    protected $public = 2;
    protected $public_vals = array(1 => "Nav publicēts", 2 => "Publicēts");
    protected $created = "";
    protected $cipher = "";
    protected $sortorder = "";
    public $mode = 1;
    protected $mode_vals = array(1 => "Automātiski (jaunākie vispirms)", 2 => "Automātiski (vecākie vispirms)", 3 => "Manuāli");
    protected $social = 0;
    protected $social_vals = array(1 => "Atslēgtas", 2 => "Ieslēgtas");
    protected $show_date = 1;
    protected $show_date_vals = array(1 => "Nerādīt", 2 => "Rādīt");
    protected $comments = 0;
    protected $comments_vals = array(1 => "Nav atļauti", 2 => "Atļauti");
    protected $family = array();
    
    private $iparent = 0;
    protected $obj_name = "Sadaļa";
    private $data_types = array("db_data" => "<img src='/inc/img/icons/24x24/Dots.png' alt='Dots.png'> Datu bloks");
    private $colors = array("#c2afa9", "#7da277", "#ddd4d5", "#99ac5b", "#c1cc8a", "#8d5441" ,"#2a0306", "#d6933a", "#e0dbc7", "#829275", "#d48214", "#52ae32", "#dacbd2", "#5ace2f", "#2b5919", "#bcb2b3", "#b2cae2");
    
    public $colin;
    
    public $admin_title = "Informācijas pārvaldīšana (izmanto linkus kreisās puses izvēlnē!)";
    
    public function __construct($colors = "", $id = "", $save = 1){
        if($colors) $this->colors = $colors;
        if(isset($_REQUEST["id"]) && !$id) $id = $_REQUEST["id"];
        $this->fields["sortorder"] = "select";
        $this->pub_names["sortorder"] = "Novietots aiz";
        $this->fields["mode"] = "options";
        $this->pub_names["mode"] = "Datu kārtošana";
        $this->fields["public"] = "options";
        $this->pub_names["public"] = "Statuss";
        $this->fields["social"] = "options";
        $this->pub_names["social"] = "Sociālo tīklu pogas";
        $this->fields["show_date"] = "options";
        $this->pub_names["show_date"] = "Rādīt datumu?";
        $this->fields["comments"] = "options";
        $this->pub_names["comments"] = "Komentāri";
        $this->fields["cipher"] = "cipher";
        //if($id && isset($_REQUEST["data"])) $id = $this->get_id_from_child($_REQUEST["id"],$lang);
        parent::__construct("cat",$id,$save);
        if($this->id){
            $id = $this->id;
            $this->family["parents"] = array();
            $query = "select parent from cat_".$GLOBALS["db_lang"]." where id = ?";
            $stmt = $this->sql->prepare($query); $stmt->bind_param('i', $id); $stmt->bind_result($id);
            while($id){
                $stmt->execute(); $stmt->fetch();
                $this->family["parents"][] = $id;
            }
            $stmt->close();
            $stmt = $this->sql->prepare("select count(*) as cnt from cat_".$GLOBALS["db_lang"]." where parent = ?");
            $stmt->bind_param('i', $this->id); $stmt->bind_result($cnt); $stmt->execute(); $stmt->fetch();
            $this->family["kids"]["cat"] = $cnt; $stmt->close();
            $stmt = $this->sql->prepare("select count(*) as cnt from data_".$GLOBALS["db_lang"]." where parent = ?");
            $stmt->bind_param('i', $this->id); $stmt->bind_result($cnt); $stmt->execute(); $stmt->fetch();
            $this->family["kids"]["data"] = $cnt; $stmt->close();
        }
        //var_dump($this->family);
        $this->obj_name = "Sadaļa";
        //if($this->id) $this->populate_road_parts($this->id);
    }
    
    public function nav_list($color = "", $iparent = 0){
        if(!$iparent){
            $this->i = 0;
            $this->out = "<div id=\"navigation\">\n\t<ul class=\"top-level\">\n";
            next($this->colors); next($this->colors);
        }
        else $this->out .= "\t\t\t<ul class=\"sub-level\">";
        $this->query = "select id, parent, title, sortorder, cipher from ".$this->table_lang." where parent = $iparent order by sortorder";
        //echo $this->query."<br />";
        $res = $this->sql->query($this->query);
        if($color && $res->num_rows){
            $col = $color;
            $colors = colors(current($this->colors));
            $this->i++;
            }
        while ($row = $res->fetch_assoc()) {
            if(!$color){
                $col = next($this->colors);
                $colors = colors($col);
                $this->i = 1;
                }
            $this->out .= "\t\t\t\t<li id=\"menu-$row[id]\" style=\"background: $col;\"><a href=\"$_SERVER[SCRIPT_NAME]?op=db_cat&id=$row[id]\">$row[title]</a>\n";
            $this->nav_list($colors[$this->i], $row["id"]);
            $this->out .= "\t\t\t\t</li>\n"; //echo $this->i;
        }
        if(!$iparent){
            $col = next($this->colors);
            $this->out .= "\t\t\t\t<li id=\"karte\" style=\"background: $col;\"><a href=\"#\" onClick=\"alert('Šo sadaļu ģenerē automātiski')\">Lapas karte</a>\n";
            $this->out .= "\t\t\t\t</li>\n"; //echo $this->i;
        }
        $this->out .= "\t\t\t</ul>\n";
        if(!$iparent) $this->out .= "</div>\n";
        //var_dump($this);
        if(!$iparent) $this->colin = key($this->colors);
        return $this->out;
    }
    
    public function after_edit(){
        if($this->writable("data")){
            $out = "<h2>Sadaļai pievienotie dati</h2>\n\t\t<div id=\"data-list\">";
            
            $mover = "";
            $orderby = "created desc";
            if($this->mode == 2) $orderby = "created";
            elseif($this->mode == 3){
                $orderby = "sortorder";
                $mover = "<div class=\"data-mover\">&nbsp;</div>";
            }
            
            if($res = $this->sql->query("select * from data_".$GLOBALS["db_lang"]." where parent = '$this->id' order by $orderby")){
                while($row = $res->fetch_assoc()){
                    $out .= "\t\t\t<div id=\"data-$row[id]\" class=\"data-obj\">$mover<a class=\"data-list\" href=\"$_SERVER[SCRIPT_NAME]?id=$row[id]&op=$row[data]\">$row[title] <img src=\"/inc/img/icons/24x24/".($row["public"] == 2 ? "Weather Sun.png" : "Sleep.png")."\"></a></div>\n";    
                }
            }
            $this->out .= $out."\t\t</div>\n";
            if($this->mode == 3){
                $sortable = file_get_contents($GLOBALS["cwd"]."inc/js/sortable-data.js");
                $this->out .= "\t\t<script type='text/javascript'>$(function(){ $sortable});</script>\n";
            }
        }
    }
    
    protected function writable($mode = "cat"){
        $writable = true;
        if(!$this->id) $writable = false;
        elseif($mode == "cat"){
            //echo $GLOBALS["cat_depth"]." - ".count($this->family["parents"]);
            if(count($this->family["parents"]) >= $GLOBALS["cat_depth"]) $writable = false;
            if(!$GLOBALS["cat_data"] && $this->family["kids"]["data"]) $writable = false;
        }
        elseif($mode == "data"){
            if(!$GLOBALS["cat_data"] && $this->family["kids"]["cat"]) $writable = false;
            if(!in_array(count($this->family["parents"]), $GLOBALS["data_levels"])){
                if($GLOBALS["data_levels"][count($GLOBALS["data_levels"])-1] != "+") $writable = false;
                elseif($GLOBALS["data_levels"][count($GLOBALS["data_levels"])-2] > count($this->family["parents"])) $writable = false;
            }
        }
        return $writable;
    }
    
    protected function before_delete(){
        $this->query = "select count(*) as cnt from ".$this->table_lang." where parent = ".$this->id;
        $res = $this->sql->query($this->query);
        $this->row = $res->fetch_assoc();
        if($this->row["cnt"] > 0) throw new Exception('Dzēšana neizdevās: sadaļa satur apakšsadaļas!');
        $this->query = "select count(*) as cnt from data_".$GLOBALS["db_lang"]." where parent = ".$this->id;
        $res = $this->sql->query($this->query);
        $this->row = $res->fetch_assoc();
        if($this->row["cnt"] > 0) return 'Dzēšana neizdevās: sadaļa satur datus!';
        //die("Finished db_cat->before_delete()");
        return true;
    }
    
    protected function add_data(){
        $this->out .= "\t\t<div class=\"add_data\">Pievienot datus: \n";
        foreach($this->data_types as $key => $val) $this->out .= "\t\t\t<a class=\"add\" href=\"$_SERVER[SCRIPT_NAME]?parent=$this->id&op=$key\">$val</a>\n";
        $this->out .= "\t\t</div>\n";
    }
    
    protected function get_id_from_child($id){
        settype($id,"integer"); //echo "select parent from data_".$lang." where id = $id";
        $res = $this->sql->query("select parent from data_".$GLOBALS["db_lang"]." where id = $id"); //var_dump($res);
        $row = $res->fetch_assoc();
        return $row["parent"];
    }
    
    protected function get_child_title($id){
        settype($id,"integer"); //echo "select parent from data_".$lang." where id = $id";
        $res = $this->sql->query("select title from data_".$GLOBALS["db_lang"]." where id = $id"); //var_dump($res);
        $row = $res->fetch_assoc();
        return $row["title"];
    }
    
    
}
?>
