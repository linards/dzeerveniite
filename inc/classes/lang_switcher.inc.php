<?

class lang_switcher{
    protected $langs_public = array();
    protected $langs_db = array();    
    
    public function __construct(){
        if(isset($GLOBALS["supported_langs"])){
            if(count($GLOBALS["supported_langs"]) > 1){ //echo "la";
                foreach($GLOBALS["supported_langs"] as $lang){
                    $this->langs_public[] = ($lang == $GLOBALS["db_lang"] ? "" : "<a href=\"".$GLOBALS["request_base"].($lang == $GLOBALS["db_lang_primary"] ? "/" : "/".$lang)."\">")."<img src=\"/inc/img/flags/".$lang.".png\">".($lang == $GLOBALS["db_lang"] ? "" : "</a>");
                    $this->langs_db[] = ($lang == $GLOBALS["db_lang"] ? "" : "<a href=\"".$_SERVER["SCRIPT_NAME"]."?lang=".$lang."\">")."<img src=\"/inc/img/flags/".$lang.".png\">".($lang == $GLOBALS["db_lang"] ? "" : "</a>");
                }
            }
        }
    }
    
    public function show_flags($mode = "public"){
        $flags = "langs_".$mode;
        return "\t\t\t".implode("\n\t\t\t", $this->$flags);
    }
}

?>
