<?

class pub_caption extends pub{
    protected $caption;
    protected $db_lang;
    
    public function __construct($title = ""){
        parent::__construct(); //echo $title;
        $this->db_lang = $GLOBALS["db_lang"];
        if(isset($GLOBALS["cp_cipher"]) && !$title) $title = $GLOBALS["cp_cipher"];
        if($title){
            $title_arr = explode(";", $title);
            if(isset($title_arr[1])){
                if($title_arr[1] == "primary") $this->db_lang = $GLOBALS["db_lang_primary"];
            }
            $stmt = $this->sql->prepare("select `desc`, caption from ".$this->table."_".$this->db_lang." where title = ?");
            $stmt->bind_param('s', $title_arr[0]); $stmt->bind_result($this->title, $this->caption); $stmt->execute(); $stmt->store_result(); $stmt->fetch(); $stmt->close();
        }
    }
    
    public function contents(){
        return text_goodies($this->caption);
    }
    
    public function show(){
        return text_goodies($this->caption);
    }

}

?>
