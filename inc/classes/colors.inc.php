<?

class colors{
    public $colors = array("#c2afa9", "#7da277", "#ddd4d5", "#99ac5b", "#c1cc8a", "#8d5441" ,"#2a0306", "#d6933a", "#e0dbc7", "#829275", "#d48214", "#52ae32", "#dacbd2", "#5ace2f", "#2b5919", "#bcb2b3", "#b2cae2");
    public $default_colors = "c2afa9,7da277,ddd4d5,99ac5b,c1cc8a,8d5441,2a0306,d6933a,e0dbc7,829275,d48214, 52ae32,dacbd2,5ace2f,2b5919,bcb2b3,b2cae2";
    public $new_colors = "";
    public $img = "img/DSC_1225.jpg";
    private $sql;
    public $colin;
    
    public function __construct(){
        //parent::__construct($GLOBALS["db_host"], $GLOBALS["db_user"], $GLOBALS["db_pass"], $GLOBALS["db_name"]);
        //$this->set_charset("utf8");
        $this->sql = $GLOBALS["sql"];
    }
    public function load(){
        $res = $this->sql->query("select * from top_images_lva where now() <= valid order by valid asc limit 1");
        if($res->num_rows > 0){
            $row = $res->fetch_assoc();
            $this->colors = explode(";",$row["colors"]);
            $this->img = "top-images/pixelated/".$row["img"];
        }
        $newcols = array();
        for($i = 0; $i < 17; $i++){
            $newcols[] = substr($this->get_color($i),1);
        }
        $this->new_colors = implode(",", $newcols);
    }
    
    public function get_color($i){
        if(isset($this->colors[$i])) return $this->colors[$i];
        else{
            do{
                $i -= count($this->colors);
            } while(!isset($this->colors[$i]));
            return $this->colors[$i];
        }
    }

    public function menu($color = "", $iparent = 0, $pcipher = ""){
        if(!$iparent){
            $this->i = 0;
            $this->out = "<div id=\"navigation\">\n\t<ul class=\"top-level\">\n";
            next($this->colors); next($this->colors);
        }
        else $this->out .= "\t\t\t<ul class=\"sub-level\">";
        $this->query = "select id, parent, title, sortorder, cipher from cat_".$GLOBALS["db_lang"]." where parent = $iparent order by sortorder";
        //echo $this->query."<br />";
        $res = $this->sql->query($this->query);
        if($color && $res->num_rows){
            $col = $color;
            $colors = colors(current($this->colors)); //var_dump($colors);
            $this->i++;
            }
        while ($row = $res->fetch_assoc()) {
            if(!$iparent) $this->cipher = "/".$row["cipher"];
            else $this->cipher .= "/".$row["cipher"];
            if(!$color){
                if(next($this->colors)) $col = current($this->colors);
                else{
                    reset($this->colors);
                    $col = current($this->colors);
                }
                $colors = colors($col);
                $this->i = 1;
                }
            $this->out .= "\t\t\t\t<li id=\"menu-$row[id]\" style=\"background: $col;\"><a href=\"/rchv".$pcipher."/".$row["cipher"]."\">$row[title]</a>\n";
            $this->menu($colors[$this->i], $row["id"], $pcipher."/".$row["cipher"]);
            $this->out .= "\t\t\t\t</li>\n"; //echo $this->i;
        }
        //var_dump($this);
        if(!$iparent){
                if(next($this->colors)) $col = current($this->colors);
                else{
                    reset($this->colors);
                    $col = current($this->colors);
                }
            $this->out .= "\t\t\t\t<li id=\"menu-karte\" style=\"background: $col;\"><a href=\"/rchv/!m\">Lapas karte</a>\n";
            $this->out .= "\t\t\t\t</li>\n";            $this->colin = key($this->colors);
            }
        $this->out .= "\t\t\t</ul>\n";
        if(!$iparent) $this->out .= "</div>\n";
        return $this->out;
    }
    

}

?>
