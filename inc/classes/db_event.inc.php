<?

class db_event extends db_entry{
    protected $obj_name = "Kalendāra notikums";
    protected $op = "db_event";
    protected $value;
    protected $date;
    public $admin_title = "Kalendāra notikumi, labošanai - kreisās puses kalendārs";

    public function __construct(){
        if(isset($_REQUEST["id"])){
            $this->id = $_REQUEST["id"];
        }
        $this->fields["cipher"] = "cipher";
        $this->fields["date"] = "date";
        $this->pub_names["date"] = "Datums";
        $this->fields["value"] = "textarea";
        $this->pub_names["value"] = "Apraksts";
        $this->fields_ndb["op"] = "hidden";
        parent::__construct("events",$this->id);
        if(!$this->date){
            if(isset($_REQUEST["date"])) $this->date = $_REQUEST["date"];
            else $this->date = date('Y-m-d');
        }
    }
    
    public function edit($new = 0){
        $stmt = $this->sql->prepare("select id, title from ".$this->table_lang." where `date` = ?");
        $stmt->bind_param('s', $this->date);
        $stmt->execute(); $events = array();
        $stmt->bind_result($id, $title);
        while($stmt->fetch()) $events[$id] = $title;
        $stmt->close();
        if(count($events)){
            $out = "\t<form method=\"get\" action=\"$_SERVER[SCRIPT_NAME]\" name=\"event_sel\">\n\t\t<input type=\"hidden\" name=\"op\" value=\"db_event\">\n".$this->out .= $this->open_line.$this->open_field."Izvēlēties notikumu".$this->open_value."\n\t\t\t\t<select name=\"id\" onChange=\"document.forms['event_sel'].submit();\">\n\t\t\t\t\t<option>Izvēlies notikumu</option>\n";
            foreach($events as $id => $title){ //var_dump($event);
                $out .= "\t\t\t\t\t<option value=\"$id\">$title</option>\n";
            }
            $out .= "\t\t\t\t</select>\n".$this->close_value.$this->close_line."\t</form>\n";
        }
        else $out = "";
        return $out.parent::edit();
    }
    
    public function before_delete(){
        return true;
    }
}

?>
