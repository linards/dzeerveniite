<?

class db_poll extends db_entry{
    protected $obj_name = "Aptauja";
    protected $start;
    protected $end;
    protected $options = array("","","");
    protected $cook;
    public $admin_title = "Kalendāra notikumi, labošanai - kreisās puses kalendārs";

    public function __construct(){
        if(isset($_REQUEST["id"])){
            $this->id = $_REQUEST["id"];
        }
        $this->fields["options"] = "add-text";
        $this->pub_names["options"] = "atbildes variants";
        $this->fields["start"] = "date";
        $this->pub_names["start"] = "Aptaujas 1. diena";
        $this->fields["end"] = "date";
        $this->pub_names["end"] = "Aptaujas pēd. diena";
        $this->pub_names["title"] = "Jautājums";
        $this->fields["cook"] = "none";
        parent::__construct("poll",$this->id);
        if(!$this->start) $this->start = date('Y-m-d');
        if(!$this->end) $this->end = date('Y-m-d', mktime(0, 0, 0, date("m")  , date("d")+7, date("Y")));
    }
    
    public function before_edit(){
        if($this->id){
            $this->options = array();
            $res = $this->sql->query("select * from poll_votes_".$GLOBALS["db_lang"]." where parent = $this->id order by sortorder");
            while($row = $res->fetch_assoc()){
                $this->options[$row["id"]] = $row["title"];
            }
        }
    }
    
    public function before_save(){
        if(!$this->id) $this->cook = sha1( uniqid() );
    }
    
    public function after_save(){
        if($this->id){
            $res = $this->sql->query("select max(sortorder) as so from poll_votes_".$GLOBALS["db_lang"]." where parent = $this->id");
            //var_dump($res); echo $this->error; die();
            $row = $res->fetch_assoc();
            $so = $row["so"];
        }
        else $so = 0;
        $stmt = $this->sql->prepare("insert into poll_votes_".$GLOBALS["db_lang"]."(parent,title,sortorder) values(?,?,?)");
        $stmt->bind_param("isi", $this->id, $option, $so);
        foreach($this->options as $option){
            if($option){
                $so++;
                $stmt->execute();
            }
        }
        $stmt->close();
    }
    
    public function before_delete(){
        $this->sql->query("delete from poll_votes_".$GLOBALS["db_lang"]." where parent = $this->id");
        return true;
    }
    
    public function edit($new = 0){
        $stmt = $this->sql->prepare("select id, title from ".$this->table."_".$GLOBALS["db_lang"]." order by `start` desc");
        $stmt->execute(); $polls = array();
        $stmt->bind_result($id, $title);
        while($stmt->fetch()) $polls[$id] = $title;
        $stmt->close();
        if(count($polls)){
            $out = "\t<form method=\"get\" action=\"$_SERVER[SCRIPT_NAME]\" name=\"poll_sel\">\n\t\t<input type=\"hidden\" name=\"op\" value=\"db_poll\">\n".$this->out .= $this->open_line.$this->open_field."Izvēlēties aptauju".$this->open_value."\n\t\t\t\t<select name=\"id\" onChange=\"document.forms['poll_sel'].submit();\">\n\t\t\t\t\t<option>Izvēlies aptauju</option>\n";
            foreach($polls as $id => $title){ //var_dump($event);
                $out .= "\t\t\t\t\t<option value=\"$id\">$title</option>\n";
            }
            $out .= "\t\t\t\t</select>\n".$this->close_value.$this->close_line."\t</form>\n";
        }
        else $out = "";
        return $out.parent::edit();
    }
}

?>
