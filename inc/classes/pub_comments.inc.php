<?

class pub_comments extends pub{
    protected $parent;
    public $cipher = "none";
    
    public function __construct($parent){
        $this->parent = $parent;
        if(isset($GLOBALS["cm_cipher"])){
            if(isset($_SESSION[$GLOBALS["cm_cipher"]])){
                $this->parent = $_SESSION[$GLOBALS["cm_cipher"]];
                $this->cipher = $GLOBALS["cm_cipher"];
            }
        } 
        parent::__construct();
        //var_dump($_POST);
        if(isset($_POST["cm-value"])) $this->save();
    }
    
    public function generate_cipher(){
        $_SESSION = @array_flip($_SESSION);
        if(isset($_SESSION[$this->parent])) unset($_SESSION[$this->parent]);
        $_SESSION = @array_flip($_SESSION);
        $this->cipher = uniqid();
        $_SESSION[$this->cipher] = $this->parent;
        //var_dump($_SESSION);
        return $this->cipher;
    }
    
    public function show($start = 0){
        $out = array();
        $query = "select id, title, value, created from ".$this->table."_".$GLOBALS["db_lang"]." where parent = ? and id > ? and visible = 1 order by created";
        $stmt = $this->sql->prepare($query); $stmt->bind_param("ii", $this->parent, $start); $stmt->execute(); $stmt->bind_result($id, $title, $value, $created); $stmt->store_result();
        while($stmt->fetch()){
            $out[] = "\t\t\t".
            "<div class=\"cm-text\">".nl2br(find_url(htmlspecialchars($value)))."\n\t\t\t\t"."<div class=\"cm-head\"><strong>".htmlspecialchars($title)."</strong>, ".format_date_full($created)."</div></div>\n";
        }
        $_SESSION["loaded-".$this->parent] = $id;
        if(count($out)) return "\t\t<div class=\"cm-block\">".implode("", $out)."</div>\n";
    }
    
    public function show_form(){
        $cipher = $this->generate_cipher(); //echo $cipher;
        $from = array("cm-action");
        $to = array($GLOBALS["request_prefix"]."/!cm/".$cipher);
        $out = str_replace($from, $to, file_get_contents($GLOBALS["cwd"]."inc/html/comment-form.htm"));
        return $out;
    }
    
    public function show_number(){
        $query = "select count(*) from ".$this->table."_".$GLOBALS["db_lang"]." where parent = ? and visible = 1";
        $stmt = $this->sql->prepare($query); $stmt->bind_param("i", $this->parent); $stmt->execute(); $stmt->bind_result($count); $stmt->store_result(); $stmt->fetch();
        return $count;
    }
    
    public function save(){
        //echo "saving ...";
        //echo $_SESSION[$this->cipher];
        if(isset($_SESSION[$this->cipher]) && $this->parent && strlen($_POST["cm-value"]) > 3 && strlen($_POST["cm-parbaude"]) < 1){
            if(strlen($_POST["cm-title"]) < 2) $_POST["cm-title"] = "Anonīms";
            $query = "insert into ".$this->table."_".$GLOBALS["db_lang"]."(parent,title,value,ip,browser) values(?,?,?,?,?)";
            //echo $query;
            $stmt = $this->sql->prepare($query); $stmt->bind_param("issss", $this->parent, $_POST["cm-title"], $_POST["cm-value"], $_SERVER["REMOTE_ADDR"], $_SERVER["HTTP_USER_AGENT"]);
            $stmt->execute(); $stmt->close();
            unset($_SESSION[$this->cipher]);
            //$db_obj = new pub_data($this->parent);
            if(isset($_POST["_"])){ // AJAX!
                $out = $this->show($_SESSION["loaded-".$this->parent]).$this->show_form();
                echo "<div class=\"hidden\">".$out."</div>";
                exit;
            }
            else{
                $ciphers = array();
                $stmt = $this->sql->prepare("select cipher, parent from data_".$GLOBALS["db_lang"]." where id = ?");
                $stmt->bind_param("i", $this->parent); $stmt->bind_result($cipher, $parent); $stmt->execute(); $stmt->fetch(); $stmt->close();
                $ciphers[] = $cipher;
                $ciphers[] = "!d";
                $stmt = $this->sql->prepare("select cipher, parent from cat_".$GLOBALS["db_lang"]." where id = ?");
                $stmt->bind_result($cipher, $parent);
                while($parent){
                    $stmt->bind_param("i", $parent); $stmt->execute(); $stmt->fetch();
                    $ciphers[] = $cipher;
                }
                $stmt->close();
                $url = $GLOBALS["request_prefix"]."/".implode("/", array_reverse($ciphers));
                header("Location: ".$url);
            }
        }
        else return "<b>Kļūda:</b> nav atrasts komentāram atbilstošais ieraksts! Vai sīkdatņu pieņemšana ir ieslēgta?";
    }
}

?>
