<?

class db_user extends db_entry{
    protected $obj_name = "Lietotāji";
    protected $username;
    protected $password;
    protected $email;
    protected $modules;
    protected $send_pw = 0;
    protected $allowed = array();
    
    public function __construct(){
        if(isset($_REQUEST["id"])){
            $this->id = $_REQUEST["id"];
        }
        $this->pub_names["title"] = "Vārds";
        if(in_array(get_class($this), $GLOBALS["pub_user"]->allowed)) $this->fields["username"] = "text";
        $this->pub_names["username"] = "Lietotāja vārds";
        $this->fields["password"] = "password";
        $this->pub_names["password"] = "Parole";
        $this->fields["email"] = "text";
        $this->pub_names["email"] = "E-pasts";
        if(in_array(get_class($this), $GLOBALS["pub_user"]->allowed)) $this->fields["modules"] = "multi";
        $this->pub_names["modules"] = "Pieejas tiesības";
        
        $this->mandatory[] = "username"; $this->mandatory[] = "email";
        parent::__construct("user",$this->id);
        if($this->id){
            $this->allowed = explode(";", $this->modules);
        }
        //echo "LA LA LA";
    }
    
    protected function before_delete(){
        return true;
    }
    
    protected function before_save(){
        if(isset($_REQUEST["modules"])){
            $this->modules = implode(";", $_REQUEST["modules"]);
        }
        else $this->modules = " ";
    }
    
    protected function after_save(){
        /*if($this->send_pw && $this->id){ //die("sending pw");
            $msg = "Labdien,\n\nvarat peislēgties rchv.lv mājas lapas administrēšanas panelim šeit: http://".$_SERVER["SERVER_NAME"]."/admin.php, izmantojot šādu pieteikšanās informāciju:\nlietotājvārds: $this->username\nparole: $this->password";
            $headers = 'From: webmaster@rchv.lv' . "\r\n" .
            'Reply-To: linards.kalvans@lu.lv' . "\r\n" .
            'Content-type: text/plain; charset=utf-8' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
            mail($this->email, "rchv.lv administrēšanas panelis", $msg, $headers);
        }*/
    }
    
    protected function after_edit(){
        if(in_array(get_class($this), $GLOBALS["pub_user"]->allowed)){
            $this->out .= "\t\t<h3>Pievienotie lietotāji</h3>\n";
            $stmt = $this->sql->prepare("select id, title, username from user_".$GLOBALS["db_lang_primary"]." order by title");
            $stmt->execute();
            $stmt->bind_result($id, $title, $username);
            while($stmt->fetch()){
                $this->out .= "<a class=\"data-list\" href=\"$_SERVER[SCRIPT_NAME]?op=$_REQUEST[op]&id=$id\">$title ($username)</a>";
            }
        }
    }
}

?>
