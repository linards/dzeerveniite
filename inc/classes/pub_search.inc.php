<?

class pub_search extends pub{
    
    public function __construct(){
        $this->obj_name = "Meklēšanas rezultāti";
        parent::__construct();
        $this->title = "Meklēšanas rezultāti &raquo; ".strip_tags($GLOBALS["match"]);
    }
    
    public function contents(){
        $out = ""; $found = 0; $h2 = "\t\t<h2>$this->title</h2>\n";
        $match = $this->sql->real_escape_string($GLOBALS["match"]);
        if(isset($GLOBALS["modules"]["db_event"])) $sql_event = "union select id, 'eve', match (title, value) against('$match' IN BOOLEAN MODE) as m
            from events_".$GLOBALS["db_lang"]." where match(title, value) against ('$match' IN BOOLEAN MODE)";
        else $sql_event = "";
        $query = "select * from 
        (select id, 'cat', match (title) against('$match' IN BOOLEAN MODE) as m
            from cat_".$GLOBALS["db_lang"]." where match(title) against ('$match' IN BOOLEAN MODE) and public = 2
        union select id, 'dat', match (title, value) against('$match' IN BOOLEAN MODE) as m
            from data_".$GLOBALS["db_lang"]." where match(title, value) against ('$match' IN BOOLEAN MODE) and public = 2
        $sql_event
        union select f.parent as id, 'dat', match (f.txt) against('$match' IN BOOLEAN MODE) as m
            from files_".$GLOBALS["db_lang"]." f, data_".$GLOBALS["db_lang"]." d where match(f.txt) against ('$match' IN BOOLEAN MODE) and d.id = f.parent and d.public = 2
        union select id, 'cap', match (`desc`, caption) against('$match' IN BOOLEAN MODE) as m
            from caption_".$GLOBALS["db_lang"]." where match(`desc`, caption) against ('$match' IN BOOLEAN MODE)
        )
        as cde order by m desc";
        $stmt = $this->sql->prepare($query); //echo $query;
        $stmt->bind_result($id, $mode, $match);
        $stmt->execute(); $stmt->store_result(); //var_dump($stmt);
        while($stmt->fetch()){ //echo "$id $mode";
            $href = $GLOBALS["request_prefix"]; $titles = array(); $parts = array();
            if($mode == "dat"){
                $res = $this->sql->query("select parent, title, cipher from data_".$GLOBALS["db_lang"]." where id = $id");
                $row = $res->fetch_assoc();
                $parts["!d/".$row["cipher"]] = $row["title"];
                $id = $row["parent"];
                $mode = "cat";
            }
            if($mode == "cat"){
                $stmt2 = $this->sql->prepare("select title, cipher, parent from cat_".$GLOBALS["db_lang"]." where id = ?");
                //var_dump($stmt2);
                $stmt2->bind_param("i", $parent); $stmt2->bind_result($title, $cipher, $parent);
                $parent = $id;
                do{
                    $stmt2->execute();
                    $stmt2->fetch();
                    $parts[$cipher] = $title;
                    //echo "$title - $cipher - $parent";
                } while($parent);
                $stmt2->close();
                foreach(array_reverse($parts) as $key => $val){
                    $href .= "/".$key;
                    $titles[] = $val;
                }
                $out .= "\t\t\t<a class=\"found\" href=\"$href\">".implode(" &raquo; ", $titles)."</a>\n";
            }
            else if($mode == "cap"){
                $res = $this->sql->query("select title, `desc` from caption_".$GLOBALS["db_lang"]." where id = $id");
                $row = $res->fetch_assoc();
                $titles[] = $row["desc"];
                $href .= "/!cp/".$row["title"];
                $out .= "\t\t\t<a class=\"found\" href=\"$href\">".implode(" &raquo; ", $titles)."</a>\n";
            }
            elseif($mode == "eve"){
                $res = $this->sql->query("select `date`, title, cipher from events_".$GLOBALS["db_lang"]." where id = $id");
                $row = $res->fetch_assoc();
                $date = explode("-", $row["date"]);
                $href .= "/!c/".$date[2]."/".$date[1]."/".$date[0]."#".$row["cipher"];
                $titles[] = format_date_full($row["date"]);
                $titles[] = $row["title"];
                $out .= "\t\t\t<a class=\"found\" href=\"$href\">".implode(" &raquo; ", $titles)."</a>\n";
            }
            $found++;
        }
        if(!$found) $out = "<p>Nav atrasts neviens ieraksts.</p>".$out;
        elseif(substr($found,-1) == "1" && substr($found,-2) != "11") $out = "<p>Atrasts <b>$found</b> ieraksts.</p>".$out;
        else $out = "<p>Atrasti <b>$found</b> ieraksti.</p>".$out;
        $stmt->close();
        return $h2.$out;
    }
}

?>
