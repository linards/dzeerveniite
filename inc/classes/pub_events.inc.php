<?

class pub_events extends pub{
    protected $id;
    protected $d;
    protected $m;
    protected $y;
    
    public function __construct(){
        parent::__construct();
        if(isset($GLOBALS["d"])) $this->d = $GLOBALS["d"]; else $this->d = 0;
        if(isset($GLOBALS["m"])) $this->m = $GLOBALS["m"]; else $this->m = 0;
        if(isset($GLOBALS["y"])) $this->y = $GLOBALS["y"]; else $this->y = 0;
        settype($this->d,"integer"); settype($this->m,"integer"); settype($this->y,"integer");
        if($this->d && $this->m && $this->y) $this->title = $GLOBALS["days"][date('N', mktime(0,0,0,$this->m,$this->d,$this->y))].", $this->y. gada $this->d. ".$GLOBALS["months"][$this->m];
        else $this->title = "Notikumu kalendārs";
    }
    
    public function get_dates($m = 0, $y = 0){
        if(!$m) $m = date('m');
        if(!$y) $y = date('Y');
        $out = array();
        $res = $this->sql->query("select title, date_format(`date`, '%Y-%m-%e') as `date` from events_".$GLOBALS["db_lang"]." where `date` > date_sub('$y-$m-1',interval 10 day) and `date` < date_add('$y-$m-1', interval 41 day)");
        while($row = $res->fetch_assoc()){
            if(!isset($out[$row["date"]])) $out[$row["date"]] = array();
            $out[$row["date"]][] = $row["title"];
        }
        //echo "select `date` from events_".$GLOBALS["db_lang"]." where `date` > date_sub('$y-$m-1',interval 10 day) and `date` < date_add('$y-$m-1', interval 41 day)";
        return $out;
    }
    
    public function contents(){
        if(!$this->d || !$this->m || !$this->y) return "Kļūda: Nekorekti norādīts datums.";
        $out = "\t\t<h1>".$this->title."</h1>\n";
        $stmt = $this->sql->prepare("select title, value, cipher from ".$this->table."_".$GLOBALS["db_lang"]." where date = ?");
        $date = "$this->y-$this->m-$this->d"; $stmt->bind_param("s", $date); $stmt->bind_result($title, $value, $cipher);
        $stmt->execute(); $stmt->store_result();
        if($stmt->num_rows){
            $tmp = array();
            while($stmt->fetch()) $tmp[] = "\t\t<a name=\"$cipher\"></a><h2>$title</h2>\n\t\t<div class=\"data-block\">".parse_images($value)."</div>\n";
            $out .= implode("\n", $tmp);
        }
        else $out .= "Neviens notikums nav atrasts.";
        return $out;
    }
}

?>
