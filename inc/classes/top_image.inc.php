<?
// augšējā bilde, saglabā, nolasa krāsas u.c.

class top_image extends db_entry{

    protected $img;
    protected $img_used = array();
    protected $valid;
    protected $path = "top-images/";
    protected $thumbs = "thumbs/";
    protected $pix = "pixelated/";
    protected $obj_name = "Augšējais attēls / krāsas";

    function __construct(){
        if(isset($_REQUEST["img"])) $this->img = $_REQUEST["img"];
        if(isset($_REQUEST["id"])){
            $this->id = $_REQUEST["id"];
        }
        $this->fields["img"] = "hidden";
        $this->fields["colors"] = "hidden";
        $this->fields["valid"] = "date";
        parent::__construct("top_images",$this->id);
        if(isset($_REQUEST["do"])){
            if($_REQUEST["do"] == "purge_old") $this->purge_old();
        }
        //if($this->img) $this->title = $this->img;
        //var_dump($this->img);
    }
    
    public function files(){
        $this->out = "\t<div class=\"header2\">Datubāzei pievienotie attēli</div>\n\t\t<table border=\"1\">\n";
        $query = "select * from ".$this->table."_".$GLOBALS["db_lang"]." order by valid desc";
        $res = $this->sql->query($query);
        while($row = $res->fetch_assoc()){
            $this->out .= "\t\t\t<tr>
                <td><a href=\"$_SERVER[SCRIPT_NAME]?op=$_REQUEST[op]&id=$row[id]\">".img("/top-images/thumbs/$row[img]")."</a>
                <td>$row[valid]</td>
            </tr>\n";
            $this->img_used[] = $row["img"];
        }
        $this->out .= "\t\t</table>\n\t\t<a class=\"delete\" href=\"$_SERVER[SCRIPT_NAME]?op=$_REQUEST[op]&do=purge_old\" onclick=\"return confirm('Tiks dzēstas VISAS bildes, kam beidzies termiņš! Turpināt?');\">Dzēst vecos ierakstus</a>\n\t<div class=\"header2\">Ielādētie, bet nepievienotie attēli</div>\n\t\t<table border=\"1\">\n";
        $dp = opendir($this->path.'/pixelated');
        while (false !== ($entry = readdir($dp))) {
            if ($entry != "." && $entry != ".." && $entry != "tmp" && $entry != "thumbs" && $entry != "pixelated" && !in_array($entry,$this->img_used)) {
                $this->out .= "\t\t\t<tr>
                <td><a href=\"$_SERVER[SCRIPT_NAME]?op=$_REQUEST[op]&img=$entry\">".img("/top-images/thumbs/$entry")."</a></td>
                <td><a href=\"inc/php/unlink.php?op=$_REQUEST[op]&img=$entry\">dzēst failu</a></td>
            </tr>\n";
            }
        }
        $dp = opendir($this->path);
        while (false !== ($entry = readdir($dp))) {
            if ($entry != "." && $entry != ".." && $entry != "tmp" && $entry != "thumbs" && $entry != "pixelated" && !in_array($entry,$this->img_used)) {
                thumbpx($entry,$this->path,250,56);
                pixelate($entry,$this->path,1);
                unlink($this->path."/$entry");
                $this->out .= "\t\t\t<tr>
                <td><a href=\"$_SERVER[SCRIPT_NAME]?op=$_REQUEST[op]&img=$entry\">".img("/top-images/thumbs/$entry")."</a></td>
                <td><a href=\"inc/php/unlink.php?op=$_REQUEST[op]&img=$entry\">dzēst failu</a></td>
            </tr>\n";
            }
        }
        $this->out .= "\t\t</table>\n";
        if(!$this->id && !$this->img) $this->out .= file_get_contents("inc/html/uploader.htm");
        closedir($dp);
        return $this->out;
    }
    public function edit($new = 0){
        if(!$this->id && !isset($_REQUEST["img"])) return "";
        if(!file_exists($this->path.$this->pix.$this->img)) pixelate($this->img,$this->path,1);
        $this->out = "\t<div class=\"header\">Augšējā bilde: $this->img</div>\n\t\t".img("/".$this->path.$this->pix.$this->img)."\n";
        if(!$this->id || isset($_REQUEST["ch"])){
            $info = getimagesize($this->path.$this->pix.$this->img);
            $function = "imagecreatefrom".substr(image_type_to_extension($info[2]),1);
            $im = $function($this->path.$this->pix.$this->img);
            $r = 255; $g = 255; $b = 255;
            for($x = 30; $x < 800; $x += 43){
                $i = 0;
                $rgb = imagecolorat($im, $x + rand(-20,20), rand(0,180));
                $r = ($rgb >> 16) & 0xFF; $g = ($rgb >> 8) & 0xFF; $b = $rgb & 0xFF;
                if($r + $g + $b < 680){
                    $colors[] = "#".str_pad(dechex($r), 2, "0", STR_PAD_LEFT).str_pad(dechex($g), 2, "0", STR_PAD_LEFT).str_pad(dechex($b), 2, "0", STR_PAD_LEFT);
                    //echo "<div style=\"background: $color; width: 200px;\">&nbsp;</div>";
                }
            }
            shuffle($colors);
            $reload = " (pārlādē lapu, lai nomainītu!)";
        }
        else{
            $colors = explode(";", $this->colors);
            $reload = " (<a href=\"$_SERVER[SCRIPT_NAME]?op=$_REQUEST[op]&id=$this->id&ch=1\">mainīt krāsas</a>)";
        }
        $GLOBALS["colors"]->colors = $colors;
        $this->out .= "\t\t<div class=\"header3\">Iegūtās krāsas$reload</div>\n";
        foreach($colors as $key => $val){
            $this->out .= "\t\t\t<div style=\"background: $val; color: #fff;\">$val</div>\n";
        }
        $this->out .= "\t<form method=\"post\" action=\"$_SERVER[SCRIPT_NAME]\">\n";
        $this->out .= "\t\t<input type=\"hidden\" name=\"op\" value=\"".$_REQUEST["op"]."\" />\n";
        $this->out .= "\t\t<input type=\"hidden\" name=\"id\" value=\"".$this->id."\" />\n";
        $this->out .= "\t\t<input type=\"hidden\" name=\"img\" value=\"".$this->img."\" />\n";
        $this->out .= "\t\t<input type=\"hidden\" name=\"colors\" value=\"".implode(";",$colors)."\" />\n";
        $this->out .= $this->open_line.$this->open_field."Aktīvs līdz:".$this->open_value."<input class=\"valid\" maxlength=\"10\" type=\"text\" name=\"valid\" id=\"valid\" value=\"".htmlspecialchars($this->valid)."\" /><br /><iframe scrolling=\"no\" class=\"form-calendar\" src=\"inc/php/form-calendar.php?date=".$this->valid."&field=valid\"></iframe>".$this->close_value.$this->close_line;
        $this->out .= $this->open_line.$this->open_field."&nbsp;".$this->open_value."<input type=\"submit\" value=\"Saglabāt\" />".$this->close_value.$this->close_line."</form>";
        if($this->id) $this->out .= $this->open_line.$this->open_field."&nbsp;".$this->open_value."<a class=\"delete\" href=\"$_SERVER[SCRIPT_NAME]?op=$_REQUEST[op]&id=".$this->id."&do=delete\" onclick=\"return confirm('Vai tiešām dzēst? Neatgriezeniska darbība!');\">Dzēst ierakstu</a>".$this->close_value.$this->close_line;
        return $this->out;
    }
    
    protected function before_delete(){
        return true;
    }
    
    protected function purge_old(){
        $res = $this->sql->query("select * from ".$this->table."_".$GLOBALS["db_lang"]." where valid < now()");
        while($this->row = $res->fetch_assoc()){
            @unlink("top-images/".$this->row["img"]);
            @unlink("top-images/thumbs/".$this->row["img"]);
            @unlink("top-images/pixelated/".$this->row["img"]);
        }
        $this->sql->query("delete from ".$this->table."_".$GLOBALS["db_lang"]." where valid < now()");
    }
}
?>
