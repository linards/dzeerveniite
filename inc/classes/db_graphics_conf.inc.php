<?

class db_graphics_conf extends db_entry{
    protected $obj_name = "Grafisko elementu konfigurācija";
    protected $width = 200;
    protected $height = 100;
    protected $mode = 1;
    protected $mode_vals = array(1 => "Pēc nejaušības principa", 2 => "Manuāli");
    protected $number = 1;
    protected $desc;
    
    public function __construct(){
        if(isset($_REQUEST["id"])){
            $this->id = $_REQUEST["id"];
        }
        if(isset($_REQUEST["parent"])){
            $this->id = $_REQUEST["parent"];
        }
        $this->fields["parent"] = "none";
        $this->pub_names["title"] = "Identifikators";
        $this->fields["desc"] = "text";
        $this->pub_names["desc"] = "Informatīvs apraksts";
        $this->fields["width"] = "integer";
        $this->pub_names["width"] = "Platums (px)";
        $this->fields["height"] = "integer";
        $this->pub_names["height"] = "Augstums (px)";
        $this->fields["mode"] = "options";
        $this->pub_names["mode"] = "Kārtošana";
        $this->fields["number"] = "integer";
        $this->pub_names["number"] = "Skaits ('-1': neierobežots)";
        parent::__construct("graphics_conf",$this->id);
        //var_dump($this);
    }
    
    protected function before_delete(){
        $stmt = $this->sql->prepare("select id from graphics_".$GLOBALS["db_lang"]." where parent = ?");
        //echo $this->id;
        $stmt->bind_param('i', $this->id); $stmt->execute(); $stmt->store_result();
        //var_dump($stmt); exit;
        if($stmt->num_rows){
            return "Dzēšana neizdevās: pastāv grafiskie elementi ar šo konfigurāciju!";
            $stmt->close();
        }
        $stmt->close();
        return true;
    }

    protected function after_edit(){
        if(in_array(get_class($this), $GLOBALS["pub_user"]->allowed)){
            $this->out .= "\t\t<h3>Pievienotie objekti</h3>\n";
            $stmt = $this->sql->prepare("select id, title, `desc` from ".$this->table_lang." order by title");
            $stmt->execute();
            $stmt->bind_result($id, $title, $desc);
            while($stmt->fetch()){
                $this->out .= "\t\t<a class=\"data-list\" href=\"$_SERVER[SCRIPT_NAME]?op=$_REQUEST[op]&id=$id\">$title ($desc)</a>\n";
            }
        }
    }

}

?>
