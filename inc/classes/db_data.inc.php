<?
// datu moduļa klase
class db_data extends db_entry{
#	protected $owner_obj = $GLOBALS["pub_user"];
	protected $owner;# = $owner_obj->show_id();
    protected $public = 2;
    protected $public_vals = array(1 => "Nav publicēts", 2 => "Publicēts");
    protected $mode;
    protected $value;
    protected $value_parts = array();
    protected $created = "";
    protected $cipher = "";
    protected $sortorder = "";
    protected $obj_name = "Datu objekts";
    protected $modes = array(
        "data-text" => "<img src='/inc/img/icons/24x24/Document2.png' alt='Document2.png'> Teksta bloku",
        "data-gallery" => "<img src='/inc/img/icons/24x24/Picture.png' alt='Picture.png'> Foto galeriju",
        "data-file" => "<img src='/inc/img/icons/24x24/Folder2.png' alt='Folder2.png'> Failus");
    
    protected $parent_obj;
    
    public function __construct(){
    	$this->fields["owner"] = "hidden";
        $this->fields["public"] = "options";
        $this->pub_names["public"] = "Statuss";
        $this->fields["cipher"] = "cipher";
        $this->fields["value"] = "value";
        if(isset($_REQUEST["id"])) $this->id = $_REQUEST["id"];
        if(isset($_REQUEST["parent"])) $this->parent = $_REQUEST["parent"];
        if($this->id) $this->parent = $this->get_parent($this->id); //var_dump($this->id);
        $this->parent_obj = new db_cat("cat", $this->parent, 0);
        if($this->parent_obj->mode == 3){
            $this->fields["sortorder"] = "select";
            $this->pub_names["sortorder"] = "Novietots aiz";
        }
        parent::__construct("data",$this->id);
        if($this->id) $this->value_parts = get_data_parts($this->value);
        if(isset($_REQUEST["move"])) $this->swap();
    	if(!$this->owner) $this->owner = $GLOBALS["pub_user"]->show_id();
        //var_dump($this->fields);
    }
    
    protected function get_parent($id){
        $res = $GLOBALS["sql"]->query("select parent from data_".$GLOBALS["db_lang"]." where id = '$id'");
        $row = $res->fetch_assoc();
        return $row["parent"];
    }
    
    public function manage_comments(){
        $out = "\t\t<h3>Komentāri (".$this->title.")</h3>\n";
        $stmt = $this->sql->prepare("select id, title, value, created, ip, visible from comments_".$GLOBALS["db_lang"]." where parent = ?");
        $stmt->bind_param("i", $this->id); $stmt->execute(); $stmt->bind_result($id, $title, $value, $created, $ip, $visible); $stmt->store_result();
        while($stmt->fetch()){
            $out .= "\t\t\t".
            "<div class=\"cm-text visible-".$visible."\"><a class=\"js delete-comment\" href=\"/inc/php/trig-comment.php?id=".$id."\">Paslēpt/parādīt</a><br>".nl2br($value)."\n\t\t\t\t"."<div class=\"cm-head\"><strong>".$title."</strong>, ".format_date_full($created).", IP: ".$ip."</div></div>\n";
        }
        return $out;
    }

    public function move(){
        $cats = array(); $parents = array(); $childs = array(); $this->out = "";
        $out = "\t\t<h2>Pārvietot / kopēt datu bloku \"$this->title\"</h2>\n";
        if(!isset($_REQUEST["newparent"])){
            $this->load_cats();
            return $out."<p>Izvēlēties sadaļu:</p>".$this->out;
        }
        else{
            if(isset($_REQUEST["action"])){ //die("L AL ");
                if($_REQUEST["action"] == "m"){ // pārvietot
                    $query = "update data_".$GLOBALS["db_lang"]." set sortorder = sortorder + 1 where parent = $_REQUEST[newparent] and sortorder >= $_REQUEST[sortorder]"; //die($query);
                    $this->sql->query($query);
                    $query = "update data_".$GLOBALS["db_lang"]." set sortorder = sortorder - 1 where parent = $this->parent and sortorder > $_REQUEST[sortorder]"; //die($query);
                    $this->sql->query($query);
                    $query = "update data_".$GLOBALS["db_lang"]." set sortorder = $_REQUEST[sortorder], parent = $_REQUEST[newparent] where id = $this->id"; //die($query);
                    $this->sql->query($query);
                }
                elseif($_REQUEST["action"] == "c"){
                    //var_dump($this->value_parts); die();
                    $newval = "";
                    foreach($this->value_parts as $key => $part){
                        if($part[0] == "text"){
                            $newval .= $part[1];
                        }
                        elseif($part[0] == "gall"){
                            $fld = uniqid();
                            recurse_copy("gallery-images/".substr($part[1],0,-1),"gallery-images/".$fld);
                            $newval .= "<!--CMS_gallery:$fld/-->\n";
                        }
                        elseif($part[0] == "file"){
                            $fld = uniqid();
                            recurse_copy("files/".substr($part[1],0,-1),"files/".$fld);
                            $newval .= "<!--CMS_files:$fld/-->\n";
                        }
                    //$query = "insert into data_".$GLOBALS["db_lang"]."(title, parent, value, sortorder, cipher)";
                    }
                $this->id = 0;
                $this->parent = $_REQUEST["newparent"];
                $this->sortorder = $_REQUEST["sortorder"];
                $this->fields["sortorder"] = "sortorder";
                $this->value = $newval;
                $this->save();
                }
                header("Location: $_SERVER[SCRIPT_NAME]?op=$this->op&id=$this->id");
            }
            $newcat = new db_cat("", $_REQUEST["newparent"], $GLOBALS["db_lang"], 0);
            $out .= "\t\t\t<p>Kopēt pārvietot uz sadaļu: \"".strip_tags($newcat->road())."\"</p>\n";
            $out .= "\t\t\t<form method=\"get\" action=\"$_SERVER[SCRIPT_NAME]\" enctype=\"multipart/form-data\">\n";
            $out .= "\t\t\t\t<input type=\"hidden\" id=\"op\" name=\"op\" value=\"$this->op\">\n";
            $out .= "\t\t\t\t<input type=\"hidden\" name=\"id\" value=\"$this->id\">\n";
            $out .= "\t\t\t\t<input type=\"hidden\" name=\"nb\" value=\"1\">\n";
            $out .= "\t\t\t\t<input type=\"hidden\" name=\"move_data\" value=\"1\">\n";
            $out .= "\t\t\t\t<input type=\"hidden\" name=\"newparent\" value=\"$_REQUEST[newparent]\">\n";
            $out .= $this->open_line.$this->open_field."Izvēlieties darbību".$this->open_value."<select name=\"action\"><option value=\"c\">Kopēt</option><option value=\"m\">Pārvietot</option></select>".$this->close_value.$this->close_line;
            $so = "\t\t\t\t<option value=\"1\">Sākumā</option>\n";
            if (!($stmt = $this->sql->prepare("select id, title, sortorder + 1 from data_".$GLOBALS["db_lang"]." where parent = (?) order by sortorder"))){
               echo "Prepare failed: (" . $this->errno . ") " . $this->error;
            }
            $stmt->bind_param("i", $_REQUEST["newparent"]);
            $stmt->execute();
            $stmt->bind_result($id, $title, $sortorder);
            while ($stmt->fetch()) {
                //if($sortorder > $this->sortorder && $this->sortorder) $sortorder--;
                $so .= "\t\t\t\t<option value=\"$sortorder\">$title</option>\n";
            }
            $stmt->close();
            $out .= $this->open_line.$this->open_field."Novietot aiz".$this->open_value."<select name=\"sortorder\">".$so."</select>".$this->close_value.$this->close_line;
            $out .= $this->open_line.$this->open_field."&nbsp;".$this->open_value."<input type=\"submit\" value=\"Kopēt / pārvietot\" />".$this->close_value.$this->close_line."</form>";
            return $out;
        }
    }
    
    public function load_cats($parent = 0, $prev = array()){ //var_dump($prev);
        //$out = array();
        $stmt = $this->sql->prepare("select id, title from cat_".$GLOBALS["db_lang"]." where parent = $parent order by sortorder");
        $stmt->bind_result($id, $title); $stmt->execute(); $stmt->store_result();
        if($stmt->num_rows) $this->out .= "\t\t\t<ul class=\"move-data-ul\">\n";
        while($stmt->fetch()){
            $next = $prev;
            $next[] = $title;
            $this->out .= "\t\t\t\t<li class=\"move-data-li\"><a class=\"js move-expand\" title=\"Izvērst\" href=\"$_SERVER[SCRIPT_NAME]?op=$_REQUEST[op]&id=$this->id&move_data=1&newparent=$id\"><img src=\"/inc/img/icons/24x24/Plus.png\" alt=\"Izvērst\"></a> <a class=\"move-data\" href=\"$_SERVER[SCRIPT_NAME]?op=$_REQUEST[op]&id=$this->id&move_data=1&newparent=$id\">".implode(" &gt; ", $next)."</a>\n";
            $this->load_cats($id, $next);
            $this->out .= "\t\t\t\t</li>";
        }
        if($stmt->num_rows) $this->out .= "\t\t\t</ul>\n";
        $stmt->close();
        //return $out;
    }
    
    public function edit($new = 0){
        //var_dump($this);
        // veidosim formu, kurā labo informāciju
        if(isset($_REQUEST["move_data"])) return $this->move();
        if(!$new){
            if($this->id) $this->out = "\t<div class=\"header2\">".$this->obj_name.": labot</div>\n";
            else $this->out = "\t<div class=\"header2\">".$this->obj_name.": pievienot jaunu</div>\n";
        }
        if($this->id) $this->out .= $this->open_line."<a target=\"_blank\" href=\"".build_url($this->id, "data")."?preview=1\"><img src=\"/inc/img/icons/24x24/Puzzle.png\"> Priekšskatīt</a><br>\n".$this->close_line;
        $this->out .= $this->open_line."<a class=\"js show-comments\" href=\"\"><img src=\"/inc/img/icons/24x24/Bubble 1.png\"> Rediģēt komentārus</a><br>\n".
        "\t\t\t<div style=\"display: none;\" id=\"data-comments\">".$this->manage_comments()."</div>".$this->close_line;
        $this->out .= $this->open_line."<a href=\"$_SERVER[SCRIPT_NAME]?op=$this->op&id=$this->id&move_data=1\"><img src=\"/inc/img/icons/24x24/Back.png\"> Pārvietot vai kopēt visu bloku uz citu sadaļu</a>".$this->close_line;
        $this->out .= "\t<form method=\"post\" action=\"$_SERVER[SCRIPT_NAME]\" enctype=\"multipart/form-data\">\n";
        foreach($this->fields + $this->fields_ndb as $key => $value){
            if(!$new) $this->fvalue = $this->$key;
            elseif($key == "parent") $this->fvalue = $this->id;
            else $this->fvalue = "";
            $this->out .= $this->field($value, @$this->pub_names[$key], $key, "$key-$new", $this->fvalue, $new);
            /*switch($value){
                case "hidden":
                    $this->out .= "\t\t<input type=\"hidden\" name=\"$key\" id=\"$key\" value=\"".$this->fvalue."\" />\n";
                    break;
                case "text": //echo "$key => $value";
                    $this->out .= $this->open_line.$this->open_field.$this->pub_names[$key].$this->open_value."<input class=\"$value\" type=\"text\" maxlength=\"255\" name=\"$key\" value=\"".htmlspecialchars(text_colors($this->fvalue))."\" />".$this->close_value.$this->close_line;
                    break;
                case "select": //echo "$key => $value";
                    $this->out .= $this->open_line.$this->open_field.$this->pub_names[$key].$this->open_value."<select class=\"$value\" name=\"$key\">\n";
                    $this->select($key, $new);
                    $this->out .= "\t\t\t</select>".$this->close_value.$this->close_line;
                    break;
                case "textarea":
                    $this->out .= $this->open_line.$this->pub_names[$key]."<br /><textarea class=\"ckeditor\" name=\"$key\">".remove_format($this->fvalue)."</textarea>".$this->close_line;
                    break;
            }*/
        }
        $delete = "&nbsp;";
        if($this->id && !$new){
            //echo "LA LA LA";
            $delete .= "<a class=\"btn btn-lg btn-danger\" href=\"$_SERVER[SCRIPT_NAME]?op=".$this->op."&id=".$this->id."&do=delete\" onclick=\"return confirm('Vai tiešām dzēst? Neatgriezeniska darbība!');\">Dzēst ierakstu</a>";
        }
        $this->out .= $this->field("save", "Saglabāt", 0, 0, 0, 0, $delete);
        $this->out .= $this->open_line."Pievienot lauku (sākumā): ";
        foreach($this->modes as $key => $val){
            $this->out .= "<a class=\"js $key start\" href=\"#\">$val</a> ";
        }
        $this->out .= $this->close_line;
        $this->out .= "<div id=\"data-fields\">";
        $i = 1; $sequence = "";
        foreach($this->value_parts as $key => $val){
            if($val[0] == "tex"){
                $this->out .= "\t\t<div id='data-field-$i' class='data-field up-text'>\n\t\t\t<input type='hidden' name='mode[$i]' value='txt'>\n\t\t\t<div class=\"move\">Velc, lai pārvietotu</div>\n\t\t\t<textarea class='ckeditor wm' name='data[$i]' id='ta-$i'>".str_replace("<!--CMS_text_block_start-->\n","",remove_format(prepare_code($val[1])))."</textarea>\n\t\t\t<a href=\"#\" class=\"js remove-this\">Noņemt šo lauku<br /><br /></a>\n\t\t<script>CKEDITOR.replace('ta-$i');</script></div>\n";
            }
            elseif($val[0] == "gal"){
                $script = $this->call_ul("gal",$i);
                $this->out .= "\t\t<div id='data-field-$i' class='data-field up-image'>\n\t\t\t<input type='hidden' name='mode[$i]' value='gal'>\n\t\t\t<div class=\"move\">Velc, lai pārvietotu</div>\n\t\t\t<input type='hidden' name='fld[$i]' value='$val[1]'>\n\t\t\t".$this->edit_gal($val[1])."\n\t\t\t<div id=\"uploader$i\">Lūdzu izmantojiet pārlūkprogrammu, kas atbalsta HTML5, piemēram Chrome vai Firefox</div><button id=\"pickfiles$i\">Izvēlēties failus</button><button id=\"uploadfiles$i\">Augšupielādēt failus</button><br><input type=\"hidden\" name=\"files[$i]\" id=\"files-$i\">\n\t\t\t<a href=\"#\" class=\"js remove-this\">Noņemt šo lauku</a>\n\t\t$script</div>\n";
            }
            elseif($val[0] == "fil"){
                $script = $this->call_ul("fil",$i);
                $this->out .= "\t\t<div id='data-field-$i' class='data-field up-file'>\n\t\t\t<input type='hidden' name='mode[$i]' value='file'>\n\t\t\t<div class=\"move\">Velc, lai pārvietotu</div>\n\t\t\t<input type='hidden' name='fld[$i]' value='$val[1]'>\n\t\t\t".$this->edit_files($val[1])."<br />\n\t\t\t<div id=\"uploader$i\">Lūdzu izmantojiet pārlūkprogrammu, kas atbalsta HTML5, piemēram Chrome vai Firefox</div><button id=\"pickfiles$i\">Izvēlēties failus</button><button id=\"uploadfiles$i\">Augšupielādēt failus</button><br><input type=\"hidden\" name=\"files[$i]\" id=\"files-$i\">\n\t\t\t<a href=\"#\" class=\"js remove-this\">Noņemt šo lauku</a>\n\t\t$script</div>\n";
            }
            $sequence .= "$i;";
            $i++;
        }
        $i++;
        $this->out .= "</div>";
        $sortable = file_get_contents($GLOBALS["cwd"]."inc/js/sortable.js");
        $this->out .= "\t\t<script type='text/javascript'>$(function(){window.field_num=$i;\n$sortable});</script>\n\t\t<input type=\"hidden\" name=\"sequence\" id=\"sequence\" value=\"$sequence\" />\n";
        $this->out .= $this->open_line."Pievienot lauku (beigās): ";
        foreach($this->modes as $key => $val){
            $this->out .= "<a class=\"js $key end\" href=\"#\">$val</a> ";
        }
        $this->out .= $this->close_line;
        $delete = "&nbsp;";
        if($this->id && !$new){
            //echo "LA LA LA";
            $delete .= "<a class=\"btn btn-lg btn-danger\" href=\"$_SERVER[SCRIPT_NAME]?op=".$this->op."&id=".$this->id."&do=delete\" onclick=\"return confirm('Vai tiešām dzēst? Neatgriezeniska darbība!');\">Dzēst ierakstu</a>";
        }
        $this->out .= $this->field("save", "Saglabāt", 0, 0, 0, 0, $delete)."</form>";
        //$this->out .= $this->open_line.$this->open_field."&nbsp;".$this->open_value."<input class=\"save\" type=\"submit\" value=\"Saglabāt\" />".$this->close_value.$this->close_line."</form>";
        //echo get_class($this);
        return $this->out;
    }
    
    protected function before_save(){
        //var_dump($this);
        //var_dump($_POST);
        //die();
        $indices = explode(";",$_POST["sequence"]); 
        $indices = array_unique($indices); //var_dump($indices);
        $org_files = array(); $new_files = array();
        $org_gals = array(); $new_gals = array();
        if($this->id){
            //var_dump($this->value);
            $res = $this->sql->query("select value from ".$this->table_lang." where id = $this->id");
            $row = $res->fetch_assoc();
            $parts = get_data_parts($row["value"]);
            foreach($parts as $part){
                if($part[0] == "fil") $org_files[] = $part[1];
                elseif($part[0] == "gal") $org_gals[] = $part[1];
                }
            //var_dump($org_gals); die();
        }
        $this->value = ""; $used_files = array(); $used_gals = array();
        if(!$this->title) $this->title = $this->parent_obj->titlel;
        //var_dump($_POST["mode"]); die();
        foreach($indices as $key => $val){ //echo "$key => $val <br />";
            if(strlen($val) > 0){
                if(isset($_POST["mode"][$val])){
                    if($_POST["mode"][$val]) $mode = $_POST["mode"][$val]; //echo $mode."<br />";
                    else $mode = "";
                }
                else $mode = "nav";
                if($mode == "txt" && isset($_POST["data"][$val])){
                    //echo $mode."<br />";
                    $this->value .= "<!--CMS_text_block_start-->\n".$_POST["data"][$val]."\n";
                }
                elseif($mode == "gal" || $mode == "file"){
                    $path = $GLOBALS["cwd"]."assets/";
                    if(isset($_POST["fld"][$val])){ $fld = substr($_POST["fld"][$val],0,-1); $add = 1; }
                    else $fld = uniqid();
                    $upfiles = @explode(";", $_POST["file_sets"][$_POST["files_id"][$val]]);
                    //var_dump($upfiles);
                    if(count($upfiles) > 1 && !file_exists($path.$fld)){
                        mkdir($path.$fld, 0775);
                        chmod($path.$fld, 0775);
                        if($mode == "gal"){
                            mkdir($path.$fld."/thumbs", 0775);
                            chmod($path.$fld."/thumbs", 0775);
                        }
                    }
                    $fld .= "/";
                    foreach($upfiles as $fileid){
                        if(strlen($fileid) > 0){
                            $file_info = @explode(";", $_POST[$fileid]);
                            $pi = pathinfo($file_info[1]);
                            if(!file_exists($path.$fld.$file_info[1])) $filename = $file_info[1];
                            else{
                                for($i = 1; $i < 99999; $i++){
                                    if(!file_exists($path.$fld.$pi["filename"]."_".$i.".".$pi["extension"])){
                                        $filename = $pi["filename"]."_".$i.".".$pi["extension"];
                                        break;
                                    }
                                }
                            }
                            copy($GLOBALS["cwd"]."tmp/".$file_info[0], $path.$fld.$filename);
                            chmod($path.$fld.$filename, 0775);
                            unlink($GLOBALS["cwd"]."tmp/".$file_info[0]);
                            if($mode == "gal") thumb($path.$fld.$filename, $path.$fld."thumbs/".$filename, $GLOBALS["thumbsize"], $GLOBALS["thumbsize"]);
                        }
                    }
                    $this->value .= "<!--CMS_$mode:$fld-->\n";
                    //die();
                }
            }
        }
        //die($this->value);
        if(!$this->value) $this->value = " ";
        $parts = get_data_parts($this->value);
        //print_r($parts); die();
        foreach($parts as $part){
            //var_dump($part);
            $stmt = $this->sql->prepare("insert into files_".$GLOBALS["db_lang"]."(file,parent,txt,location) values(?,?,?,?)");
            $stmt->bind_param("siss", $filedb, $this->id, $txt, $location); //die($this->id);
            if($part[0] == "gal"){
                $files = list_files($GLOBALS["cwd"]."assets/".$part[1]);
                foreach($files as $file){
                    //var_dump($file);
                    $filedb = filename_to_db($part[1].$file);
                    //echo $_POST[$filedb]."<br>";
                    $this->sql->query("delete from files_".$GLOBALS["db_lang"]." where file = '".$filedb."' ");
                    if(isset($_POST["loc_".$filedb])) $location = $_POST["loc_".$filedb];
                    else $location = "";
                    if(isset($_POST[$filedb])) $txt = $_POST[$filedb];
                    else{
                        $pi = pathinfo($file);
                        $txt = $pi["filename"];
                    }
                    $stmt->execute();
                }
                $new_gals[] = $part[1];
            }
            elseif($part[0] == "fil") $new_files[] = $part[1];
            $stmt->close();
        }
        foreach($org_gals as $ogal){
            if(!in_array($ogal, $new_gals)){
                $this->sql->query("delete from files_".$GLOBALS["db_lang"]." where file like '$ogal%'");
                rrmdir($GLOBALS["cwd"]."assets/".$ogal);
            }
        }
        foreach($org_files as $ofil){
            if(!in_array($ofil, $new_files)){
                rrmdir($GLOBALS["cwd"]."assets/".$ofil);
            }
        }
        //die();
        if(!$this->title) $this->title = $this->parent_obj->title;
        //var_dump($this->value);
        //die("finished pre ed");
    }
    
    protected function edit_gal($fld){
        $path = $GLOBALS["cwd"]."assets/";
        $path_pub = "/assets/";
        //die($path.$fld);
        $out = ""; $files = array();
        if(file_exists($path.$fld)){
            $dir = new DirectoryIterator($path.$fld);
            foreach ($dir as $fileinfo) {
                if ($fileinfo->isFile()) {
                    $files[] = $fileinfo->getFilename();
                }
            }
        }
        natcasesort($files);
        foreach($files as $file){
                $out .= "\t\t\t\t<div class='img-data' id='$path_pub$fld$file'><a href='$path_pub$fld$file' style='float: left; margin-right: 10px; border: none;' class='highslide' onclick='return hs.expand(this);'><img src='".$path_pub.$fld."thumbs/$file' title='Uzklikšķini, lai palielinātu'></a><textarea name='".filename_to_db($fld.$file)."' class='img-data-ta'>".$this->get_img_sig($fld.$file)."</textarea><br /><a class=\"js delete-file btn btn-danger btn-sm\" href=\"/inc/php/unlink.php?op=$this->op&amp;file=$path_pub$fld$file\">Dzēst attēlu</a> Atrašanās vieta: <input type='text' name='loc_".filename_to_db($fld.$file)."' value='".$this->get_img_sig($fld.$file, "location")."'> (angliski, meklētājiem)</div>\n";
        }
        return $out;
    }
    
    protected function edit_files($fld){
        $path = $GLOBALS["cwd"]."assets/";
        $path_pub = "/assets/";
        //die($path.$fld);
        $out = ""; $files = array();
        if(file_exists($path.$fld)){
            $dir = new DirectoryIterator($path.$fld);
            foreach ($dir as $fileinfo) {
                if ($fileinfo->isFile()) {
                    $files[] = $fileinfo->getFilename();
                }
            }
        }
        natcasesort($files);
        foreach($files as $file){
                $pi = pathinfo($path_pub.$fld.$file);
                if(file_exists("mime/".strtolower($pi["extension"])."-icon-64x64.png")) $icon = "mime/".strtolower($pi["extension"])."-icon-64x64.png";
                else $icon = "mime/blank-64x64.png";
            $out .= "\t\t\t\t<div class='img-data' id='$path_pub$fld$file'><img style=\"float: left;\" src=\"/$icon\"><br /><a href='$path_pub$fld$file' style='float: left; margin-right: 10px; border: none;'>$file</a><br /><a class=\"js delete-file\" href=\"/inc/php/unlink.php?op=$this->op&amp;file=$path_pub$fld$file\"><img src=\"/inc/img/icons/16x16/Forbidden.png\"> Dzēst failu</a></div>\n";

        }
        return $out;
    }
    
    protected function before_delete(){
        $res = $this->sql->query("select value from ".$this->table_lang." where id = $this->id");
        $row = $res->fetch_assoc();
        $this->value_parts = get_data_parts($row["value"]);
        foreach($this->value_parts as $key => $val){
            if($val[0] == "gall"){
                //echo $val[1];
                $this->sql->query("delete from files_".$GLOBALS["db_lang"]." where file like '$val[1]%'");
                rrmdir($GLOBALS["cwd"]."assets/".$val[1]."thumbs");
                rrmdir($GLOBALS["cwd"]."assets/".substr($val[1],0,-1));
            }
            elseif($val[0] == "file"){
                rrmdir($GLOBALS["cwd"]."assets/".substr($val[1],0,-1));
            }
        }
        //die("deleting paused");
        $query = "delete from comments_".$GLOBALS["db_lang"]." where parent = ".$this->id;
        $this->sql->query($query);
        return true;
    }
    
    protected function swap(){
        if($_REQUEST["move"] == "up" && $_REQUEST["m"] > 0){
            $swix = $_REQUEST["m"] - 1;
            $tmp = $this->value_parts[$_REQUEST["m"]];
            $this->value_parts[$_REQUEST["m"]] = $this->value_parts[$swix];
            $this->value_parts[$swix] = $tmp;
        }
        elseif($_REQUEST["move"] == "down" && $this->value_parts[$_REQUEST["m"] + 1]){
            $swix = $_REQUEST["m"] + 1;
            $tmp = $this->value_parts[$_REQUEST["m"]];
            $this->value_parts[$_REQUEST["m"]] = $this->value_parts[$swix];
            $this->value_parts[$swix] = $tmp;
        }
        
        $this->value = "";
        foreach($this->value_parts as $value){
            if($value[0] == "text") $this->value .= "<!--CMS_text_block_start-->\n".str_replace("<!--CMS_text_block_start-->\n","",$value[1])."";
            elseif($value[0] == "gall") $this->value .= "<!--CMS_gallery:$value[1]-->\n";
            elseif($value[0] == "file") $this->value .= "<!--CMS_files:$value[1]-->\n";
        }
        //var_dump($this->value); die();
        $stmt = $this->sql->prepare("update ".$this->table_lang." set value = ? where id = $this->id");
        $stmt->bind_param("s", $this->value);
        $stmt->execute();
        $stmt->close();
        header("Location: $_SERVER[SCRIPT_NAME]?op=$this->op&id=$this->id");
    }
    
}
?>
