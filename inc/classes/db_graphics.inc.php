<?

class db_graphics extends db_graphics_conf{
    protected $obj_name = "Grafiskie elementi";
    protected $ids = array();
    protected $filenames = array();
    protected $alts = array();
    protected $urls = array();
    protected $blanks = array();
    protected $blank_vals = array(0 => "Tajā pašā logā", 1 => "Jaunā logā");
    protected $parent;
    
    public function __construct(){
        parent::__construct();
        if(!$this->id) $_SESSION["message"] = array("alert-danger", "Nav norādīta grafisko elementu konfigurācija, kuru izmantot!");
        //trigger_error("Nav norādīta grafisko elementu konfigurāciju, kuru izmantot!", E_USER_ERROR);
        $this->parent = $this->id;
        $this->fields = array();
        $this->pub_names = array();
        $this->fields["id"] = "hidden";
        $this->pub_names["id"] = "ID";
        $this->fields["filename"] = "hidden";
        $this->pub_names["filename"] = "Faila nosaukums";
        $this->fields["alt"] = "text";
        $this->pub_names["alt"] = "Paraksts / ALT teksts";
        $this->fields["url"] = "text";
        $this->pub_names["url"] = "Hipersaite";
        $this->fields["blank"] = "options";
        $this->pub_names["blank"] = "Saites atvēršana";

        // Visus augstāk aprakstītos parametrus ielādēsim no DB
        $stmt = $this->sql->prepare("select id, filename, alt, url, blank from graphics_".$GLOBALS["db_lang"]." where parent = ? order by sortorder");
        //var_dump($stmt);
        $stmt->bind_param('i', $this->parent); $stmt->bind_result($id, $filename, $alt, $url, $blank); $stmt->execute();
        while($stmt->fetch()){
            foreach($this->fields as $field => $type){
                $name = $field."s";
                array_push($this->$name, $$field);
            }
        }
        //var_dump($this);
        $this->fields["parent"] = "hidden";
        $this->pub_names["parent"] = "Izmantojamā konfigurācija";
    }
    
    public function edit($new = 0){
        if(!$this->id) return null;
        $out = "\t<div class=\"header1\">".$this->title." (".$this->desc.")</div>";
        $out .= "\t<div class=\"header2\">Pievienotie elementi:</div>\n\t<form method=\"post\" id=\"data-form\" name=\"db_entry\" action=\"$_SERVER[SCRIPT_NAME]\" enctype=\"multipart/form-data\">\n";
        $out .= $this->field("hidden", "Parent", "parent", "parent", $this->parent);
        $out .= $this->field("hidden", "op", "op", "op", $this->op);
        $out .= $this->field("hidden", "sequence", "sequence", "sequence");
        $out .= "\t\t<div class=\"data-fields\" id=\"data-fields\">\n";
        $path = "/assets/graphics/";
        $path_th = "/assets/graphics/thumbs/".$GLOBALS["thumbsize"]."/";
        //var_dump($this->ids);
        foreach($this->ids as $key => $id){
            $out .= "\t\t<div id='data-field-$id' class='data-field up-image'>\n\t\t\t<div class=\"move\">Velc, lai pārvietotu</div>\n\t\t\t<div class='img-graphics' style='display: inline-block;' id='".$path.$this->filenames[$key]."'><a href='".$path.$this->filenames[$key]."' style='margin-right: 10px; border: none;' class='highslide' onclick='return hs.expand(this);'><img src='".$path_th.$this->filenames[$key]."' title='Uzklikšķini, lai palielinātu'></a></div>\n\t\t<div  style='display: inline-block; width: 600px;'>\n";
            foreach($this->fields as $field => $type){
                if($field != "parent"){
                    $name = $field."s"; $array = $this->$name;
                    $out .= $this->field($type, $this->pub_names[$field], $field."[$id]", $field."-".$id, $array[$key]);
                }
            }
            $out .= "\t\t</div>\n\t\t<br><a class=\"js delete-file btn btn-danger btn-sm\" href=\"/inc/php/unlink.php?op=$this->op&amp;file=".$path.$this->filenames[$key]."\">Dzēst elementu</a></div>\n";
        }
        
        $out .= "\t\t</div>\n";
        $sortable = file_get_contents($GLOBALS["cwd"]."inc/js/sortable.js");
        $out .= "\t\t<script type='text/javascript'>$(function(){ \n\t$sortable });</script>\n";
        $script = $this->call_ul("gal", 0, $this->width, $this->height);
        $out .= "\t\t<div class=\"header2\">Jaunu elementu pievienošana:</div>\n\t\t<div id='data-field-ul' class='data-field up-image'>\n\t\t\t<div id=\"uploader0\">Lūdzu izmantojiet pārlūkprogrammu, kas atbalsta HTML5, piemēram Chrome vai Firefox</div><button id=\"pickfiles0\">Izvēlēties failus</button><button id=\"uploadfiles0\">Augšupielādēt failus</button><br><input type=\"hidden\" name=\"files[0]\" id=\"files-0\">\n\t\t$script</div>\n";
        $delete = "<a class=\"btn btn-lg btn-danger\" href=\"$_SERVER[SCRIPT_NAME]?op=".$this->op."&id=".$this->id."&do=empty\" onclick=\"return confirm('Vai tiešām dzēst? Neatgriezeniska darbība!');\">Dzēst visus elementus</a>";
        $out .= $this->field("save", "Saglabāt", 0, 0, 0, 0, $delete)."</form>";

        return $out;
    }
    
    public function save(){
        //var_dump($this); die();
        //var_dump($_POST);
        $sortorder = 1;
        $indices = explode(";",$_POST["sequence"]);
        //var_dump($indices); die();
        $stmt = $this->sql->prepare("update graphics_".$GLOBALS["db_lang"]." set sortorder = ?, alt = ?, url = ?, blank = ? where id = ?");
        $stmt->bind_param('issii', $sortorder, $alt, $url, $blank, $id);
        foreach($indices as $index){
            if(isset($_POST["id"][$index])){
                /*foreach($this->fields as $field => $type){
                    if(isset($_POST[$field])){
                        $array = $_POST[$field];
                        if(is_array($array)) $$field = $array[$index];
                        //echo $field." => ".$$field;
                    }
                }*/
                $id = $_POST["id"][$index];
                $alt = $_POST["alt"][$index];
                $url = $_POST["url"][$index];
                $blank = $_POST["blank"][$index];
                $stmt->execute();
                $sortorder++;
            }
        }
        $stmt->close();
        $path = $GLOBALS["cwd"]."assets/graphics/";
        $upfiles = @explode(";", $_POST["file_sets"][$_POST["files_id"][0]]);
        $stmt = $this->sql->prepare("insert into graphics_".$GLOBALS["db_lang"]."(parent, filename, alt, sortorder) values(?, ?, ?, ?)");
        $stmt->bind_param('issi', $_POST["parent"], $filename, $filename, $sortorder);
        foreach($upfiles as $fileid){
            if(strlen($fileid) > 0){
                $file_info = @explode(";", $_POST[$fileid]);
                $pi = pathinfo($file_info[1]);
                if(!file_exists($path.$file_info[1])) $filename = $file_info[1];
                else{
                    for($i = 1; $i < 99999; $i++){
                        if(!file_exists($path.$pi["filename"]."_".$i.".".$pi["extension"])){
                            $filename = $pi["filename"]."_".$i.".".$pi["extension"];
                            break;
                        }
                    }
                }
                copy($GLOBALS["cwd"]."tmp/".$file_info[0], $path.$filename);
                chmod($path.$filename, 0775);
                unlink($GLOBALS["cwd"]."tmp/".$file_info[0]);
                thumb($path.$filename, $path."thumbs/".$filename, $this->width, $this->height, 1);
                thumb($path.$filename, $path."thumbs/".$GLOBALS["thumbsize"]."/".$filename, $GLOBALS["thumbsize"], $GLOBALS["thumbsize"]);
                $stmt->execute();
                //die($this->width.$this->height);
                $sortorder++;
            }
        }
        $stmt->close();
        //die();
        $_SESSION["message"] = array("alert-success", "Labots/-a ".strtolower($this->obj_name)." '".$this->title."'!");
        header("Location: $_SERVER[SCRIPT_NAME]?op=".get_class($this)."&id=$_POST[parent]".$GLOBALS["aq"]);
        exit;
    }
}

?>
