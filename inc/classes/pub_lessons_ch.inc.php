<?

class pub_lessons_ch extends pub{
    protected $class;
    protected $date;
    protected $value;
    protected $class_obj;    
    
    public function __construct($id = 0){
        $this->class = $id;
        $this->fields[] = "class";
        $this->fields[] = "date";
        $this->fields[] = "value";
        $this->obj_name = "Stundu izmaiņas";
        parent::__construct();
        if($this->class) $this->class_obj = new pub_lessons($this->class);
        //var_dump($this->class_obj->title);
    }
    
    public function contents(){
        $out = "";
        if(!$this->class){
            $out .= "\t\t\t<h2>Stundu izmaiņas</h2>";
            $stmt = $this->sql->prepare("select l.title, lpad(l.title, 4, '0') as ptitle, l.cipher, lc.date from lessons_".$GLOBALS["db_lang"]." l, lessons_ch_".$GLOBALS["db_lang"]." lc where lc.date >= date_format(addtime(now(), '08:00:00'), '%Y-%m-%d') and lc.class = l.id order by lc.date, ptitle");
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($title, $ptitle, $cipher, $date);
            if(!strpos($_SERVER["REQUEST_URI"], "!lc")) $part = "/!lc";
            else $part = "";
            if($stmt->num_rows){
                $dates = array();
                $out .= "";
                while($stmt->fetch()){
                    if(!in_array($date, $dates)){
                        if($dates) $out .= "<br />";
                        //$parts = explode("-", $date);
                        //$nday = date('N', mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));
                        $out .= "\t\t".format_date_part($date).": ";
                            }
                    $out .= "<a href=\"$_SERVER[REQUEST_URI]$part/$cipher\">$title</a> ";
                    if(!in_array($date, $dates)){
                        $dates[] = $date;
                    }
                }
            }
            else $out .= "Izmaiņu nav.";
        }
        else{
            $out = array("\t\t\t<h2>Stundu izmaiņas, ".$this->class_obj->title." klase</h2>");
            $stmt = $this->sql->prepare("select `date`, value from lessons_ch_".$GLOBALS["db_lang"]." where class = $this->class and `date` >= date_format(addtime(now(), '08:00:00'), '%Y-%m-%d') order by `date`");
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($date, $value);
            if($stmt->num_rows){
                $tmp = array();
                $times = "\t\t\t<div class=\"lesson-times-".$this->class_obj->mode."\">";
                foreach($GLOBALS["times"][$this->class_obj->mode] as $num => $time){
                    //if($time) $tmp[] = $num.". ".$time;
                    if($time) $tmp[] = $num.". ".str_replace(array("<sup>","</sup>"), array(":",""),$time);
                }
                $times .= implode("<br />", $tmp)."</div>\n";
                
                //$out = array();
                while($stmt->fetch()){
                    //$parts = explode("-", $date);
                    //$nday = date('N', mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));
                    //$nmon = date('n', mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));
                    $out[] = "\t\t\t<h3>".format_date_part($date)."</h3>\n".$times."\t\t\t<div class=\"lessons-".$this->class_obj->mode."\">".nl2br($value)."</div>";
                }
            }
            else $out[] = "Izmaiņu nav.";
            $out = implode("\n", $out);
        }
        return $out;
    }
    
    public function shortcuts(){
        $out = "<a href=\"/rchv/!l\">Stundu saraksts</a><br /><a href=\"/rchv/!lc\">Stundu izmaiņas</a><br />";
        $stmt = $this->sql->prepare("select l.title, lpad(l.title, 4, '0') as ptitle, l.cipher, lc.date from lessons_".$GLOBALS["db_lang"]." l, lessons_ch_".$GLOBALS["db_lang"]." lc where lc.date >= date_format(addtime(now(), '08:00:00'), '%Y-%m-%d') and lc.class = l.id order by lc.date, ptitle");
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($title, $ptitle, $cipher, $date);
        if($stmt->num_rows){
            $dates = array();
            $out .= "";
            while($stmt->fetch()){
                if(!in_array($date, $dates)){
                    if($dates) $out .= "<br />";
                    //$parts = explode("-", $date);
                    //$nday = date('N', mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));
                    $out .= "\t\t".format_date_short($date).": ";
                    }
                $out .= "<a href=\"/rchv/!lc/$cipher\">$title</a> ";
                if(!in_array($date, $dates)){
                    $dates[] = $date;
                }
            }
        }
        else $out .= "Izmaiņu nav.";
        return $out;
    }

}

?>
