<?

class pub_data extends pub{
    protected $parent;
    protected $parent_obj;
    protected $value;
    
    public function __construct($id, $colors = ""){
        $this->id = $id;
        $this->fields[] = "parent";
        $this->fields[] = "value";
        parent::__construct();
        $this->cat_parent = $this->parent;
        $this->parent_obj = new pub_cat($this->parent);
    }
    
    public function social(){
        if($this->parent_obj->social) return "<script type=\"text/javascript\">var social=1;</script>";
        else return "<script type=\"text/javascript\">var social=0;</script>";
    }
    
    public function contents(){
        $out = "";
        $stmt = $this->sql->prepare("select title, cipher, value, created from data_".$GLOBALS["db_lang"]." where id = $this->id");
        $stmt->execute(); $stmt->store_result(); $stmt->bind_result($title, $cipher, $value, $created); $data = array();
        //var_dump($stmt);
        if($stmt->num_rows > 1) $mode = 2;
        else $mode = 1;
        while($stmt->fetch()){
            $data[] = format_data($this->id, $value, $title, $mode, $cipher, $created, $this->parent_obj->social, $this->parent_obj->show_date, $this->parent_obj->comments);
        }
        $stmt->close(); //var_dump($data);
        $out .= implode("\n\t\t", $data);
        return $out;
    }
}


?>
