<?

class pub_cat extends pub{
    protected $parent;
    protected $mode;
    protected $request;
    public $social;
    public $show_date;
    public $comments;
    protected $request_parts = array();
    protected $found_kids = 0;
    protected $data_found = 0;
    
    public function __construct($id){
        $this->id = $id;
        $this->fields[] = "parent";
        $this->fields[] = "mode";
        $this->fields[] = "social";
        $this->fields[] = "show_date";
        $this->fields[] = "comments";
        if(strlen($_SERVER["REQUEST_URI"]) < 3) $this->request = $GLOBALS["request_prefix"];
        else $this->request = $_SERVER["REQUEST_URI"];
        /*$res = $GLOBALS["sql"]->query("select count(*) as cnt from data_".$GLOBALS["db_lang"]." where parent = $this->id");
        $rowC = $res->fetch_assoc();
        if($rowC["cnt"]) $this->found_kids = 1;
        else $this->childs($this->id);*/
        //var_dump($this->request);
        parent::__construct();
        if(!in_array($GLOBALS["action"]["class"], $GLOBALS["avoid_cat"])) $this->find_data($this->id);
        $this->request .= implode("/", $this->request_parts);
        $_SERVER["REQUEST_URI"] = $this->request;
        if($this->id != $id) $this->load();
        $this->cat_parent = $this->id;
        //echo $this->id;
    }
    
    protected function find_data($id){
        $stmt = $this->sql->prepare("select id from data_".$GLOBALS["db_lang"]." where parent = ?");
        $stmt->bind_param('i', $id); $stmt->execute(); $stmt->store_result();
        //var_dump($stmt);
        if($stmt->num_rows){ $this->data_found = 1; $this->id = $id; }
        else{
            $stmtc = $this->sql->prepare("select id, cipher from cat_".$GLOBALS["db_lang"]." where parent = ? and public = 2 order by sortorder");
            //var_dump($stmtc);
            $stmtc->bind_param('i', $id); $stmtc->bind_result($cid, $cipher); $stmtc->execute(); $stmtc->store_result();
            while($stmtc->fetch()){
                $this->find_data($cid);
                if($this->data_found){
                    //echo "found at $cid!!!";
                    $GLOBALS["action"]["parents"][] = $cid;
                    //$this->request .= $cipher."/";
                    array_unshift($this->request_parts, $cipher);
                    break;
                }
            }
            $stmtc->close();
        }
        $stmt->close();
    }
    
    public function user_menu($parent = 0, $ciphers = array()){
        $stmt = $this->sql->prepare("select id, title, cipher from ".$this->table."_".$GLOBALS["db_lang"]." where parent = ? and public = 2");
        $stmt->bind_param('i', $parent); $stmt->execute(); $stmt->store_result(); $stmt->bind_result($id, $title, $cipher);
        if(!$parent) $out = "<a class=\"menu-trig visible-xs\" href=\"/\"><span class=\"glyphicon glyphicon-plus-sign\"></span> ".$GLOBALS["project_title"][$GLOBALS["db_lang"]]."</a>\n\t\t<div class=\"visible-xs\">&nbsp;</div>\n";
        else $out = "";
        if($stmt->num_rows){
            $stmt_ch = $this->sql->prepare("select id from ".$this->table."_".$GLOBALS["db_lang"]." where parent = ? and public = 2");
            $stmt_ch->bind_param('i', $id);
            $out .= "\t\t\t<ul class=\"nav nav-list";
            if(!$parent) $out .=  " hidden-xs";
            $out .= "\">\n";
            //var_dump($stmt);
            while($stmt->fetch()){
                $aciphers = $ciphers; array_push($aciphers, $cipher);
                $link = $GLOBALS["request_prefix"]."/".implode("/", $aciphers);
                
                $stmt_ch->execute(); $stmt_ch->store_result();
                if($stmt_ch->num_rows){
                    if(in_array($id, $GLOBALS["action"]["parents"])) $icon = "glyphicon glyphicon-minus-sign";
                    else $icon = "glyphicon glyphicon-plus-sign";
                    $aclass = "expandable";
                    $active = "";
                }
                else{
                    $icon = "glyphicon glyphicon-chevron-right";
                    $aclass = "data";
                    if($id == $this->id) $active = "active";
                    else $active = "";
                }
                //echo $id;
                $expanded = ""; if(in_array($id, $GLOBALS["action"]["parents"])) $expanded = "expanded";
                $out .= "\t\t\t\t<li id=\"menu-$id\" class=\"$active $expanded\"><a class=\"$aclass\" href=\"$link\"><span class=\"$icon\"></span> $title</a>\n".$this->user_menu($id, $aciphers)."\t\t\t\t</li>\n";
            }
            $out .= "\t\t\t</ul>\n";
            $stmt_ch->close();
        }
        //echo $this->parent;
        return $out;
    }
    
    protected function childs($parent = 0, $prev = array()){ //var_dump($prev);
        $out = array();
        $stmt = $GLOBALS["sql"]->prepare("select id, title, cipher from cat_".$GLOBALS["db_lang"]." where parent = $parent and public = 2 order by sortorder");
        $stmt->bind_result($id, $title, $cipher); $stmt->execute(); $stmt->store_result();
        $stmtD = $GLOBALS["sql"]->prepare("select count(*) as cnt from data_".$GLOBALS["db_lang"]." where parent = ? and public = 2");
        $stmtD->bind_param("i", $id); $stmtD->bind_result($cnt);
        while($stmt->fetch()){
            $next = $prev;
            $next["titles"][] = $title;
            $next["ciphers"][] = $cipher;
            $stmtD->execute(); $stmtD->store_result(); $stmtD->fetch();
            if($cnt && !$this->found_kids){
                $this->id = $id; $this->found_kids = 1;
                $this->request .= "/".implode("/",$next["ciphers"]);
                break;
            }
            elseif($this->found_kids) break;
            $this->childs($id, $next);
        }
        $stmtD->close();
        $stmt->close();
        //return $out;
    }
    
    public function contents(){
        $out = "";
        /*$stmt = $this->sql->prepare("select title, cipher from cat_".$GLOBALS["db_lang"]." where parent = $this->id order by sortorder");
        $stmt->execute(); $stmt->store_result(); $stmt->bind_result($title, $cipher); $childs = array();
        while($stmt->fetch()){
            $childs[] = "<a class=\"cat-title\" href=\"".$this->request."/$cipher\">$title</a>";
        }
        $stmt->close();
        $out .= implode("\n\t\t", $childs);*/
        $orderby = "created desc";
        if($this->mode == 2) $orderby = "created";
        elseif($this->mode == 3) $orderby = "sortorder";
        $stmt = $this->sql->prepare("select id, title, cipher, value, created from data_".$GLOBALS["db_lang"]." where parent = $this->id and public = 2 order by $orderby limit ".$this->s.", ".$GLOBALS["ipp"]);
        $stmt->execute(); $stmt->store_result(); $stmt->bind_result($id, $title, $cipher, $value, $created); $data = array();
        //var_dump($stmt);
        if($stmt->num_rows > 1){
            $mode = 2;
        }
        else{
            $mode = 1;
        }
        while($stmt->fetch()){
            if($mode == 2) $link = str_replace(array("///","//"),array("/","/"),"<a class=\"lasit-vairak\" href=\"$_SERVER[REQUEST_URI]/!d/$cipher\">");
            else $link = false;
            $data[] = format_data($id, $value, $title, $mode, $cipher, $created, $this->social, $this->show_date, $this->comments)."\t\t<div class=\"data-delimiter\">&nbsp;</div>\n";
        }
        $stmt->close(); //var_dump($data);
        $out .= implode("\n\t\t", $data);
        $res = $this->sql->query("select count(*) as cnt from data_".$GLOBALS["db_lang"]." where parent = $this->id");
        $row = $res->fetch_assoc(); //var_dump($row);
        $out .= other_items($row["cnt"]);
        $out .= "\t<script type=\"text/javascript\">\n".
        "\t\t$(document).ready(function() { $('#menu li').removeClass('active'); $('#menu #menu-".$this->id."').addClass('active'); });\n".
        "\t</script>\n";
        return $out;
    }
}

?>
