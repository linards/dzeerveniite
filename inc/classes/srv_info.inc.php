<?

class srv_info{
    public $out;
    
    public function __construct(){
        $file = file($GLOBALS["cwd"]."q834hsdom.txt");
        $this->out = "\t\t<div class=\"info\"><b>Lapas apjoms</b><br />\n";
        $max = $file[1]; settype($max, "integer"); $maxmb = round($max / 1024, 1);
        $used = $file[2]; settype($used, "integer"); $usedmb = round($used / 1024, 1);
        $proc = round(100 * $used / $max, 1);
        if($proc > 90) $font = "<font color=\"red\"><b>";
        else $font = "<font>";
        $this->out .= "\t\t\t".$font."Aizņemti $proc % ($usedmb no $maxmb MB)</font></b>\n";
        $this->out .= "\t\t\t<div class=\"info-img\"><img src=\"/img/black.png\" width=".round($proc*1.9)." height=\"10\"></div>\n";
        $this->out .= "\t\t\tDati iegūti: $file[0]\n";
        
        $this->out .= "\t\t</div>\n";
    }
}

?>
