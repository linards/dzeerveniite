<?php
/**
* Calendar Generation Class
*
* This class provides a simple reuasable means to produce month calendars in valid html
*
* @version 2.8
* @author Jim Mayes <jim.mayes@gmail.com>
* @link http://style-vs-substance.com
* @copyright Copyright (c) 2008, Jim Mayes
* @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt GPL v2.0
*/

class Calendar{
	var $date;
	var $year;
	var $month;
	var $day;
	
	var $week_start_on = FALSE;
	var $week_start = 7;// sunday
	
	var $my_link_mode = 1; // normāli
	
	var $link_days = TRUE;
	var $link_to;
	var $formatted_link_to;
	
	var $mark_today = TRUE;
	var $today_date_class = 'today';
	
	var $mark_selected = TRUE;
	var $selected_date_class = 'selected';
	
	var $mark_passed = TRUE;
	var $passed_date_class = 'passed';
	
	var $highlighted_dates;
	var $titles;
	var $default_highlighted_class = 'highlighted';
	
	var $days = array("Sv.","P.","O.","T.","C.","P.","S.");
	var $days_long = array("svētdiena", "pirmdiena", "otrdiena", "trešdiena", "ceturtdiena", "piektdiena", "sestdiena");
	var $months = array("", "janvāris", "februāris", "marts", "aprīlis", "maijs", "jūnijs", "jūlijs", "augusts", "septembris", "oktobris", "novembris", "decembris");
	
	/* CONSTRUCTOR */
	function Calendar($date = NULL, $year = NULL, $month = NULL){
		$self = htmlspecialchars($_SERVER['PHP_SELF']);
		$this->link_to = $self;
		
		if( is_null($year) || is_null($month) ){
			if( !is_null($date) ){
				//-------- strtotime the submitted date to ensure correct format
				$this->date = date("Y-m-j", strtotime($date)); //var_dump($this->date);
			} else {
				//-------------------------- no date submitted, use today's date
				$this->date = date("Y-m-j");
			}
			$this->set_date_parts_from_date($this->date);
		} else {
			$this->year		= $year;
			$this->month	= str_pad($month, 2, '0', STR_PAD_LEFT);
		}	
	}
	
	function set_date_parts_from_date($date){
		$this->year		= date("Y", strtotime($date));
		$this->month	= date("m", strtotime($date));
		$this->day		= date("d", strtotime($date));
		//settype($this->month, "integer"); settype($this->day, "integer");
	}
	
	function day_of_week($date){
		$day_of_week = date("N", $date);
		if( !is_numeric($day_of_week) ){
			$day_of_week = date("w", $date);
			if( $day_of_week == 0 ){
				$day_of_week = 7;
			}
		}
		return $day_of_week;
	}
	
	function output_calendar($year = NULL, $month = NULL, $calendar_class = 'calendar'){
		
		if( $this->week_start_on !== FALSE ){
			echo "The property week_start_on is replaced due to a bug present in version before 2.6. of this class! Use the property week_start instead!";
			exit;
		}
		
		//--------------------- override class methods if values passed directly
		$year = ( is_null($year) )? $this->year : $year;
		$prev_year = $year - 1;
		$next_year = $year + 1;
		$month = ( is_null($month) )? $this->month : str_pad($month, 2, '0', STR_PAD_LEFT);
	
		//------------------------------------------- create first date of month
		$month_start_date = strtotime($year . "-" . $month . "-01");
		$prev_month = $month_start_date - 86400;
		//------------------------- first day of month falls on what day of week
		$first_day_falls_on = $this->day_of_week($month_start_date);
		//----------------------------------------- find number of days in month
		$days_in_month = date("t", $month_start_date);
		//-------------------------------------------- create last date of month
		$month_end_date = strtotime($year . "-" . $month . "-" . $days_in_month);
		$next_month = $month_end_date + 86400;
		//----------------------- calc offset to find number of cells to prepend
		$start_week_offset = $first_day_falls_on - $this->week_start;
		$prepend = ( $start_week_offset < 0 )? 7 - abs($start_week_offset) : $first_day_falls_on - $this->week_start;
		//-------------------------- last day of month falls on what day of week
		$last_day_falls_on = $this->day_of_week($month_end_date);

		//------------------------------------------------- start table, caption
		$output  = "<table class=\"" . $calendar_class . "\">\n";
		$mon = strftime("%m", $month_start_date); settype($mon,"integer");
		$mon = $this->months[$mon];
		if(isset($_REQUEST["field"])) $add = "&field=".$_REQUEST["field"];
		else $add = "";
		if($this->my_link_mode == 1) $output .= "<caption><a target=\"_self\" href=\"$this->link_to?y=" . strftime("%Y", $prev_month) . "&amp;m=" . strftime("%m", $prev_month) . "$add\">&lt;</a> " . $mon . " <a target=\"_self\" href=\"$this->link_to?y=" . strftime("%Y", $next_month) . "&amp;m=" . strftime("%m", $next_month) . "$add\">&gt;</a> " . "<a target=\"_self\" href=\"$this->link_to?y=" . $prev_year . "&amp;m=" . $month . "$add\">&lt;</a> " . strftime("%Y", $month_start_date) . " <a target=\"_self\" href=\"$this->link_to?y=" . $next_year . "&amp;m=" . $month . "$add\">&gt;</a>" . " </caption>\n";
		else $output .= "<caption><a href=\"#\" onClick=\"change_cal(" . strftime("%Y", $prev_month) . "," . strftime("%m", $prev_month) . "); return false;\">&lt;</a> " . $mon . " <a href=\"#\" onClick=\"change_cal(" . strftime("%Y", $next_month) . "," . strftime("%m", $next_month) . "); return false;\">&gt;</a> " . "<a href=\"#\" onClick=\"change_cal(" . $prev_year . "," . $month . "); return false;\">&lt;</a> " . strftime("%Y", $month_start_date) . " <a href=\"#\" onClick=\"change_cal(" . $next_year . "," . $month . "); return false;\">&gt;</a>" . " </caption>\n";
		$col = '';
		$th = '';
		for( $i=1,$j=$this->week_start,$t=(3+$this->week_start)*86400; $i<=7; $i++,$j++,$t+=86400 ){
			$localized_day_name = gmstrftime('%w',$t);
			$col .= "<col class=\"" . strtolower($localized_day_name) ."\" />\n";
			$th .= "\t<th title=\"" . ucfirst($localized_day_name) ."\">" . ucfirst($this->days[$localized_day_name]) ."</th>\n";
			$j = ( $j == 7 )? 0 : $j;
		}
		
		//------------------------------------------------------- markup columns
		$output .= $col;
		
		//----------------------------------------------------------- table head
		$output .= "<thead>\n";
		$output .= "<tr>\n";
		
		$output .= $th;
		
		$output .= "</tr>\n";
		$output .= "</thead>\n";
		
		//---------------------------------------------------------- start tbody
		$output .= "<tbody>\n";
		$output .= "<tr>\n";
		
		//---------------------------------------------- initialize week counter
		$weeks = 1;
		
		//--------------------------------------------------- pad start of month
		
		//------------------------------------ adjust for week start on saturday
		$last_day_prev = date('d',$prev_month);
		for($i=1;$i<=$prepend;$i++){
		    $d = $last_day_prev - $prepend + $i;
		    $day_date = date('Y', $prev_month).'-'.date('m', $prev_month).'-'.$d;
			$y = strftime("%Y", strtotime($day_date)); $m = strftime("%m", strtotime($day_date)); $wd = strftime("%w", strtotime($day_date)); $d = strftime("%e", strtotime($day_date)); settype($m,"integer");
   			if( is_array($this->highlighted_dates) ){
   				if( in_array($day_date, $this->highlighted_dates) ){
   					$class = " ".$this->default_highlighted_class;
   					$title = "\n".$this->titles[$day_date];
   				}
   				else{ $class = ""; $title = ""; }
  			}
			else{ $class = ""; $title = ""; }
			$output .= "\t<td class=\"pad$class\" id=\"" . strftime("%Y-%m-%d", strtotime($day_date)) . "\" title=\"" . $this->days_long[$wd] . ", $y. gada $d. " . $this->months[$m] . $title . "\">";
			unset($title, $class);
			switch( $this->link_days ){
				case 0 :
					$output .= $d;
				break;
				
				case 1 :
					if( empty($this->formatted_link_to) ){
						$output .= "<a href=\"" . $this->link_to . "?date=" . $day_date . "\">" . $d . "</a>";
					} else {
						$output .= "<a href=\"" . strftime($this->formatted_link_to, strtotime($day_date)) . "\">" . $d . "</a>";
					}
				break;
				case 2 :
					if( is_array($this->highlighted_dates) ){  //var_dump($day_date);
						if( in_array($day_date, $this->highlighted_dates) ){
							if( empty($this->formatted_link_to) ){
								$output .= "<a href=\"" . $this->link_to . "?date=" . $day_date . "\">";
							} else {
								$output .= "<a href=\"" . strftime($this->formatted_link_to, strtotime($day_date)) . "\">";
							}
						}
					}
					
					$output .= $d;
					
					if( is_array($this->highlighted_dates) ){
						if( in_array($day_date, $this->highlighted_dates) ){
							if( empty($this->formatted_link_to) ){
								$output .= "</a>";
							} else {
								$output .= "</a>";
							}
						}
					}
				break;
		    }

			$output .= "</td>\n";
		}
		
		//--------------------------------------------------- loop days of month
		for($day=1,$cell=$prepend+1; $day<=$days_in_month; $day++,$cell++){
			$title = "";
			/*
			if this is first cell and not also the first day, end previous row
			*/
			if( $cell == 1 && $day != 1 ){
				$output .= "<tr>\n";
			}
			
			//-------------- zero pad day and create date string for comparisons
			//$day = str_pad($day, 2, '0', STR_PAD_LEFT);
			$day_date = $year . "-" . $month . "-" . $day;
			
			//-------------------------- compare day and add classes for matches
			if( $this->mark_today == TRUE && $day_date == date("Y-m-d") ){
				$classes[] = $this->today_date_class;
			}
			
			if( $this->mark_selected == TRUE && $day_date == $this->date ){
			    //echo $day_date;
				$classes[] = $this->selected_date_class;
			}
			
			if( $this->mark_passed == TRUE && $day_date < date("Y-m-d") ){
				$classes[] = $this->passed_date_class;
			}
			
			if( is_array($this->highlighted_dates) ){
				if( in_array($day_date, $this->highlighted_dates) ){
					$classes[] = $this->default_highlighted_class;
					$title = "\n".$this->titles[$day_date];
				}
			}
			
			//----------------- loop matching class conditions, format as string
			if( isset($classes) ){
				$day_class = ' class="';
				foreach( $classes AS $value ){
					$day_class .= $value . " ";
				}
				$day_class = substr($day_class, 0, -1) . '"';
			} else {
				$day_class = '';
			}
			
			//---------------------------------- start table cell, apply classes
			// detect windows os and substitute for unsupported day of month modifer
			$y = strftime("%Y", strtotime($day_date)); $m = strftime("%m", strtotime($day_date)); $wd = strftime("%w", strtotime($day_date)); $d = strftime("%e", strtotime($day_date)); settype($m,"integer");
			$title_format = (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN')? "%A, %B %#d, %Y": "%A, %B %e, %Y";
			//var_dump($day_date);
			//$output .= "\t<td" . $day_class . " title=\"" . ucwords(strftime($title_format, strtotime($day_date))) . "\">";
			$output .= "\t<td" . $day_class . " id=\"" . strftime("%Y-%m-%d", strtotime($day_date)) . "\" title=\"" . $this->days_long[$wd] . ", $y. gada $d. " . $this->months[$m] . $title . "\">";
			
			//----------------------------------------- unset to keep loop clean
			unset($day_class, $classes, $title);
			
			//-------------------------------------- conditional, start link tag 
			switch( $this->link_days ){
				case 0 :
					$output .= $day;
				break;
				
				case 1 :
					if( empty($this->formatted_link_to) ){
						$output .= "<a href=\"" . $this->link_to . "?date=" . $day_date . "\">" . $day . "</a>";
					} else {
						$output .= "<a href=\"" . strftime($this->formatted_link_to, strtotime($day_date)) . "\">" . $day . "</a>";
					}
				break;
				
				case 2 :
					if( is_array($this->highlighted_dates) ){ //var_dump($day_date);
						if( in_array($day_date, $this->highlighted_dates) ){
							if( empty($this->formatted_link_to) ){
								$output .= "<a href=\"" . $this->link_to . "?date=" . $day_date . "\">";
							} else {
								$output .= "<a href=\"" . strftime($this->formatted_link_to, strtotime($day_date)) . "\">";
							}
						}
					}
					
					$output .= $day;
					
					if( is_array($this->highlighted_dates) ){
						if( in_array($day_date, $this->highlighted_dates) ){
							if( empty($this->formatted_link_to) ){
								$output .= "</a>";
							} else {
								$output .= "</a>";
							}
						}
					}
				break;
			}
			
			//------------------------------------------------- close table cell
			$output .= "</td>\n";
			
			//------- if this is the last cell, end the row and reset cell count
			if( $cell == 7 ){
				$output .= "</tr>\n";
				$cell = 0;
			}
			
		}
		
		//----------------------------------------------------- pad end of month
		if( $cell > 1 ){
    		//$first_day_next = date('d',$prev_month);
			for($i=$cell;$i<=7;$i++){
 		        $d = 1 + $i - $cell;
		        $day_date = date('Y', $next_month).'-'.date('m', $next_month).'-'.$d;
    			$y = strftime("%Y", strtotime($day_date)); $m = strftime("%m", strtotime($day_date)); $wd = strftime("%w", strtotime($day_date)); $d = strftime("%e", strtotime($day_date)); settype($m,"integer");
    			if( is_array($this->highlighted_dates) ){
    				if( in_array($day_date, $this->highlighted_dates) ){
    					$class = " ".$this->default_highlighted_class;
    					$title = "\n".$this->titles[$day_date];
    				}
       				else{ $class = ""; $title = ""; }
      			}
    			else{ $class = ""; $title = ""; }
	    		$output .= "\t<td class=\"pad$class\" id=\"" . strftime("%Y-%m-%d", strtotime($day_date)) . "\" title=\"" . $this->days_long[$wd] . ", $y. gada $d. " . $this->months[$m] . $title. "\">";
    			unset($title, $class);
	    		switch( $this->link_days ){
	    			case 0 :
	    				$output .= $d;
	    			break;
	    			
	    			case 1 :
	    				if( empty($this->formatted_link_to) ){
	    					$output .= "<a href=\"" . $this->link_to . "?date=" . $day_date . "\">" . $d . "</a>";
	    				} else {
	    					$output .= "<a href=\"" . strftime($this->formatted_link_to, strtotime($day_date)) . "\">" . $d . "</a>";
	    				}
	    			break;
				case 2 :
					if( is_array($this->highlighted_dates) ){  //var_dump($day_date);
						if( in_array($day_date, $this->highlighted_dates) ){
							if( empty($this->formatted_link_to) ){
								$output .= "<a href=\"" . $this->link_to . "?date=" . $day_date . "\">";
							} else {
								$output .= "<a href=\"" . strftime($this->formatted_link_to, strtotime($day_date)) . "\">";
							}
						}
					}
					
					$output .= $d;
					
					if( is_array($this->highlighted_dates) ){
						if( in_array($day_date, $this->highlighted_dates) ){
							if( empty($this->formatted_link_to) ){
								$output .= "</a>";
							} else {
								$output .= "</a>";
							}
						}
					}
				break;
	    	    }

			$output .= "</td>\n";
			//$output .= "\t<td class=\"pad\">&nbsp;</td>\n";
			}
			$output .= "</tr>\n";
		}
		
		//--------------------------------------------- close last row and table
		$output .= "</tbody>\n";
		$output .= "</table>\n";
		
		//--------------------------------------------------------------- return
		return $output;
		
	}
	
}
?>
