<?

class db_permission extends db_entry{
    protected $obj_name = "Pieejas tiesības";
    protected $modules;
    protected $allowed = array();
    
    public function __construct(){
        if(isset($_REQUEST["id"])){
            $this->id = $_REQUEST["id"];
        }
        if(!$this->id) $_SESSION["message"] = array("alert-danger", "Nav norādīts lietotājs, kura tiesības tiek rediģētas!");
        $this->fields["modules"] = "multi";
        $this->pub_names["modules"] = "Pieejas tiesības";
        $this->fields["title"] = "";
        
        parent::__construct("permission",$this->id);
        if($this->id){
            $this->allowed = explode(";", $this->modules);
        }
        $stmt = $this->sql->prepare("select title from user_".$GLOBALS["db_lang_primary"]." where id = ?");
        $stmt->bind_param("i", $this->id); $stmt->execute(); $stmt->bind_result($user); $stmt->fetch(); $stmt->close();
        $this->obj_name .= ": ".$user;
        //echo "LA LA LA";
    }
    
    protected function before_edit(){
        if(!$this->id) $this->abort = true;
    }
    
    protected function before_delete(){
        return true;
    }
    
    protected function before_save(){
        if(isset($_REQUEST["modules"])){
            $this->modules = implode(";", $_REQUEST["modules"]);
        }
        else $this->modules = " ";
        $stmt = $this->sql->prepare("select id from ".$this->table_lang." where id = ?");
        $stmt->bind_param("i", $this->id); $stmt->execute(); $stmt->store_result();
        if(!$stmt->num_rows){
            $query = "insert into ".$this->table_lang." (id) values (?)";
            $stmti = $this->sql->prepare($query); $stmti->bind_param("i", $this->id); $stmti->execute(); $stmti->close();
        }
        $stmt->close();
    }
    
    protected function after_save(){
        /*if($this->send_pw && $this->id){ //die("sending pw");
            $msg = "Labdien,\n\nvarat peislēgties rchv.lv mājas lapas administrēšanas panelim šeit: http://".$_SERVER["SERVER_NAME"]."/admin.php, izmantojot šādu pieteikšanās informāciju:\nlietotājvārds: $this->username\nparole: $this->password";
            $headers = 'From: webmaster@rchv.lv' . "\r\n" .
            'Reply-To: linards.kalvans@lu.lv' . "\r\n" .
            'Content-type: text/plain; charset=utf-8' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
            mail($this->email, "rchv.lv administrēšanas panelis", $msg, $headers);
        }*/
    }
    
    protected function after_edit(){
        if(in_array(get_class($this), $GLOBALS["pub_user"]->allowed)){
            $this->out .= "\t\t<h3>Pievienotie lietotāji</h3>\n";
            $stmt = $this->sql->prepare("select id, title, username from user_".$GLOBALS["db_lang_primary"]." order by title");
            $stmt->execute();
            $stmt->bind_result($id, $title, $username);
            while($stmt->fetch()){
                $this->out .= "<a class=\"data-list\" href=\"$_SERVER[SCRIPT_NAME]?op=$_REQUEST[op]&id=$id\">$title ($username)</a>";
            }
        }
    }
}

?>
