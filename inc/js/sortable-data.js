            $('#data-list').sortable({
                axis: 'y',
                handle: 'div.data-mover',
                opacity: 0.6,
                start: function(ev, ui){
                },
                stop: function(ev, ui){
                    var i = 1;
                    $('#data-list div.data-obj').each(function(){
                        if($(this).attr('id') == ui.item.attr('id')){
                            var url = '/admin.php?op=db_data&do=move_so&id=' + ui.item.attr('id') + '&so=' + i;
                            $.get(url, { ajax: 1 });
                        }
                        else i++;
                    });
                }
            });

