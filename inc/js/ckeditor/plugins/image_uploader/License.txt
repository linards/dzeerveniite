Software License Agreement

1. The Software (Uploader plugin) is offered by Weeby Sp. z o.o., Jagiellońska 16/11, 40-035 Katowice, Poland, for your use in accordance with the terms and conditions below.

2. By downloading, installation or using this Software you represent that you have read, understand and agree terms and conditions of this Software License Agreement (the "License"). If you do not accept these terms and conditions, you are not authorized to download, install or use this Software.

3. This License permits Licensee to install and use this Software on more than one website. Website has to be owned or produced by you. Licensee will not allow copies of the Software to be made by others.

4. This software is licensed, not sold. Licensee is not allowed to sell, lease, loan, sub-license, rent this Software - in whole or in part.

5. Licensee is not allowed to modify source code. Weeby Sp. z o.o. owns intellectual property rights of this Software, its files and source code.

6. This software contains external libraries: Fancy Upload and SabreDAV. Fancy Upload is licensed under the MIT License, and SabreDAV is licensed under the New BSD License. The License do not apply those parts of this Software.

7. This License Agreement is the entire and exclusive agreement between Licensor and Licensee regarding this Software. This License Agreement replaces and supersedes all prior negotiations, dealings, and agreements between Licensor and Licensee regarding this Software.

8. This License Agreement is valid without Licensor's signature.

9. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

