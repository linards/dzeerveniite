var language = new Class({
	
	lang: 'LV',
	panel_name:'Attēli',
	close:'Aizvērt',
	add_dir:'Pievienot mapi',
	add_picture:'Pievienot attēlu',
	confirm:'Vai esi pārliecināts?',
	confirmTree:'Vai esi pārliecināts? \n\n Izdzēst visu koku?',
	search:'Meklēt',
	del:'Dzēst',
	renew:'Mēgini vēlreiz',
	empty_dir:'Tukša mape',
	file_error:'Kļūda - nevar nolasīt failu',
	serverError:'Kļūda - nav atļaujas',
	serverErrorFile:'Kļūda - nevar nomainīt faila nosaukumu',
	file_doubled:'Kļūda - dublējoši faili',
	add_picture_text:'Var ielādēt vairākus failus vienlaicīgi, turot nospiestu CTRL taustiņu',
	file_added:'Fails pievienots',
	send_files:'Nosūtīt failus'
});
