hs.graphicsDir = '/inc/js/highslide/graphics/';
hs.transitions = ['expand', 'crossfade'];
hs.fadeInOut = true;
// Add the controlbar
if (hs.addSlideshow) hs.addSlideshow({
    slideshowGroup: 'gallery',
    interval: 5000,
    repeat: false,
    useControls: true,
    fixedControls: false,
    overlayOptions: {
        opacity: .6,
        position: 'bottom center',
        hideOnMouseOut: false
    }
});

function load_social(){
    if(social){
        //alert(social);
        title = $('#dynamic h3').html();
        $('#social').empty().html('<div id="draugiemLike" style="display: inline-block;"></div>');
        $('#social').append('<div class="fb-like"><iframe height="21" width="90" allowTransparency=\'true\' frameborder=\'0\' scrolling=\'no\' src=\'http://www.facebook.com/plugins/like.php?href='+window.location+'&send=false&layout=button_count&width=85&show_faces=true&action=like&colorscheme=light&height=21\'></iframe></div>');
        $('#social').append('<div id="tweet-button"><a href="https://twitter.com/share" class="twitter-share-button" data-url="'+window.location+'" data-text="'+title+'" data-via="linardskalvans">Tweet</a></div>');
        $('#social').delay(1500).fadeIn(150);
        $('#draugiemLike').empty();
        new DApi.Like({layout:"icon"}).append('draugiemLike');
        twttr.widgets.load();
        social = 0;
    }
}

    $(document).ready(function() {
        if(window.location.hash) {
            // Fragment exists
            var hash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character
            $.get(hash, { ajax: 1 }, function( data ) {
                $("#dynamic").html(data);
                $("#dynamic").find("script").each(function(i) {
                    eval($(this).text());
                });
       			MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
   	    		$('#social').fadeOut(0, function(){
   	    		    load_social();
   	    		});
            });
        }
		var
			History = window.History,
			State = History.getState();
			State.data = {state: $('#dynamic').html()};
			//alert (JSON.stringify(State.data));
		History.pushState(State.data, State.title, State.url);
		History.Adapter.bind(window,'statechange',function(){ // Note: We are using statechange instead of popstate
			var State = History.getState(); // Note: We are using History.getState() instead of event.state
			$('#dynamic').html(State.data.state);
			hs.updateAnchors();
            $("#dynamic").find("script").each(function(i) {
                eval($(this).text());
            });
   			MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
		});
		
		$('#search').on('submit', '#search-form', function(){
            $("#menu li").removeClass('active');
		    url = $(this).attr('action');
		    phrase = $(this).children('#phrase').val();
            $('#social').fadeOut(50);
            $('#dynamic').fadeOut(150,function(){
                $.get(url + '/' + encodeURIComponent(phrase), { ajax: 1 }, function( data ) {
                     var title = $(data).filter('.breadcrumb').text();
                     History.pushState({state:data}, title, url + '/' + encodeURIComponent(phrase));
                     $('#dynamic').fadeIn(150);
                });
            });
            return false;
		});
		   		    
        $("#menu li").on("click", "a.expandable", function () {
                if($(this).parent('li').children('ul').hasClass('hidden-xs')) $(this).parent('li').children('ul').toggleClass('hidden-xs');
                $(this).parent('li').children('ul').toggle(100);
                $(this).children('span').toggleClass('glyphicon-minus-sign').toggleClass('glyphicon-plus-sign');
                return false;
            });

            $("#menu").on("click", "a.menu-trig", function () {
                $(this).children('span').toggleClass('glyphicon-minus-sign').toggleClass('glyphicon-plus-sign');
                $("#menu ul").each( function(i, e){
                    if(i == 0) $(this).toggleClass('hidden-xs');
                });
                return false;
            });

            $("#menu li a.data").on("click", function (event) {
                $("#menu li").removeClass('active');
                $(this).parent('li').addClass('active');
                //$('#social').fadeOut(50);
                //return false;
            });

            $("#menu li, #dynamic").on("click","a:not(.expandable, .menu-trig, .highslide, [href^='javascript'], [href^='http'])",function(event){
                loadUrl = $(this).attr('href'); //alert('clicked');
                if($(this).hasClass('removeActive')) $("#menu li").removeClass('active');
                state = History.getState();
                var regex = new RegExp(loadUrl + '$');
                if(!state.cleanUrl.match(regex)){
                    //alert('acting');
                    $('#social').fadeOut(50);
                    $('#dynamic').fadeOut(150,function(){
                        //$(this).empty();
                        $.get(loadUrl, { ajax: 1 }, function( data ) {
                            var title = $(data).filter('.breadcrumb').text();
                            //alert(JSON.stringify(state));
                            History.pushState({state:data}, title, loadUrl);
                            //alert(data);
                            $('#dynamic').fadeIn(150, function(){
                                load_social();
                            });
                        });
                    });
                }
                return false;
            });
            
            $('#dynamic').on('submit', '#comment-form', function(event){
                //alert('trying to submit');
                url = $(this).attr( 'action' );
                $(this).append('<input type="hidden" name="_" value="jq">');
                vars = $(this).serialize();
                $('#cm').append('<div class="spacer">&nbsp;</div>');
                $(this).parent('div').fadeOut(150, function(){
                    $.post(url, vars ,function( data ) {
                        $('#cm').append(data);
                        $('#cm .hidden').slideDown(150, function(){
                            $(this).removeClass('hidden');
                            $('#cm .spacer').hide(0).removeClass('spacer');
                        });
                    });
                });
                return false;
            });
   	    load_social();
        });

