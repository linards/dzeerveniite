            $('#data-fields').sortable({
                axis: 'y',
                handle: 'div.move',
                opacity: 0.6,
                start: function(ev, ui){
                    if (ui.item.hasClass("up-text")){
                        //alert(ui.item.attr('id'));
                        $('#' + ui.item.attr('id') + ' textarea[id|="ta"]').each(function(){
                            CKEDITOR.instances[$(this).attr('id')].updateElement();
                            CKEDITOR.instances[$(this).attr('id')].destroy(true);
                            //var instance = CKEDITOR.instances[$(this).attr('id')];
                            //CKEDITOR.remove(instance);
                        });
                    }
                },
                stop: function(ev, ui){
                    if (ui.item.hasClass("up-text")){
                        $('textarea[id|="ta"]').each(function(){
                            CKEDITOR.replace($(this).attr('id'));
                        });
                    }
                }
            });

