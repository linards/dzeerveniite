    /*(function(window,undefined){

				// Check Location
				if ( document.location.protocol === 'file:' ) {
					alert('The HTML5 History API (and thus History.js) do not work on files, please upload it to a server.');
				}

				// Establish Variables
				var
					History = window.History, // Note: We are using a capital H instead of a lower h
					State = History.getState();

				// Log Initial State
				//History.log('initial:', State.data, State.title, State.url);
				History.pushState(State.data, State.title, State.url);

				// Bind to State Change
				History.Adapter.bind(window,'statechange',function(){ // Note: We are using statechange instead of popstate
					// Log the State
					var State = History.getState(); // Note: We are using History.getState() instead of event.state
					//History.log('statechange:', State.data, State.title, State.url);
					//var dynamic = document.getElementById('dynamic');
					//dynamic.innerHTML = State.data.state;
					$('#dynamic').html(State.data.state);
				});

			})(window);*/
		var uptitle, upclass, upmode, upresize, upfilters, browse, field_num;
        $(document).ready(function() {
            /*sortable*/
            // Izvēlnes pārslēgšana
            $('#menu li').on('click', '.expand', function () {
                var next = $(this).parent('li').children('ul');
                if($(this).children('img').attr('src') == '/inc/img/icons/24x24/Plus.png'){
                    var src = '/inc/img/icons/24x24/Minus.png';
                    var title = 'Sakļaut';
                }
                else{
                    var src = '/inc/img/icons/24x24/Plus.png';
                    var title = 'Izvērst';
                }
                $(this).attr('title', title).children('img').attr('src', src).attr('alt', title);
                next.toggle(100);
                return false;
            } );

            // AJAX
            $.ajaxSetup ({  
                cache: false  
            });
            
            // Moduļi
            $('#modules').on('mouseover mouseout', 'li', function(event) {
                if (event.type == 'mouseover') {
                //alert("hover");
                    $(this).children('ul').show();
                } else {
                    $(this).children('ul').hide();
                }
            });
            /*
            $("#modules li").on("click", function () {
                $('#menu li.nochildren-expanded').addClass('nochildren')
                $("#menu li").removeClass('nochildren-expanded');
                $("#modules li").removeClass('expanded');
                $(this).addClass('expanded');
            });
            
            // Saites
            // Pielikt garā nosaukuma pievienošanu
            $("a:not(.js, .logout, .cke_button, .cke_dialog_tab, .cke_dialog_ui_button, .cke_dialog_close_button, .cke_combo_button, .plupload_button, .highslide)").on("click",function(event){
                if($(this).parent('li').hasClass('menu')){
                    $('#modules li:not(.db_cat)').removeClass('expanded');
                    if(!$('#modules li.db_cat').hasClass('expanded')) $('#modules li.db_cat').addClass('expanded');
                }
                loadUrl = $(this).attr('href');
                //alert
                //var title = $(this).text();
                $('#dynamic').fadeOut(150,function(){
                    //$('#dynamic').html('<img src="/inc/img/loader.gif" alt="loader">').fadeIn(150);
                    $.get(loadUrl, function( data ) {
                        var hist = History.getState();
                        //$('#dynamic').fadeOut(150, function(){
                            var title = 'Admin: ' + $(data).filter('div.road').text();
                            History.pushState({state:data}, title, loadUrl);
                            $("#dynamic").find("script").each(function(i) {
                                eval($(this).text());
                            });
                            $('#dynamic').fadeIn(150);
                        //});
                        if(GetURLParameter('op') == "db_cat" && GetURLParameter('do') == "delete"){
                            $('#menu ul:first').load('admin.php' + '?update=cats&id=' + $('#dynamic #id-0').val()).fadeIn(100);
                        }
                        $('#data-fields').sortable({
                            start: function(){
                                $('textarea[id|="ta"]').each(function(){
                                    CKEDITOR.instances[$(this).attr('id')].updateElement();
                                    CKEDITOR.instances[$(this).attr('id')].destroy(true);
                                });
                            },
                            stop: function(){
                                $('textarea[id|="ta"]').each(function(){
                                    CKEDITOR.replace($(this).attr('id'));
                                });
                            }
                        });
                    });
                });
            
                if(event.preventDefault){
                    event.preventDefault();
                }else{
                    event.returnValue = false; 
                }
            });*/
            // Faila dzēšana
            $('#dynamic').on('click', '.delete-file', function(event){
                if(confirm('Vai tiešām dzēst? Neatgriezeniska darbība!')){
                    var parent = $(this).parent('div');
                    var loadUrl = $(this).attr('href');
                    parent.fadeOut(100, function(){
                        $.get(loadUrl, function( data ) {
                            parent.html(data).fadeIn(150);
                        });
                    });
                }
                if(event.preventDefault){
                    event.preventDefault();
                }else{
                    event.returnValue = false; 
                }
            });
            // Formas

            $('#dynamic').on('submit', 'form[id!="auth-forma"]', function(event) {
                //if($(this).attr('id') == "auth-foma") return true;
                //$(this).append('<input type="hidden" name="_" value="' + $(this).find("#op").val() + '">');
                var formUrl = $(this).attr('action');
                var sequence = new Array(); var i = 0;
                $('.data-field').each(function(){
                    var fieldid = $(this).attr('id');
                    sequence[i] = fieldid.replace('data-field-', '');
                    i++;
                });
                //alert(sequence.join(';'));
                $("#sequence").val(sequence.join(';'));
            });
            /*
            $("form[id!='auth-forma']").on("submit", function(event) {
                //if($(this).attr('id') == "auth-foma") return true;
                $(this).append('<input type="hidden" name="_" value="' + $(this).find("#op").val() + '">');
                var formUrl = $(this).attr('action');
                var sequence = new Array(); var i = 0;
                $('.data-field').each(function(){
                    var fieldid = $(this).attr('id');
                    sequence[i] = fieldid.replace('data-field-', '');
                    i++;
                });
                //alert(sequence.join(';'));
                $("#sequence").val(sequence.join(';'));
                $('textarea[id|="ta"]').each(function(){
                    CKEDITOR.instances[$(this).attr('id')].destroy(true);
                });
                var vars = $(this).serialize();
                //alert(vars);
                var op = $(this).find("input[name='op']").val();
                window.formErrors = 0;
                //return false;
                $('div.plupload').remove();
                if(!window.formErrors){
                    $('#dynamic').fadeOut(150,function(){
                        //$('#dynamic').html('<img src="/inc/img/loader.gif" alt="loader">').fadeIn(150);
                        $.post(formUrl, vars ,function( data ) {
                            var hist = History.getState();
                            var title = 'Admin: ' + $(data).filter('div.road').text();

                            //var title = document.title;
                            //$('#dynamic').fadeOut(150, function(){
                                History.pushState({state:data}, title, $(this).attr('action'));
                                $("#dynamic").find("script").each(function(i) {
                                    eval($(this).text());
                                });
                                $('#dynamic').fadeIn(150);
                            //});
                            //alert(op);
                            if(op == "db_user"){
                                $('#modules ul:first').fadeOut(200,function(){$('#modules ul:first').load(formUrl + '?update=links&op='+op).fadeIn(100)});
                            }
                            else if(op == "db_cat"){
                                $('#menu ul:first').fadeOut(200,function(){$('#menu ul:first').load(formUrl + '?update=cats&id=' + $('#id-0').val()).fadeIn(100)});
                            }
                        },
                        'html');
                    });
                }
                if(event.preventDefault){
                    event.preventDefault();
                }else{
                    event.returnValue = false; 
                }
            });*/
            
            // Datu formas elementi
            $('#dynamic').on('click', '.js', function(event){
                $link = $(this);
                if($(this).hasClass('data-text') || $(this).hasClass('data-gallery') || $(this).hasClass('data-file')){
                    var object;
                    if($(this).hasClass('data-text')){
                        object = '<textarea class="ckeditor" id="ta-'+field_num+'" name="data['+field_num+']"></textarea><input type="hidden" name="mode['+field_num+']" value="txt">';
                        if($(this).hasClass('end')) $('#data-fields').append('<div class="new-data data-field hidden-field up-text" id="data-field-'+field_num+'"><div class="move">Velc, lai pārvietotu</div>'+object+'<br><a href="#" class="js remove-this">Noņemt šo lauku</a></div>');
                        else $('#data-fields').prepend('<div class="new-data data-field hidden-field up-text" id="data-field-'+field_num+'"><div class="move">Velc, lai pārvietotu</div>'+object+'<br><a href="#" class="js remove-this">Noņemt šo lauku</a></div>');
                        if($(this).hasClass('data-text')) CKEDITOR.replace( 'ta-'+field_num );
                        $('div.hidden-field').slideDown(200).removeClass('hidden-field');
                    }
                    
                    else if($(this).hasClass('data-file') || $(this).hasClass('data-gallery')){
                        if($(this).hasClass('data-file')){
                            uptitle = 'Pievienot jaunus failus (faili tiks sakārtoti pēc nosaukumiem):';
                            upclass = 'up-file';
                            upmode = 'file';
                            upresize = '';
                            upfilters = {title : "Visi faili", extensions : "*"};
                        }
                        else{
                            uptitle = 'Pievienot jaunus attēlus (attēli tiek sakārtoti pēc faila nosaukuma; faila nosaukums tiks izmantots kā noklusetais paraksts; lieli attēli tiks automātiski samazināti līdz ' + ur_images.width + ' px garākajā malā):';
                            upclass = 'up-image';
                            upmode = 'gal';
                            upresize = ur_images;
                            upfilters = {title : "Attēli", extensions : "jpg,JPG,jpeg,JPEG,gif,GIF,png,PNG"};
                        }
                        var data = '<div class="new-data data-field hidden-field uploader '+upclass+'" id="data-field-'+field_num+'"><div class="move">Velc, lai pārvietotu</div><div id="uploader'+field_num+'">Lūdzu izmantojiet pārlūkprogrammu, kas atbalsta HTML5, piemēram Chrome vai Firefox</div><button id="pickfiles'+field_num+'">Izvēlēties failus</button><button id="uploadfiles'+field_num+'">Augšupielādēt failus</button><br><a href="#" class="js remove-this">Noņemt šo lauku</a><input type="hidden" name="mode['+field_num+']" value="'+upmode+'"><input type="hidden" name="files['+field_num+']" id="files-'+field_num+'"></div>';
                        if($link.hasClass('end')) $('#data-fields').append(data);
                        else $('#data-fields').prepend(data);
                        browse = 'pickfiles'+field_num;
                        var uploader = new plupload.Uploader({
                    		runtimes : 'html5',
                      		browse_button : browse,
                       		max_file_size : '10mb',
                       		multiple_queues : true,
                       		url : '/inc/php/upload-handler.php',
                       		resize : upresize,
                       		filters : [
    		                    upfilters
                       		],
                       	});
                        	
                       	uploader.bind('Init', function(up, params) {
                       		$('#uploader'+field_num).html(uptitle + '<input type="hidden" name="mode['+field_num+']" value="'+upmode+'"><input type="hidden" name="files_id['+field_num+']" value="'+uploader.id+'"><input type="hidden" id="files-'+uploader.id+'" name="file_sets['+uploader.id+']">');
                       	});

                       	uploader.bind('FilesAdded', function(up, files) {
	                        $.each(files, function(i, file) {
                       			$('#uploader-'+up.id).append(
                       				'<div id="' + file.id + '" class="file-added">' +
                       				file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
                       			'</div>');
                       		});
                       	});
	
                       	uploader.bind('UploadFile', function(up, file) {
                       		$('<input type="hidden" name="file-' + file.id + '" value="' + file.name + '">')
                       			.appendTo('#data-form');
                       	});

                       	uploader.bind('UploadProgress', function(up, file) {
                       		$('#' + file.id + " b").html(file.percent + "%");
                       	});
                       	
                       	uploader.bind('FileUploaded', function(up, file, response) {
                       	    var obj = jQuery.parseJSON(response.response);
                       	    var val = $('#files-'+up.id).val();
                       	    $('#'+file.id).addClass('file-uploaded').removeClass('file-added');
                       	    $('#files-'+up.id).val(val+';'+file.id);
                   	        $('#uploader-'+up.id).append('<input type="hidden" name="'+file.id+'" value="'+obj.name+';'+file.name+'">');
                        });

                       	$('#uploadfiles'+field_num).click(function(e) {
                       		uploader.start();
                       		e.preventDefault();
                       	});

                        uploader.init();
                        $('#uploader'+field_num).attr('id','uploader-'+uploader.id)
                        $('#files-'+field_num).attr('id','files-'+uploader.id);
                        $('div.hidden-field').slideDown(200).removeClass('hidden-field');
                    }
                    var sequence = $("#sequence").val();
                    if($link.hasClass('end')) $("#sequence").val(sequence + ';' + field_num);
                    else $("#sequence").val(field_num + ';' + sequence);
                    field_num++;
                }
                else if($(this).hasClass('remove-this') && confirm('Vai tiešām noņemt?')) $(this).parent('div').slideUp(200,function(){$(this).empty();});
                else if($(this).hasClass('show-comments')) $('#data-comments').toggle(300);
                else if($(this).hasClass('delete-comment')){ //alert('clicked');
                    url = $(this).attr('href');
                    a = $(this);
                    $.get(url, function( data ) {
                        $(a).parent('div').toggleClass('visible-0').toggleClass('visible-1');
                    });
                }
                else if($(this).hasClass('move-expand')){ //alert('clicked');
                    var child = $(this).parent('li').children('ul');
                    child.toggle(100);
                    if($(this).children('img').attr('src') == '/inc/img/icons/24x24/Plus.png'){
                        var src = '/inc/img/icons/24x24/Minus.png';
                        var title = 'Sakļaut';
                    }
                    else{
                        var src = '/inc/img/icons/24x24/Plus.png';
                        var title = 'Izvērst';
                    }
                    $(this).attr('title', title).children('img').attr('src', src).attr('alt', title);
                    //alert(child.context);
                }
                return false;
            });
            // paziņojumi
            $('button[data-dismiss="alert"]').on("click", function(){
                $(".alert").alert();
            });
        });

