CREATE TABLE IF NOT EXISTS `data_lva` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner` int(10) unsigned,
  `public` int(1) unsigned NOT NULL DEFAULT '2',
  `parent` int(10) unsigned NOT NULL DEFAULT '0',
  `data` varchar(12) NOT NULL DEFAULT 'db_data',
  `title` varchar(512) NOT NULL DEFAULT 'sadaļas dati',
  `value` text NOT NULL,
  `sortorder` int(10) unsigned NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cipher` varchar(1024) NOT NULL DEFAULT 'sadalas_dati',
  PRIMARY KEY (`id`),
  KEY `parent` (`owner`, `parent`,`sortorder`,`public`),
  KEY `parent_2` (`owner`, `parent`,`created`,`public`),
  KEY `cipher` (`cipher`),
  KEY `mode` (`data`),
  FULLTEXT KEY `data_ft` (`title`,`value`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci
