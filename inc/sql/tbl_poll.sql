CREATE TABLE IF NOT EXISTS `poll_lva` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(512) NOT NULL DEFAULT 'jautājums',
  `start` date DEFAULT NULL,
  `end` date DEFAULT NULL,
  `cook` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cipher` (`cook`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci
