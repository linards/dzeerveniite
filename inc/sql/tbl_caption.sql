CREATE TABLE IF NOT EXISTS `caption_lva` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(512) NOT NULL DEFAULT 'identifikators',
  `desc` varchar(1024) NOT NULL DEFAULT 'Informatīvs apraksts',
  `caption` text,
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  FULLTEXT KEY `caption_ft` (`desc`, `caption`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci
