CREATE TABLE IF NOT EXISTS `files_lva` (
  `file` varchar(256) NOT NULL,
  `parent` int(10) unsigned,
  `txt` text,
  `location` varchar(1024),
  PRIMARY KEY (`file`),
  KEY (`parent`),
  FULLTEXT KEY `files_ft` (`txt`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci
