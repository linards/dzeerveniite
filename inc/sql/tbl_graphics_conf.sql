CREATE TABLE IF NOT EXISTS `graphics_conf_lva` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mode` int(11) NOT NULL DEFAULT '1',
  `title` varchar(512) NOT NULL DEFAULT 'identifikators',
  `desc` varchar(512) not null default 'Informatīvs apraksts',
  `width` int(11) NOT NULL DEFAULT '200',
  `height` int(11) NOT NULL DEFAULT '100',
  `number` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `title` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci
