CREATE TABLE IF NOT EXISTS `events_lva` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(512) NOT NULL DEFAULT 'notikums',
  `date` date NOT NULL,
  `value` text,
  `cipher` varchar(5124) NOT NULL DEFAULT 'notikums',
  PRIMARY KEY (`id`),
  KEY `date` (`date`),
  KEY `cipher` (`cipher`),
  FULLTEXT KEY `events_ft` (`title`,`value`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci
