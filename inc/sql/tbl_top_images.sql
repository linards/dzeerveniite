CREATE TABLE IF NOT EXISTS `top_images_lva` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `img` varchar(1024) DEFAULT NULL,
  `valid` date DEFAULT NULL,
  `colors` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `valid` (`valid`),
  KEY `img` (`img`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci
