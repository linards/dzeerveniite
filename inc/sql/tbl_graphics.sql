CREATE TABLE IF NOT EXISTS `graphics_lva` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent` int(10) unsigned,
  `filename` varchar(5124) NOT NULL DEFAULT 'faila-nosaukums',
  `sortorder` int(11) not null default '1',
  `alt` varchar(512) not null default 'paraksts',
  `url` varchar(512) NOT NULL DEFAULT 'http://',
  `blank` int(11) NOT NULL DEFAULT '0',
  `shows` int(11) NOT NULL DEFAULT '0',
  `hits` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  KEY `filename` (`filename`),
  KEY `sortorder` (`sortorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci
