CREATE TABLE IF NOT EXISTS `comments_lva` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(512) NOT NULL DEFAULT 'Anonīms',
  `value` text NOT NULL,
  `created` timestamp NOT NULL,
  `ip` varchar(512),
  `browser` varchar(512),
  `visible` int(1) not null default '1',
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  FULLTEXT KEY `comment_ft` (`title`,`value`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci
