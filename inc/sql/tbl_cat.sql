CREATE TABLE IF NOT EXISTS `cat_lva` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `public` int(1) unsigned NOT NULL DEFAULT '2',
  `parent` int(10) unsigned NOT NULL DEFAULT '0',
  `mode` int(11) NOT NULL DEFAULT '1',
  `social` int(1) not null default '0',
  `show_date` int(1) not null default '1',
  `comments` int(1) not null default '0',
  `title` varchar(512) NOT NULL DEFAULT 'sadaļa',
  `sortorder` int(10) unsigned NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `cipher` varchar(512) NOT NULL DEFAULT 'sadala',
  PRIMARY KEY (`id`),
  KEY `parent` (`public`, `parent`,`sortorder`),
  KEY `cipher` (`cipher`),
  KEY `sortorder` (`sortorder`),
  FULLTEXT KEY `cat_ft` (`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci
