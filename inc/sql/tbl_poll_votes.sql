CREATE TABLE IF NOT EXISTS `poll_votes_lva` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent` int(10) unsigned NOT NULL,
  `title` varchar(1024) NOT NULL DEFAULT 'atbildes variants',
  `votes` int(10) unsigned NOT NULL DEFAULT '0',
  `sortorder` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci
