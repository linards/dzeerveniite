CREATE TABLE IF NOT EXISTS `lessons_ch_lva` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` int(10) unsigned NOT NULL,
  `date` date DEFAULT NULL,
  `value` text,
  PRIMARY KEY (`id`),
  KEY `date` (`date`),
  KEY `class` (`class`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci
