CREATE TABLE IF NOT EXISTS `lessons_lva` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(512) NOT NULL DEFAULT 'klase',
  `mode` int(1) NOT NULL DEFAULT '1',
  `lessons1` text,
  `lessons2` text,
  `lessons3` text,
  `lessons4` text,
  `lessons5` text,
  `cipher` varchar(1024) NOT NULL DEFAULT 'klase',
  `parent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci
