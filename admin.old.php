<?
//COOKIE_start();
//var_dump($_COOKIE);
error_reporting(E_ERRORS);
ini_set("display_errors", "1");  
include_once("config.php");

settype($page,"string");
settype($contents,"string");

if(isset($_COOKIE["user"])) $pub_user = new pub_user($_COOKIE["user"]);
else $pub_user = new pub_user();
if(isset($_REQUEST["logout"])) $pub_user->logout();

//var_dump($pub_user->limited);
$allowed = $pub_user->limited + $pub_user->allowed;

$colors = new colors;
$colors->load();
//var_dump($colors);

// te nāk centrs kolonna
$contents .= $pub_user->info();

//$contents .= $cat->road();
if(!$pub_user->username) $contents .= $pub_user->auth();

if(isset($_REQUEST["add"])){
    $data = new $_REQUEST["add"](null, $_REQUEST["id"]);
    if(in_array(get_class($data), $allowed)) $contents .= $data->edit();
}
elseif(isset($_REQUEST["op"])){
    $op = new $_REQUEST["op"];
    //if(isset($_REQUEST["id"]) || isset($_REQUEST["img"])) $contents .= $op->edit();
    $contents .= $op->road();
    if(in_array(get_class($op), $allowed)) $contents .= $op->edit();
    else $contents .= "<div class=\"deny\">Jums nav pieejas tiesību šiem datiem</div>";
    if(method_exists($op,"files") && in_array(get_class($op), $allowed)) $contents .= $op->files();
    if(in_array(get_class($op), $allowed) && get_class($op) == "db_cat") $contents .= $op->data_list();
    //var_dump($op);
}
//COOKIE_destroy();

// te nāk augša

$diff = 0; $h = 1; $a = -1; $f = 2;
do{
    $a++;
    $diff = colorDistance($colors->get_color($a), "#fade47");
} while($diff < 150); //echo $diff;
if($a == $h) $h = 0;
elseif($a == $f) $f = 0;

$from = array("Dzērvenīte","#93cfbb", "#0000ff", "#b48773", "#cdc081", "/img/DSC_1225.jpg");
$from[] = $colors->default_colors;
$to = array($project_title, $colors->get_color($f), $colors->get_color($a), $colors->get_color(0), $colors->get_color($h), $colors->img);
$newcols = array();
$to[] = $colors->new_colors;

$page .= str_replace($from, $to, file_get_contents("./inc/html/head-admin.htm"));

$page .= file_get_contents("./inc/html/3-col-center-admin.htm");

if(!$contents) $contents = "&nbsp;";
$page .= $contents;
    
//echo $cat->save();

//var_dump($data);

$page .= file_get_contents("./inc/html/3-col-left.htm");
// te nāk kreisā mala

$cat = new db_cat_menu($colors->colors);
$page .= $cat->nav_list();

//include("html/menu.htm");
$from = array("#919e95", "#544c61", "#aab7a1");
$to = array($colors->get_color($cat->colin + 1),$colors->get_color($cat->colin + 2),$colors->get_color($cat->colin + 3));
$page .= str_replace($from, $to, file_get_contents("./inc/html/left-pane-admin.htm"));

$info = new srv_info();

$from = array("<!--lapinas-linki-->", "<!--info-->");
$to = array($pub_user->links(), $info->out);
$page .= str_replace($from, $to, file_get_contents("./inc/html/3-col-right-admin.htm"));
// te nāk labā mala

$page .= file_get_contents("./inc/html/3-col-bottom.htm");
// te nāk apakša

$page .= file_get_contents("./inc/html/foot.htm");

echo $page;
?>
