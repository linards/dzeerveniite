<?
header('Content-Type: text/html; charset=utf-8');
session_start();
error_reporting(E_ALL);
ini_set("display_errors", "1");
include_once("config.php");

//var_dump($_SERVER["REQUEST_URI"]);
//die();

$displevel = 1;
if(isset($_GET["preview"])){
	if(isset($_COOKIE["user"])) $pub_user = new pub_user($_COOKIE["user"]);
	if($pub_user->username){
		$displevel = 0;
#		var_dump($parr);
	}
}
$action = prepare_request($_SERVER["REQUEST_URI"], $displevel);
#var_dump($parr);
#var_dump($action);

// Valoda
setlocale ( LC_ALL , $locales[$db_lang]);
$long_date = $date_formats[$db_lang]; // strftime

$obj = new $action["class"]($action["parent"]);
//var_dump($obj);
$obj->populate_road_parts();
$road = $obj->road();

$contents = $road;
$contents .= $obj->contents()."\n".$GLOBALS["tracking_code"];

if(isset($_REQUEST["ajax"])){
    echo $contents;
    exit;
}

$page = file_get_contents($cwd."inc/html/index.htm");

//echo $obj->cat_parent;
if($action["class"] != "pub_cat"){
    if(isset($action["parents"])) $cat = new pub_cat(end($action["parents"]));
    else  $cat = new pub_cat(0);
}
else $cat = $obj;
//var_dump($cat);

$langs = new lang_switcher();

$from = array(
    "<!--title-->",
    "<!--var social-->",
    "<!--lang-switcher-->",
    "<!--menu-->",
    "<!--contents-->",
    "/request_prefix"
    );
    
$to = array(
    strip_tags(preg_replace("%</li>.*\n.*<li>%", " &raquo; ", $road)),
    (method_exists($obj,"social") ? $obj->social() : "<script type=\"text/javascript\">var social=0;</script>"),
    $langs->show_flags(),
    $cat->user_menu(),
    $contents,
    $GLOBALS["request_prefix"]
);

$start = 0;
while($start = strpos($page, "<!--graphics:", $start+1)){
    $end = strpos($page, "-->", $start);
    $length = $end - $start - 13;
    $conf = trim(substr($page, $start + 13, $length));
    $graphics = new pub_graphics($conf);
    $from[] = substr($page, $start, $length + 16);
    $to[] = $graphics->show();
}
while($start = strpos($page, "<!--caption:", $start+1)){
    $end = strpos($page, "-->", $start);
    $length = $end - $start - 12;
    $conf = trim(substr($page, $start + 12, $length));
    $graphics = new pub_caption($conf);
    $from[] = substr($page, $start, $length + 15);
    $to[] = $graphics->show();
}

echo str_replace($from, $to, $page);

?>
