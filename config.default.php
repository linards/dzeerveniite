<?
// Pēc ikvienu izmaiņu veikšanas ir ļoti ieteicams pārlūkprogrammā atvērt /init.php skriptu

// Pamatdati
$project_title = array("lva" => "Dzērvenīte", "rus" => "Клюква", "eng" => "Cranberries");
$db_host = "localhost";
$db_user = "";
$db_pass = "";
$db_name = "";

// valoda
$db_lang_primary = "lva";
$supported_langs = array("lva", "rus", "eng");
$db_lang = $db_lang_primary;
$locales = array(
    "lva" => "lv_LV.UTF-8",
    "rus" => "ru_RU.UTF-8",
    "eng" => "en_GB.UTF-8"
    );
$date_formats = array(
    "lva" => "%H:%M, %A, %e. %B, %Y.",
    "rus" => "%H:%M, %A, %B %e, %Y.",
    "eng" => "%H:%M, %A, %B %e, %Y."
    );
/*if(isset($_SESSION["lang"])){
    if(in_array($_SESSION["lang"], $supported_langs)) $db_lang = $_SESSION["lang"];
}
if(isset($_GET["lang"])){
    if(in_array($_GET["lang"], $supported_langs)) $db_lang = $_GET["lang"];
}
$_SESSION["lang"] = $db_lang;*/

$cwd = $_SERVER["DOCUMENT_ROOT"];
if(substr($cwd, -1) != "/") $cwd .= "/";

$request_prefix = "/dz";

$tracking_code = "";

$ipp = 10; // items_per_page
$imsize = 1200; // max attēla izmērs, jānomaina arī failā /inc/js/kcfinder/config.php
$basewidth = 870; // lapas satura pilnais platums
$thumbsize = 110; // ikonas izmērs, jānomaina arī failā /inc/js/kcfinder/config.php, pēc nomainīšanas nerādīsies iepriekš pievienot gr. elementu 'thumbi' administratora panelī
$thumbquality = 90; // ikonas kvalitātes

// Lapas satura konfigurācija
$cat_depth = 3; // Maksimālais sadaļu līmeņu skaits
$cat_data = 0; // 0 - nav atļauti vienlaicīgi datu bloki un apakšsadaļas vienā sadaļā; 1 - ir atļauti
$data_levels = array("2","+"); // kuros līmeņos ir atļauti datu bloki, "+" nozīmē, ka visos līmeņos, lielākos par iepriekšējo ir atļauti datu bloki

// Lai izslēgtu moduli, aizkomentē to!
$modules = array(
    "db_cat" => "Lapas saturs", // Šo vajadzētu vienmēr atstāt ieslēgtu!
    //"top_image" => "Augšējais attēls",
    //"rnd_pic" => "Gadījuma attēli",
    //"db_lessons" => "Stundu saraksts",
    //"db_lessons_ch" => "Stundu izmaiņas",
    //"db_event" => "Kalendāra notikumi",
    //"db_poll" => "Aptaujas",
    "db_graphics_conf" => "Grafisko el. konf.",
    "db_graphics" => "Grafiskie elementi",
    "db_caption" => "Uzraksti",
    "db_user" => "Lietotāji",
    "db_permission" => "Pieejas tiesības",
    "" => "" // šo vajag, lai negadītos komatu aizmiršana
);

// Moduļi, kam jādod pieeja tikai no primārās valodas
// Izmaiņas šajos nebūs fleksiblas un var radīt problēmas
$modules_primary = array("db_user", "db_graphics_conf");
// Šos drīkst atslēgt/pieslēgt svešvalodām
$moduls_primary[] = "db_graphics";

$avoid_cat = array(
    "pub_search", "pub_caption"
);

$icons = array(
    "db_cat" => "Sitemap.png",
    "top_image" => "Photo.png",
    "rnd_pic" => "Picture.png",
    "db_lessons" => "Table.png",
    "db_lessons_ch" => "Tag.png",
    "db_event" => "Calendar.png",
    "db_poll" => "Light.png",
    "db_graphics_conf" => "Tool.png",
    "db_graphics" => "Puzzle.png",
    "db_caption" => "Text Large.png",
    "db_user" => "Key.png",
    "db_permission" => "Lock Open.png",
    "" => "" // šo vajag, lai negadītos komatu aizmiršana
);

$updates = array(
    "db_cat" => array("cats", "#menu ul"),
    "db_user" => array("links", "#modules ul")
);

include_once($cwd."functions.php");
require_once($cwd."inc/classes/db.inc.php");
$sql = db::getInstance();

function __autoload($classname) {
    $filename = $GLOBALS["cwd"]."inc/classes/". $classname .".inc.php";
    include_once($filename);
}

$times = array(
    1 => array("", "8<sup>00</sup> - 8<sup>40</sup>",
        "8<sup>45</sup> - 9<sup>25</sup>",
        "9<sup>30</sup> - 10<sup>10</sup>",
        "10<sup>15</sup> - 10<sup>55</sup>",
        "11<sup>00</sup> - 11<sup>40</sup>",
        "11<sup>45</sup> - 12<sup>25</sup>",
        "12<sup>30</sup> - 13<sup>10</sup>",
        "13<sup>15</sup> - 13<sup>55</sup>",
        "14<sup>00</sup> - 14<sup>40</sup>",
        "14<sup>45</sup> - 15<sup>25</sup>",
        "15<sup>30</sup> - 16<sup>10</sup>",
        "16<sup>15</sup> - 16<sup>55</sup>"
    ),
    2 => array("", "13<sup>15</sup> - 13<sup>55</sup>",
        "14<sup>00</sup> - 14<sup>40</sup>",
        "14<sup>45</sup> - 15<sup>25</sup>",
        "15<sup>30</sup> - 16<sup>10</sup>",
        "16<sup>15</sup> - 16<sup>55</sup>",
        "17<sup>00</sup> - 17<sup>40</sup>",
        "17<sup>45</sup> - 18<sup>25</sup>"
    )
);

$days = array("Svētdiena", "Pirmdiena", "Otrdiena", "Trešdiena", "Ceturtdiena", "Piektdiena", "Sestdiena", "Svētdiena");
$days_short = array("Sv.", "Pr.", "O.", "T.", "C.", "Pk.", "Se.", "Sv."); 
$months = array("", "janvāris", "februāris", "marts", "aprīlis", "maijs", "jūnijs", "jūlijs", "augusts", "septembris", "oktobris", "novembris", "decembris");
$months_short = array("", "jan.", "feb.", "mar.", "apr.", "maijs", "jūn.", "jūl.", "aug.", "sept.", "okt.", "nov.", "dec.");

?>
