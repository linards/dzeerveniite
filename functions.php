<?

function test(){
    return "test()@functions.php";
}

function prepare_request($rq, $displevel = 1){
    $parts = explode("?", $rq);
    $path = explode("/", $parts[0]);

    //echo "keram valodu!"; // pārbaudam, vai mums tiek piedāvāta valoda
    if(@in_array($path[2], $GLOBALS["supported_langs"])){
        $GLOBALS["db_lang"] = $path[2];
        $start = 3;
        //echo "te?!";
    }
    else{
        $GLOBALS["db_lang"] = $GLOBALS["db_lang_primary"];
        $start = 2;
        //echo "nea?!";
    }
    if($GLOBALS["db_lang"] != $GLOBALS["db_lang_primary"]) $GLOBALS["request_prefix"] .= "/".$GLOBALS["db_lang"];
    //echo $GLOBALS["db_lang"];

    $stmt = $GLOBALS["sql"]->prepare("select id from cat_".$GLOBALS["db_lang"]." where cipher = ? and parent = ? and public > $displevel");
    $stmt->bind_param("si", $cipher, $parent);
    $stmt->bind_result($parent);
    $parents = array();
    
    // pēc noklusējuma uzskatām, ka ir sadaļa
    $class = "pub_cat";
    
    // ejam cauri takai
    $ciphers = array();
    for($i = $start; $i < count($path); $i++){
        $cipher = $path[$i];
        $ciphers[] = $cipher;
        if(substr($cipher, 0, 1) == "!") break;
        $stmt->execute();
        $stmt->fetch();
        $parents[] = $parent;
    }
    $stmt->close();

    // dati
    if($cipher == "!d"){
        //echo "dati!!!";
        $class = "pub_data"; $cipher = $path[$i + 1];
        $ciphers[] = $cipher;
        $stmt = $GLOBALS["sql"]->prepare("select id from data_".$GLOBALS["db_lang"]." where cipher = ? and parent = ? and public > $displevel");
        $stmt->bind_param("si", $cipher, $parent);
        $stmt->bind_result($parent);
        $stmt->execute();
        $stmt->fetch();
        $stmt->close();
    }
    
    // stundu saraksts
    elseif($cipher == "!l" || $cipher == "!lc"){
        if($cipher == "!lc") $class = "pub_lessons_ch";
        else $class = "pub_lessons";
        $parent_cat = $parent;
        if(isset($path[$i + 1])){
            $ciphers[] = $path[$i + 1];
            $stmt = $GLOBALS["sql"]->prepare("select id from lessons_".$GLOBALS["db_lang"]." where cipher = ?");
            $stmt->bind_param("s", $cipher); $cipher = $path[$i + 1];
            $stmt->bind_result($parent);
            $stmt->execute();
            $stmt->fetch();
            $stmt->close();
        }
        else $parent = 0;
    }
    
    // kalendārs
    elseif($cipher == "!c"){
        $class = "pub_events";
        $d = $path[$i + 1]; $m = $path[$i + 2]; $y = $path[$i + 3];
        $ciphers[] = $path[$i + 1];
        $ciphers[] = $path[$i + 2];
        $ciphers[] = $path[$i + 3];
        //echo "$d/$m/$y";
    }
    
    // aptaujas
    elseif($cipher == "!p"){
        $class = "pub_poll";
    }
    
    // meklēšana
    elseif($cipher == "!m"){
        $class = "pub_search";
        if(isset($path[$i + 1])){
            $GLOBALS["match"] = rawurldecode($path[$i + 1]);
            $ciphers[] = $path[$i + 1];
        }
        elseif(isset($_POST["phrase"])) $GLOBALS["match"] = $_POST["phrase"];
        else $GLOBALS["match"] = "";
    }
    
    // komentāri
    elseif($cipher == "!cm"){
        $class = "pub_comments";
        if(isset($path[$i + 1])){
            $GLOBALS["cm_cipher"] = rawurldecode($path[$i + 1]);
            $ciphers[] = $path[$i + 1];
        }
        else $GLOBALS["cm_cipher"] = 0;
    }
    
    // komentāri
    elseif($cipher == "!cp"){
        $class = "pub_caption";
        if(isset($path[$i + 1])){
            $GLOBALS["cp_cipher"] = $path[$i + 1];
            $ciphers[] = $path[$i + 1];
        }
        else $GLOBALS["cp_cipher"] = 0;
    }
    
    // karte
    elseif($cipher == "!mp") $class = "pub_map";
    
    // nekā cita nav, tad ņemam pirmo sadaļu
    elseif(!$parent){
        /*$res = $GLOBALS["sql"]->query("select id, cipher from cat_".$GLOBALS["db_lang"]." where parent = 0 order by sortorder limit 1");
        $row = $res->fetch_assoc();
        $parent = $row["id"];*/
        $_SERVER["REQUEST_URI"] = $GLOBALS["request_prefix"]."/";
        $uri = 1;
        //echo $_SERVER["REQUEST_URI"];
        $parent = 0;
    }
    
    if(!isset($uri)){
        $_SERVER["REQUEST_URI"] = $GLOBALS["request_prefix"]."/".implode("/", $ciphers)."/";
        //echo $_SERVER["REQUEST_URI"];
    }
    
    return array(
        "class" => $class,
        "parent" => $parent,
        "parents" => $parents
    );
}

function no_lv_symbols($text){
    // Vispirms iztīram LV simbolus    
    $alfabets = array("A","a","Ā","ā","B","b","C","c","Č","č","D","d","E","e","Ē","ē","F","f","G", "g","Ģ","ģ","H","h","I","i","Ī","ī","J","j","K","k","Ķ","ķ","L","ļ","M","m","N","n","Ņ","ņ","O","o", "P","p","Q","q","R","r","S","s","Š","š","T","t","U","u","Ū","ū","V","v","W","w","X","x","Y","y","Z", "z","Ž","ž");
$alfabets_no_lv = array("A","a","A","a","B","b","C","c","C","c","D","d","E","e","E","e","F","f","G", "g","G","g","H","h","I","i","I","i","J","j","K","k","K","k","L","l","M","m","N","n","N","n","O","o", "P","p","Q","q","R","r","S","s","S","s","T","t","U","u","U","u","V","v","W","w","X","x","Y","y","Z", "z","Z","z");
    $text = str_replace($alfabets, $alfabets_no_lv, $text);

    // Swap out Non "Letters" with a -
    $text = preg_replace('/[^\\pL\d]+/u', '-', $text); 

    // Trim out extra -'s
    $text = trim($text, '-');

    // Convert letters that we have left to the closest ASCII representation
    //$text = iconv('utf-8', 'iso-8859-1//TRANSLIT', $text);
#    $text = transliterate($text,array(
#		'cyrillic_transliterate', 
#		'diacritical_remove'
#	   ),
#    'utf-8', 'utf-8');
    
    $rule = 'NFD; [:Nonspacing Mark:] Remove; NFC';
    $myTrans = Transliterator::create($rule);
    $text = $myTrans->transliterate($text);

    // Make text lowercase
    $text = strtolower($text);

    // Strip out anything we haven't been able to convert
    $text = preg_replace('/[^-\w]+/', '', $text);

    return $text;
}

function colors($color, $mode = 0){
    $start[0] = hexdec(substr($color,1,2));
    $start[1] = hexdec(substr($color,3,2));
    $start[2] = hexdec(substr($color,5,2));
    $sum = $start[0] + $start[1] + $start[2];
    $sh = 4;
    if($mode == 0){
        if($sum > 255){
            for($i = 0; $i < 3; $i++) $diff[$i] = (-$start[$i])/$sh;
        }
        else{
            $sh = 6;
            for($i = 0; $i < 3; $i++) $diff[$i] = (255-$start[$i])/$sh;
        }
    
        for($j = 0; $j <= $sh; $j++){
            for($i = 0; $i < 3; $i++){
                $comp[$i] = round($start[$i]+$diff[$i]*$j);
                $hex[$i] = str_pad(dechex($comp[$i]), 2, "0", STR_PAD_LEFT);
            }
            $out[] = "#".$hex[0].$hex[1].$hex[2];
        }
    }
    else{
        if($start[0] >= $start[1]){
            if($start[0] >= $start[2]){
                $m[0] = 0;
                if($start[1] >= $start[2]){ $m[1] = 1; $m[2] = 2; }
                else{ $m[1] = 2; $m[2] = 1; }
            }
            else{ $m[0] = 2; $m[1] = 0; $m[2] = 1; }
        }
        elseif($start[1] >= $start[2]){
            $m[0] = 1;
            if($start[0] >= $start[2]){ $m[1] = 0; $m[2] = 2; }    
            else{ $m[1] = 2; $m[2] = 0; }    
            }
        else{ $m[0] = 2; $m[1] = 1; $m[2] = 0; }
        if($sum > 255){
            $diff = -25;
            $m = array_reverse($m);
        }
        else{
            $diff = 25;
        }
        $loop = true;
        $comp = $start;
        while($loop){
            if($comp[$m[0]] + $diff <= 255 && $comp[$m[0]] + $diff >= 0) $comp[$m[0]] += $diff;
            elseif($comp[$m[1]] + $diff <= 255 && $comp[$m[1]] + $diff >= 0) $comp[$m[1]] += $diff;
            elseif($comp[$m[2]] + $diff <= 255 && $comp[$m[2]] + $diff >= 0) $comp[$m[2]] += $diff;
            else $loop = false;
            for($i = 0; $i < 3; $i++){
                $hex[$i] = str_pad(dechex($comp[$i]), 2, "0", STR_PAD_LEFT);
            }
            $out[] = "#".$hex[0].$hex[1].$hex[2];
        }
    }
    return $out;
}

function thumb($img, $path, $width = 110, $height=0, $adaptive=1){
    require_once $GLOBALS["cwd"].'inc/classes/ThumbLib.inc.php';
    $options = array('jpegQuality' => $GLOBALS["thumbquality"]);
    $thumb = PhpThumbFactory::create($img, $options);
    $size = getimagesize($img);
    if($width > 0) $asize = ($size[1] > $size[0] ? round($width * $size[1]/$size[0]) : $width);
    else $asize = ($size[0] > $size[1] ? round($height * $size[0]/$size[1]) : $height);
    //echo $asize."thumb!!!"; die();
    if($height > 0 && $width > 0 && $adaptive){ $thumb->adaptiveResize($width, $height)->save($path); }
    else if($height > 0 && $width > 0) $thumb->resize($width, $height)->save($path);
    else if($height < 0 || $width < 0) $thumb->resize($asize, $asize)->save($path);
    else if($adaptive) $thumb->adaptiveResize($asize, $asize)->save($path);
    else $thumb->resize($asize, $asize)->save($path);
    //if($frame) thumb_frame($path);
}

function thumb_frame($img, $frame = 8){
    /*$thumb = new easyphpthumbnail;
    $pi = pathinfo($img);
    //$size = getimagesize($img);
    $thumb -> Thumblocation = $pi["dirname"]."/"; //var_dump($pi); die();
    $thumb -> Framewidth = $frame;
    $thumb -> Framecolor = '#FFFFFF';
    $thumb -> Backgroundcolor = '#ffffff';
    $thumb -> Shadow = true;
    $thumb -> Createthumb($img, 'file');*/
}

function thumbpx($img,$path,$width = 150,$height=""){
    //include_once('class/easyphpthumbnail.class.php');
    $thumb = new easyphpthumbnail;
    $thumb -> Thumblocation = $path."thumbs/";
    $thumb -> Thumbwidth = $width;
    $thumb -> Thumbheight = $height;
    $thumb -> Createthumb($path.$img,'file');
}

function pixelate($img,$path,$size = 10){
    //include_once('class/easyphpthumbnail.class.php');
    $thumb = new easyphpthumbnail;
    $thumb -> Thumblocation = $path."pixelated/";
    $thumb -> Thumbheight = 180;
    $thumb -> Thumbwidth = 800;
    $thumb -> Pixelate = array(1,$size);
    $thumb -> Createthumb($path.$img,'file');
    //echo img($path.$img);
}

function img($img, $class = "", $alt = ""){
    //var_dump(pathinfo($img));
    if(!file_exists($GLOBALS["cwd"].substr($img,1))) $img = "/img/not-found.png";
    $size = getimagesize($GLOBALS["cwd"].substr($img,1));
    $pi = pathinfo($GLOBALS["cwd"].substr($img,1));
    $img = str_replace($pi["basename"], rawurlencode($pi["basename"]), $img);
    $out = "<img alt=\"$alt\" class=\"$class\" src=\"$img\" $size[3] />";
    return $out;
}

function show_thumb($img, $class, $width = 110, $alt = "", $expand = 1, $group = "data", $align = ""){ 
    $org = $img;
    $img = substr($img, 1);
    //echo " $img ";
    if(!file_exists($GLOBALS["cwd"].$img)) $img = "img/not-found.png";
    $size = getimagesize($GLOBALS["cwd"].$img);
    if($size[0] > $GLOBALS["imsize"] || $size[1] > $GLOBALS["imsize"]) thumb($img, $img, $GLOBALS["imsize"]);
    $pi = pathinfo($img);
    if($size[0] > $width){
        if(!file_exists($pi["dirname"]."/thumbs")){
            mkdir($pi["dirname"]."/thumbs", 0777);
            chmod($pi["dirname"]."/thumbs", 0777);
        }
        if(!file_exists($pi["dirname"]."/thumbs/".$pi["basename"])){
            thumb($pi["dirname"]."/".$pi["basename"],$pi["dirname"]."/thumbs/".$pi["basename"], $width, 0, 1);
        }
        //$img = "thumb.php?img=$img&amp;width=$width";
        $img = str_replace($GLOBALS["cwd"],"/",$pi["dirname"]."/thumbs/".$pi["basename"]);
        //$height = round($size[1] * $width / $size[0]);
        $size[3] = "width=\"$width\" height=\"$width\"";
    }
    $img = str_replace($pi["basename"], rawurlencode($pi["basename"]), $img);
    $phone = "<div class=\"visible-xs phone-img\">".show_single($org, $alt)."<br>$alt</div>";
    $out = "<img alt=\"$alt\" class=\"hidden-xs $class\" src=\"/$img\" $size[3]>";
    if($expand) return "<a href='$org' class='highslide hidden-xs $class' onclick='return hs.expand(this, { slideshowGroup: \"$group\", align: \"$align\" });'>".$out."</a>"."<div class=\"highslide-caption\">$alt</div>".$phone;
    else return $out.$phone;
}

function show_single($img, $alt = ""){ 
    $img = substr($img, 1);
    if(!file_exists($GLOBALS["cwd"].$img)) $img = "img/not-found.png";
    $info = getimagesize($GLOBALS["cwd"].$img);
    if($info[0] > $GLOBALS["basewidth"]){
        $height = round($GLOBALS["basewidth"] * $info[1] / $info[0]);
        $info[3] = "width=\"".$GLOBALS["basewidth"]."\" height=\"$height\"";
    }
    $pi = pathinfo($img);
    $img = str_replace($pi["basename"], rawurlencode($pi["basename"]), $img);
    return "<img alt=\"$alt\" class=\"data-img-single\" src=\"/$img\" $info[3]>";
}

function makeValuesReferenced($arr){
    $refs = array();
    foreach($arr as $key => $value)
        $refs[$key] = &$arr[$key];
    return $refs;

}

function next_business_day() {
    $date = date('Y-m-d');
    $add_day = 0;
    do {
        $add_day++;
        $new_date = date('Y-m-d', strtotime("$date +$add_day Days"));
        $new_day_of_week = date('w', strtotime($new_date));
    } while($new_day_of_week == 6 || $new_day_of_week == 0);

    return $new_date;
}

function rrmdir($dir) {
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
             if ($object != "." && $object != "..") {
                if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object);
             }
        }
        reset($objects);
        rmdir($dir);
    }
}

function parse_images($text, $expand = 1){
    //echo $expand;
    //preg_match_all('/<img .*alt="(.*?)".*src="(.*?)".*>/', $text, $img_ord);
    $doc = new DOMDocument();
    @$doc->loadHTML('<?xml encoding="UTF-8">'. $text);
    $tags = $doc->getElementsByTagName('img');
    //var_dump($tags);
    foreach ($tags as $tag) {
       $alt = $tag->getAttribute('alt');
       $src = $tag->getAttribute('src');
       $text = preg_replace('(<img.*src="'.$tag->getAttribute('src').'"[^>]+>)',show_thumb($src, "data-txt-img", $GLOBALS["thumbsize"], $alt, $expand), $text);
       //var_dump($tag);
    }
    return $text;
}

function get_data_parts($value){
    $p = 0; $prev = 0; $parts = array(); $out = array();
    //var_dump($value);
    do{
        $p = strpos($value,"<!--CMS",$prev);
        $ps[] = $p; $prev = $p + 1;
    } while($p !== false);
    //var_dump($ps);
    foreach($ps as $key => $val){
        if($val === false) break;
        $type = substr($value, $val+8, 3);
        //echo $type."<br />";
        if($type == "tex"){
            if(isset($ps[$key + 1])){
                if($ps[$key + 1]) $val = substr($value, $val + 28, $ps[$key + 1] - $val - 28);
                else $val = substr($value, $val);
            }
            else $val = substr($value, $val);
        }
        elseif($type == "gal"){
            $val = substr($value, $val+12, 14);
        }
        elseif($type == "fil"){
            $val = substr($value, $val+13, 14);
        }
        $parts[] = array($type, $val);
    }
    //var_dump($parts);
    return $parts;
}

function format_data($id, $value, $title, $mode = 1, $cipher = "", $timestamp = false, $social = 0, $show_date = 1, $comments = 0){ //echo $mode;
    $cmo = new pub_comments($id);
    if($comments == 2) $title .= " (".$cmo->show_number().")";
    
    $p = 0; $prev = 0; $parts = array(); $out = array();
    $parts = get_data_parts($value);
    $link = false;
    if($mode == 2){
        $link = str_replace(array("///", "//"),array("/","/"),"<a class=\"lasit-vairak\" href=\"$_SERVER[REQUEST_URI]/!d/$cipher\">");
        //else $link = false;
        $parts = @array($parts[0]); //echo $link;
        }
    reset($parts);
    foreach($parts as $key => $part){
        if($part[0] == "tex"){ //var_dump(strpos("#-", strip_tags($part[1])));
            if(strpos($part[1], "#--stundu-saraksts--#")){ //echo "LA LA LA";
                $ls = new pub_lessons(0);
                $part[1] = str_replace("##--stundu-saraksts--##", $ls->contents(), $part[1]);
            }
            if(strpos($part[1], "#--stundu-izmainas--#")){ //echo "LA LA LA";
                $ls = new pub_lessons_ch(0);
                $part[1] = str_replace("##--stundu-izmainas--##", $ls->contents(), $part[1]);
            }
            if($key < 1) $emph = " lead";
            else $emph = "";
            $out[] = "<div class=\"data-block$emph\">".text_goodies(remove_format(parse_images($part[1], ($link ? 0 : 1))))."</div>";
            //$out[] = "<div class=\"data-block\">".text_colors(remove_format($part[1]))."$link</div>";
        }
        elseif($part[0] == "gal"){
            $path = $GLOBALS["cwd"]."assets/";
            $path_pub = "/assets/";

            $fld = $part[1];
            $files = array();
            
            if(file_exists($path.$fld)){
                $dir = new DirectoryIterator($path.$fld);
                foreach ($dir as $fileinfo) {
                    if ($fileinfo->isFile()) {
                        $files[] = $fileinfo->getFilename();
                    }
                }
            }
            natcasesort($files); $tmp = "";

            $stmt = $GLOBALS["sql"]->prepare("select txt from files_".$GLOBALS["db_lang"]." where file = ?");
            $stmt->bind_param("s",$file_path);
            $stmt->bind_result($txt);
            
            if(count($files) > 1){            
                $i = 1;
                foreach($files as $file){
                    $file_path = filename_to_db($part[1].$file);
                    $stmt->execute(); $stmt->store_result(); //echo $file_path;
                    if($stmt->num_rows) $stmt->fetch(); 
                    else{
                        $pi = pathinfo($file);
                        $txt = $pi["filename"];
                    }
                    $file_path = $part[1].rawurlencode($file);
                    //$class = "thumb"; if($i == 5){ $class = "thumb-5"; $i = 0; } $i++;
                    //$tmp .= "<a href='$path_pub$file_path' class='highslide hidden-xs' onclick='return hs.expand(this, { slideshowGroup: \"$fld\" });'><img class='$class' src='$path_pub$part[1]thumbs/$file' title='Uzklikšķini, lai palielinātu'></a><div class=\"highslide-caption\">$txt</div>";
                    $tmp .= show_thumb($path_pub.$file_path, "thumb", $GLOBALS["thumbsize"], htmlspecialchars($txt), 1, 'gallery', 'center');
                }
            }
            elseif(isset($files[0])){
                $file = $files[0];
                $file_path = filename_to_db($part[1].$file);
                $stmt->execute(); $stmt->store_result();
                if($stmt->num_rows) $stmt->fetch();
                else $txt = "";
                $tmp .= show_single($path_pub.$part[1].$file)."<div class=\"caption\">$txt</div>";
            }
            $stmt->close();
            $out[] = "<div class=\"data-block\">$tmp$link</div>";
        }
        elseif($part[0] == "fil"){ //echo "fileeesss!!!";
            $path = $GLOBALS["cwd"]."assets/";
            $path_pub = "/assets/";

            $fld = $part[1];
            $files = array();
            
            if(file_exists($path.$fld)){
                $dir = new DirectoryIterator($path.$fld);
                foreach ($dir as $fileinfo) {
                    if ($fileinfo->isFile()) {
                        $files[] = $fileinfo->getFilename();
                    }
                }
            }
            natcasesort($files); $tmp = "";
            
            $i = 1;
            foreach($files as $file){ //echo $file;
                $pi = pathinfo($path_pub.$part[1].$file);
                if(file_exists("mime/".strtolower($pi["extension"])."-glyphicon-64x64.png")) $icon = "mime/".strtolower($pi["extension"])."-glyphicon-64x64.png";
                else $icon = "mime/blank-64x64.png";
                $class = "data-file"; if($i == 5){ $class = "data-file-5"; $i = 0; } $i++;
                $tmp .= "<div class='$class'><a href='$path_pub$part[1]".rawurlencode($file)."'><img alt=\"$pi[extension]\" src=\"/$icon\"><br />".str_replace(array("_","-","."), array(" ", " ",". "), $pi["filename"])."</a></div>";
            }
            //echo $tmp;
            $out[] = "<div class=\"data-block\">$tmp$link</div>";
        }
        //var_dump($out);
    }
    if($timestamp && $show_date == 2) $time = "\t\t<div class=\"data-date\">".format_date_full($timestamp)."</div>\n";
    else $time = null;
    if($comments == 2 && !$link){
        array_push($out, "<div id=\"cm\">\n"."\t\t\t<h3>Komentāri</h3>\n".$cmo->show().$cmo->show_form()."\t\t</div>\n");
    }
    return $link."\t\t<h3>$title</h3>\n".$time.implode("\n\t\t\t", $out).($link ? "</a>" : "").($social == 2 && !$link ? "<script type=\"text/javascript\">var social = 1;</script>" : "<script type=\"text/javascript\">var social = 0;</script>");
}

function format_date_full($date){
    $dt = new DateTime($date);
    //return $dt->format('l, j. F, Y.');
    return ucfirst(strftime($GLOBALS["long_date"], $dt->format('U')));
}

function other_items($cnt){
    if(isset($_REQUEST["s"])) $s = $_REQUEST["s"];
    else $s = 0;
    $out = array();
    $pages = ceil($cnt / $GLOBALS["ipp"]);
    if($pages > 1){
        for($i = 1; $i <= $pages; $i++){
            $p = ($i - 1) * $GLOBALS["ipp"];
            $active = "";
            if($p == $s) $active = "class=\"active\"";
            $out[] = "<li $active><a href=\"".str_replace(array("//","?".$_SERVER["QUERY_STRING"]), array("/",""), $_SERVER["REQUEST_URI"])."?s=$p\">$i</a></li>";
        }
        //echo $out;
        $disabled = "";
        if(!$s) $disabled = "class=\"disabled\"";
        $p = max(0, $s - $GLOBALS["ipp"]);
        $link = "<li $disabled><a href=\"".str_replace(array("//","?".$_SERVER["QUERY_STRING"]), array("/",""), $_SERVER["REQUEST_URI"])."?s=$p\">&laquo;</a></li>";
        array_unshift($out, $link);
        
        $disabled = "";
        if($s == ($pages - 1) * $GLOBALS["ipp"]) $disabled = "class=\"disabled\"";
        $p = min($pages * $GLOBALS["ipp"], $s + $GLOBALS["ipp"]);
        $link = "<li $disabled><a href=\"".str_replace(array("//","?".$_SERVER["QUERY_STRING"]), array("/",""), $_SERVER["REQUEST_URI"])."?s=$p\">&raquo;</a></li>";
        array_push($out, $link);
        return "\t\t<div class=\"pagination\"><ul>".implode("\n\t\t\t",$out)."</ul></div>\n";
    }
    else return null;
}

function isValidEmail($email){
	return preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})^", $email);
}

function generatePassword($length = 8) {
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $count = mb_strlen($chars);

    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }

    return $result;
}

function recurse_copy($src,$dst) { 
    $dir = opendir($src); 
    @mkdir($dst); 
    while(false !== ( $file = readdir($dir)) ) { 
        if (( $file != '.' ) && ( $file != '..' )) { 
            if ( is_dir($src . '/' . $file) ) { 
                recurse_copy($src . '/' . $file,$dst . '/' . $file); 
            } 
            else { 
                copy($src . '/' . $file,$dst . '/' . $file); 
            } 
        } 
    } 
    closedir($dir); 
}

function colorDistance($color1,$color2){
    $dis=0;
    $col1 = array(substr($color1, 1, 3), substr($color1, 3, 5), substr($color1, 5, 7));
    $col2 = array(substr($color2, 1, 3), substr($color2, 3, 5), substr($color2, 5, 7));
    for($i=0;$i<3;$i++){
        $dis += ($col1[$i]-$col2[$i]) * ($col1[$i]-$col2[$i]);
    }
    $dis=sqrt($dis);
    return $dis; 
}

function pr_code($matches){
    return "<code>" . htmlspecialchars($matches[1]) . "</code>";
}

function prepare_code($txt){
    return preg_replace_callback('/\<code\>(.+)\<\/code\>/isU', "pr_code", $txt);
}

function find_url($text){
    $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
    preg_match_all($reg_exUrl, $text, $matches);
    $usedPatterns = array();
    foreach($matches[0] as $pattern){
        if(!array_key_exists($pattern, $usedPatterns)){
             $usedPatterns[$pattern]=true;
             $text = str_replace  ($pattern, "<a href=\"{$pattern}\" rel=\"nofollow\">{$pattern}</a> ", $text);   
        }
    }
    return $text;            
}

function remove_format($txt){
    $from = array("align=");
    $to = array("class=");
    return trim(strip_tags(str_replace($from, $to, $txt), "<sub><sup><quote><code><br><p><a><img><strong><em><strike><table><tbody><tr><td><th><ol><ul><li><h1><h2><h3><h4><h5><h6><pre><hr><address><div><span>"), chr(0xC2).chr(0xA0));
    //return $txt;
}

function filename_to_db($filename){
    return substr(str_replace(array("."," "),array("_","_"),$filename),0,300);
}

function list_files($path){
    if(file_exists($path)){
        $dir = new DirectoryIterator($path);
        foreach ($dir as $fileinfo) {
            if ($fileinfo->isFile()) {
                $files[] = $fileinfo->getFilename();
            }
        }
    }
    natcasesort($files);
    return $files;
}

function message(){
    if(isset($_SESSION["message"])){
        $out = "\t\t\t<div class=\"alert ".$_SESSION["message"][0]."\">\n\t\t\t\t".
        "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n\t\t\t\t".
        "".$_SESSION["message"][1]."\n\t\t\t".
        "</div>";
        unset($_SESSION["message"]);
        return $out;
    }
    else return "";
}

function text_goodies($text){
    $dt = new DateTime();
    $nyear = strftime('%Y', $dt->format('U'));
    $text = str_replace($nyear." &ndash; ".$nyear, $nyear, preg_replace("^\[years\]([0-9]*)\[/years\]^", '$1 &ndash; '.$nyear, $text));
    return $text;
}

function build_url($id, $table = "data"){
	if($table == "data"){
		$stmt = $GLOBALS["sql"]->prepare("select parent, cipher from data_".$GLOBALS["db_lang"]." where id = ?");
	    $stmt->bind_param("i", $id);
	    $stmt->bind_result($id, $cipher);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();
		$parts = array($cipher, "!d");
	}
	else $parts = array();
	$stmt = $GLOBALS["sql"]->prepare("select parent, cipher from cat_".$GLOBALS["db_lang"]." where id = ?");
    $stmt->bind_param("i", $id);
    $stmt->bind_result($id, $cipher);
	$stmt->execute();
	while($stmt->fetch()){
		if($cipher) $parts[] = $cipher;
	}
	$stmt->close();
	return $GLOBALS["request_prefix"]."/".implode("/", array_reverse($parts));
}
?>
