<?
header('Content-Type: text/html; charset=UTF-8');
session_start();

//var_dump($_POST); die();

error_reporting(E_ALL);
ini_set("display_errors", "1");  
include_once("config.php");

if(isset($_SESSION["lang"])){
    if(in_array($_SESSION["lang"], $supported_langs)) $db_lang = $_SESSION["lang"];
}
if(isset($_GET["lang"])){
    if(in_array($_GET["lang"], $supported_langs)) $db_lang = $_GET["lang"];
}
$_SESSION["lang"] = $db_lang;

settype($page,"string");
settype($contents,"string");

if(isset($_COOKIE["user"])) $pub_user = new pub_user($_COOKIE["user"]);
else $pub_user = new pub_user();
if(isset($_REQUEST["logout"])) $pub_user->logout();
if(!isset($_REQUEST["op"]) && !isset($_REQUEST["update"])) $_REQUEST["op"] = "db_cat";
if(isset($_REQUEST["_"])) $aq = "&_=".$_REQUEST["_"];
else $aq = "";

#echo $pub_user->show_id();
$allowed = $pub_user->limited + $pub_user->allowed;

if(isset($_REQUEST["update"])){
    echo $pub_user->$_REQUEST["update"]();
    exit;
}

if(isset($_REQUEST["add"])){
    $data = new $_REQUEST["add"](null, $_REQUEST["id"]);
    if(in_array(get_class($data), $allowed)) $contents .= $data->edit();
}
elseif(isset($_REQUEST["op"])){ //var_dump($_REQUEST["op"]);
    $op = new $_REQUEST["op"];
    //if(isset($_REQUEST["id"]) || isset($_REQUEST["img"])) $contents .= $op->edit();
    $contents .= $op->road();
    if(!isset($_COOKIE["user"])){ $contents .= $pub_user->auth(); }
    elseif(in_array(get_class($op), $allowed)) $contents .= $op->edit();
    else $contents .= "<div class=\"deny\">Jums nav pieejas tiesību šiem datiem</div>";
    if(method_exists($op,"files") && in_array(get_class($op), $allowed)) $contents .= $op->files();
    //if(in_array(get_class($op), $allowed) && get_class($op) == "db_cat") $contents .= $op->data_list();
    //var_dump($op);
}

if(isset($_REQUEST["_"])){
    echo $contents;
    exit;
}

$page = file_get_contents($cwd."inc/html/admin.htm");

if(!$pub_user->username){
    $from = "<!--dynamic-content-->";
    $to = $pub_user->auth();
    echo str_replace($from, $to, $page);
    exit;
}
//$_SESSION["message"] = array("alert-success", "Kaut kas labs!");

$langs = new lang_switcher();

$from = array(
    "<!--title-->",
    "/*modules-icons*/",
    "/*sortable*/",
    "<!--thumbsize-->",
    "<!--imsize-->",
    "<!--thumbquality-->",
    "<!--menu-->",
    "<!--lang-switcher-->",
    "<!--user-info-->",
    "<!--modules-list-->",
    "<!--dynamic-content-->",
    "<!--message-->"
);

$to = array(
    strip_tags($op->road()),
    $pub_user->css(),
    file_get_contents($GLOBALS["cwd"]."inc/js/sortable.js"),
    $GLOBALS["thumbsize"],
    $GLOBALS["imsize"],
    $GLOBALS["thumbquality"],
    $pub_user->cats(),
    $langs->show_flags("db"),
    $pub_user->info(),
    $pub_user->links(),
    $contents,
    message()
);

//$contents .= $cat->road();
echo str_replace($from, $to, $page);
//var_dump($_REQUEST);
?>
